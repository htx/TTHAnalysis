# To-do list for FNCN studies

## Signal sample generation

- [ x ] definition of job option files
- [ x ] validation of job option
- [ x ] request sample production JIRA ticket https://its.cern.ch/jira/browse/ATLMCPROD-3570 
- [ ] request TOPQ1 derivation
- [ ] Feynman diagrams for documentation (`tHq` example available in
  `/afs/cern.ch/user/g/gerbaudo/public/tHq_decay.py`, needs to be
  modified for all decays we consider). We might also want to make the
  same "matching" diagrams as the ones made by Loic (with Jaxodraw)

## Basic kinematic checks

Checks that do not require signal samples.

- [ ] wrap up new (VLQ) submission scripts and test them on ntuples [2.4.20-3](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/VLQand4top#Latest_ntuples_produced) (Davide, target 2016-11-01)
- [ ] bkg-only fit with >=5j
- [ ] check data/mc in >=4j >=2b
- [ ] bkg-only fit with >=4j >=2b
- [ ] can we use tight/tight+loose b-tag splitting? (requires continuous b-tagging)

## Discriminant

Tasks that do not require signal samples
- [X] check whether all necessary truth info is available in our tt/ttbb MultibjetsAnalysis ntuples (Davide 2016-10-24)
- [ ] implement (ntuple-making or offline) the categorization that
  TtHbbLeptonic stores in `jet_truthmatch`, see description in
  [TTHbbLeptonic/JetTruthMatchingTool](https://svnweb.cern.ch/cern/wsvn/atlasphys-hsg8/Physics/Higgs/HSG8/AnalysisCode/TTHbbLeptonic/trunk/docs/tools.html#jet-truth-matching-tool)
  and implementation here
  [JetTruthMatchingTool](https://svnweb.cern.ch/cern/wsvn/atlasphys-hsg8/Physics/Higgs/HSG8/AnalysisCode/TTHbbLeptonic/trunk/Root/truthMatching.cxx)
- [ ] fill templates for background (Davide, target 2016-11-01)
- [ ] plot matching fractions for background

Tasks that do require signal samples
- [ ] check truth info from MultibjetsAnalysis ntuples
- [ ] evaluate matching fractions
- [ ] fill templates
- [ ] smooth templates

## Code and infrastructure
- [ ] figure out with VLQ whether we need to have separate ntuples for 4j
  - [ ] what is the breakdown of size in 0L/1L/4j/5j
  - [ ] are there branches we can drop? (hard to imagine a significant
    reduction, get a branch size summary)
- [ ] figure out disk space where we can store shared ntuples
  - [ ] add examples to request and renovate copies on `CERN-PROD_SCRATCHDISK`
  - [ ] make a list of how many user eos spaces we have (1TB each)
  - [ ] disk on HK T2? when available? to how many users?
  - [ ] disk at IFAE ? can we grant access to external users? or just
    store there and then have a script to replicate elsewhere
    (e.g. `CERN-PROD_SCRATCHDISK`)
