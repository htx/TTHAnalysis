#!/bin/env python

# Plot the p_{sig}, p_{bkg}, discr distributions from FCNC trees made by TTH_Analysis
#
# davide.gerbaudo@gmail.com
# March 2017

import ROOT as R
R.gROOT.SetBatch(True)
R.gStyle.SetOptStat(0)

import glob
import sys
from collections import OrderedDict

def main():
    def open_root_file(fn):
        return R.TFile.Open(fn)
    plot_logp = True # plot log(p) or p
    print_separation = True
    input_dir = sys.argv[1] if len(sys.argv)>1 else '/tmp/disc_out6'
    signal_label = 'thc'
    input_files = OrderedDict({'thc'     : glob.glob(input_dir+'/outVLQAnalysis_uHcWm_410698._nominal_*_FcncTree.root'),
                               # 'tt'      : open_root_file(input_dir+'/outVLQAnalysis_ttbar_410000._nominal_0_FcncTree.root'),
                               # 'ttbb'    : open_root_file(input_dir+'/ttbarbb_410000._nominal_0Tree.root'),
                               # 'ttcc'    : open_root_file(input_dir+'/ttbarcc_410000._nominal_0Tree.root'),
                               'ttlight' : glob.glob(input_dir+'/outVLQAnalysis_ttbarlight_410000._nominal_*_FcncTree.root')
                               })
    selections = OrderedDict({'4jin2bin' : 'jets_n>=4 && bjets_n>=2',
                              '4jin2bin' : 'jets_n>=4 && bjets_n>=2',

                              '4jex2bex' : 'jets_n==4 && bjets_n==2',
                              '4jex3bex' : 'jets_n==4 && bjets_n==3',
                              '4jex4bin' : 'jets_n==4 && bjets_n>=4',

                              '5jex2bex' : 'jets_n==5 && bjets_n==2',
                              '5jex3bex' : 'jets_n==5 && bjets_n==3',
                              '5jex4bin' : 'jets_n==5 && bjets_n>=4',

                              '6jin2bex' : 'jets_n>=6 && bjets_n==2',
                              '6jin3bex' : 'jets_n>=6 && bjets_n==3',
                              '6jin4bin' : 'jets_n>=6 && bjets_n>=4'})
    variables = ['ProbSig', 'ProbBkg', 'Discriminant']
    binning_p = (100, -40.0, 0.0) if plot_logp else (100, 0.0, 0.0025)
    binning_d = (20, 0.0, 1.0)
    histograms = OrderedDict({sel+'_'+sam+'_'+var : R.TH1F("h_%s_%s_%s" % (sel, sam, var), "%s %s" % (sel, sam),
                                                           *(binning_d if var=='Discriminant' else binning_p))
                              for sel in selections.keys()
                              for sam in input_files.keys()
                              for var in variables})
    for sample, input_filenames in input_files.iteritems():
        tree_name = 'fcncTree'
        chain = R.TChain(tree_name)
        for ifn in input_filenames:
            chain.Add(ifn)
        for selection_name, selection_expression in selections.iteritems():
            for var in variables:
                histokey = "%s_%s_%s" % (selection_name, sample, var)
                histo = histograms[histokey]
                histoname = histo.GetName()
                # selection_command = "nomWeight * (%s)" % selection_expression
                selection_command = "1.0 * (%s)" % selection_expression
                draw_var = ("Discriminant" if var=='Discriminant' else
                            ("TMath::Log(%s)" if plot_logp else "%s") % var)
                draw_command = draw_var+' >> ' + histoname
                print draw_command
                num_events = chain.Draw(draw_command, selection_command)
                print "%s : %d entries (%d events selected)" % (histoname, histo.GetEntries(), num_events)
    can = R.TCanvas('log_ProbSig', '')
    leg = R.TLegend(0.15, 0.65, 0.55, 0.875)
    tex = R.TLatex(0.0, 0.0, '')
    tex.SetNDC(True)
    
    colors = {'thc' : R.kBlue,
              'tt' : R.kBlack,
              'ttbb': R.kBlack,
              'ttcc' : R.kRed,
              'ttlight': R.kViolet}
    for selection in selections.keys():
        for var in variables:
            can.Clear()
            leg.Clear()
            histos = OrderedDict(sorted({k:h for k,h in histograms.iteritems()
                                         if k.startswith(selection) and k.endswith(var)}.items()))
            for h in histos.values():
                integral = h.Integral()
                if integral:
                    h.Scale(1.0/integral)
            pad_master = histos.values()[0]
            pad_master.SetMaximum(1.1*max(h.GetMaximum() for h in histos.values()))
            pad_master.SetMinimum(0.0)
            pad_master.SetTitle(selection + (var if var=='Discriminant' else
                                             ";log(p_{%s})"%var if plot_logp else
                                             "p_{%s}"%var))
            pad_master.Draw('axis')
            sig_histo = next(h for k, h in histos.iteritems() if signal_label in k)
            for k, h in histos.iteritems():
                samplename, samplecolor = next((cn, cv) for cn, cv in colors.iteritems() if cn in h.GetName())
                h.SetLineColor(samplecolor)
                h.SetMarkerColor(samplecolor)
                h.Draw('same')
                separation = R.TMVA.gTools().GetSeparation(sig_histo, h)
                print "separation %s %s : %f" % (sig_histo.GetName(), h.GetName(), R.TMVA.gTools().GetSeparation(sig_histo, h))
                leg.AddEntry(h,
                             ("{} ".format(samplename) +
                              ": {:.2E} entries".format(h.GetEntries()) +
                              (", mean: %.3f" % (samplename, h.GetEntries(), h.GetMean()) if not print_separation else
                               ", S={:.3f}".format(separation) if separation else
                               '')),
                             'l')
            leg.Draw()
            tex.DrawLatex(0.25, 0.95, selection)
            can.Update()
            img_filename = ('discriminant' if var=='Discriminant' else
                            ('log_'+var) if plot_logp else var)+'_'+selection
            can.SaveAs(img_filename+'.png')
            can.SaveAs(img_filename+'.eps')
    
if __name__=='__main__':
    main()
