import os
import string
import time, getpass
import socket
import sys
import datetime
from TTH_Samples import *

#sys.path.append("../../IFAETopFramework/python/")
sys.path.append( os.getenv("ROOTCOREBIN") + "python/IFAETopFramework/" )
from BatchTools import *
from Job import *
from Samples import *

##________________________________________________________
## OPTIONS
debug=False
nFilesSplit = 20 #number of files to merge in a single run of the code (chaining them)
nMerge = 20 #number of merged running of the code in a single job
channels = ["./.",]

useMiniIsolation = False
usePileUpWeight = True

HFIsSplitted = True
HFFilter = "NOFILTER"######default
DataFilter = "NOFILTER"
#DataFilter = "2015"
#DataFilter = "2016"
#param_applyTtbbCorrection = True
#param_applyTopPtWeight = False
#btagOP = "85"
#doBlind = False
#dumpHistos = True
#splitVLQDecays = True
#doTruthAnalysis = False
#applyMetMtwCuts = False
#param_useWeightSyst = False
#param_useObjectSyst = False
#param_onlyDumpSystHistos = False
#if True: #activate for systematics
    #param_useWeightSyst = True
    #param_useObjectSyst = True
    #param_onlyDumpSystHistos = True
param_produceTarBall = True
#param_produceTarBall = False

param_diffFiles = False #object systematics in different files (True) or different trees (False)
##........................................................
 
##________________________________________________________
## Defines some useful variables
platform = socket.gethostname()
now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")
here = os.getcwd()
##........................................................
 
##________________________________________________________
## Defining the paths and the tarball
#inputDir="/nfs/at3/scratch2/lvalery/VLQFiles/MBJ-00-01-16-2015_11/" #place where to find the input files
#inputDir="/nfs/at3/scratch2/mcasolino/ttHbbFramework/Ntuples_20_11/"
#inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-12-02Production/"
#inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-12-02Production/"
#inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-13-01/"
inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-15-01/"

#outputDir = "/nfs/at3/scratch2/lvalery/VLQAnalysisRun2/VLQAnalysisOutputs_" #output repository
#outputDir = "/nfs/pic.es/user/r/rodina/TTHAnalysisOutput"
#outputDir = "/nfs/pic.es/user/r/rodina/TTHAnalysisOutput"
outputDir = "/nfs/at3/scratch2/rodina/TTHAnalysisOutput"

#if useMiniIsolation:
    #outputDir += "MI_"
#if usePileUpWeight:
    #outputDir += "PURW_"
#if applyMetMtwCuts:
    #outputDir += "METMT_"
#if param_applyTtbbCorrection:
    #outputDir += "ttbbCorrection_"
#if param_applyTopPtWeight:
    #outputDir += "top_ptCorr_"
#if param_useObjectSyst:
    #outputDir += "objSyst_"
#if param_useWeightSyst:
    #outputDir += "wgtSyst_"
#outputDir += "btagOP"+btagOP
outputDir += "_"+now

#print("here "+here)
packageName="NewAnalysisFramework" #name of the folder containing all the installation
listFolder=here+"/Lists_Analysis_" + outputDir.split("/")[len(outputDir.split("/"))-1] #name of the folder containing the file lists
scriptFolder=here+"/Scripts_Analysis_" + outputDir.split("/")[len(outputDir.split("/"))-1] #name of the folder containing the scripts
#tarballPath="/nfs/at3/scratch2/lvalery/AnaCode_forBatch.tgz" # path to the tarball
#tarballPath="/nfs/pic.es/user/r/rodina/ttH/AnaCode_forBatch.tgz" # path to the tarball
#tarballPath="/nfs/pic.es/user/r/rodina/ttH/AnaCode_2.tgz" # path to the tarball
##........................................................
#print("outputDir"+outputDir)
#print("listFolder "+listFolder)
#print("scriptFolder "+scriptFolder)
##________________________________________________________
## Creating usefull repositories
os.system("mkdir -p " + listFolder) #list files folder
os.system("mkdir -p " + scriptFolder) #script files folder
os.system("mkdir -p " + outputDir) #output files folder
##........................................................
 ##________________________________________________________
## Creating tarball
#tarballPath="/nfs/at3/scratch2/lvalery/AnaCode_forBatch.tgz" # path to the tarball
if param_produceTarBall:
    tarballPath = outputDir + "/AnaCode_forBatch.tgz"
    if not os.path.exists(tarballPath):
        prepareTarBall("/nfs/pic.es/user/r/rodina/ttH",tarballPath)
        #prepareTarBall("/nfs/at3/scratch2/rodina/",tarballPath)

##........................................................
##________________________________________________________
## Getting all samples and their associated weight/object systematics
##
Samples = []

if HFIsSplitted == True: Samples += GetTtbarSamples ( ttbarSystSamples = False, hfSplitted = True )
else:  Samples += GetTtbarSamples ( ttbarSystSamples = False, hfSplitted = False )
Samples += GetTtHSamples()
Samples += GetOtherSamples()


#Samples += GetTtbarSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst, ttbarSystSamples = False, hfSplitted = True )
#Samples += GetVLQSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst )
#Samples += GetOtherSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst )
#Samples += GetUEDRPPSamples ( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst )
#Samples += Get4TopsSamples( useWeightSyst = param_useWeightSyst, useObjectSyst = param_useObjectSyst)
Samples += GetDataSamples()
printGoodNews("--> All samples recovered")
##........................................................
 
##________________________________________________________
## Defines a text file to checks all outputs are produced
chkFile = open(scriptFolder+"/JobCheck.chk","write")
##........................................................
 
##________________________________________________________
## Loop over channels
printGoodNews("--> Performing the loop over samples and jobs")
nJobs = 0
for channel in channels:
   
    ##________________________________________________________
    ## Loop over samples
    for sample in Samples:
       
        SName = sample['name'] # sample name
        SType = sample['sampleType'] # sample type (first argument of the getSamplesUncertainties())
        #print("SName = "+SName)
        #print("SType = "+SType)
        if (HFIsSplitted == True and SType == "ttbarlight"): HFFilter = "TTBARLIGHT"
        elif (HFIsSplitted == True and SType == "ttbarbb"): HFFilter = "TTBARBB"
        elif (HFIsSplitted == True and SType == "ttbarcc"): HFFilter = "TTBARCC"
        else: HFFilter = "NOFILTER"
        #else:
            #printError("Something is wrong with ttbar splitting!")
            #continue
            
        iDir = inputDir
        excluded = []
        #joblist = getSampleJobs(sample,InputDir=iDir+"/"+channel+"/",NFiles=nFilesSplit,UseList=False,ListFolder=listFolder,exclusions=excluded,useDiffFilesForObjSyst=param_diffFiles)
        joblist = getSampleJobs(sample,InputDir=iDir,NFiles=nFilesSplit,UseList=False,ListFolder=listFolder,exclusions=excluded,useDiffFilesForObjSyst=param_diffFiles)
        if(not joblist):
            continue
   
        #Setting caracteristics for the JobSet object
        JOSet = JobSet(platform)
        JOSet.setScriptDir(scriptFolder)
        JOSet.setLogDir(outputDir)
 
        JOSet.setTarBall(tarballPath)#tarball sent to batch (contains all executables)

        
        #JOSet.setPackageName(packageName)#name of the first repository in the tarball (containing all the packages)
        JOSet.setJobRecoveryFile(scriptFolder+"/JobCheck.chk")
        JOSet.setQueue("at3_short")
 
        ##________________________________________________________
        ## Loop over jobs for this sample (multiple files or systematics)
        for iJob in range(len(joblist)):
 
            ## Declare the Job object (one job = one code running once)
            jO = Job(platform)
       
            ## Name of the executable you want to run
            jO.setExecutable("runTTH")
            jO.setDebug(False)
 
            name=""
            if(SType.find("ttbar")>-1 or SType.find("VLQ")>-1):
                name += SName+SType
            else:
                name += SName
     
            name += "_"+joblist[iJob]['objSyst']+"_"+`iJob` #name of the job
            #name += "_"+joblist[iJob]['objSyst'] #name of the job

            jO.setName(name)
                   
            ## SETTING OF THE JOB (inputs, outputs, ...)
            OFileName = "out"+jO.execName+"_"+name+".root"
            jO.addOption("outputFile",OFileName) #name of the output file
            jO.addOption("inputFile",joblist[iJob]['filelist']) #name of the input file (already got from ls)
            jO.addOption("textFileList","false")
            jO.addOption("sampleName",sample['sampleType'])
            jO.addOption("useLeptonsSF","true")
            jO.addOption("addUnderflow","false")
            print("OFileName "+OFileName)
            
            if(HFIsSplitted):
                jO.addOption("HFfilterType", HFFilter)
            #print("HFFilter = ")
            #print(HFFilter)

            # Only systematic histograms
            #if param_onlyDumpSystHistos:
                #jO.addOption("onlyDumpSystHistograms","true")
           
            #b-tag OP
            #jO.addOption("btagOP",btagOP)
 
            #blind Option
            #if doBlind:
                #jO.addOption("doBlind","true")
            #else:
                #jO.addOption("doBlind","false")
 
            #if SType.find("ttbar")>-1 :
                #ttbb correction
                #if param_applyTtbbCorrection:
                    #jO.addOption("applyTtbbCorrection","true")
           
                #top pT weight
                #if param_applyTopPtWeight:
                    #if SType.find("ttbarbb")>-1 and param_applyTtbbCorrection:
                        #jO.addOption("useTopPtWeight","false")
                    #else:
                        #jO.addOption("useTopPtWeight","true")
                #else:
                    #jO.addOption("useTopPtWeight","false")
 
            # Weight systematics
            #if param_useWeightSyst:
                #jO.addOption("computeWeightSys","true")
                #jO.addOption("sysName",joblist[iJob]['weightSyst'])
 
            # Mini-isolation flags
            #if(useMiniIsolation):
                #jO.addOption("useMiniIsolation","true")
            #else:
                #jO.addOption("useMiniIsolation","false")
           
            # Pile-up reweighting
            #if(usePileUpWeight):
                #jO.addOption("usePUWeight","true")
            #else:
                #jO.addOption("usePUWeight","false")
           
            # Splitting VLQ decay types
#            if(splitVLQDecays):
                #jO.addOption("splitVLQDecays","true")
            #else:
                #jO.addOption("splitVLQDecays","false")
           
            # Dump histos
            #if(dumpHistos):
                #jO.addOption("dumpHistos","true")
            #else:
                #jO.addOption("dumpHistos","false")
           
            #Apply MET and MTW cuts (reject QCD)
            #if(applyMetMtwCuts):
                #jO.addOption("applyMetMtwCuts","true")
            #else:
                #jO.addOption("applyMetMtwCuts","false")
           
            # Truth analysis
            #if(doTruthAnalysis and SType.upper().find("DATA")==-1):
                #jO.addOption("doTruthAnalysis","true")
            #else:
                #jO.addOption("doTruthAnalysis","false")
           
            jO.setOutDir(outputDir)
            jO.addOption("sampleID",SName)
            jO.addOption("inputTree","nominal_Loose")
            ####################################################################################################################
            # Input tree name (might change for systematics)
            #if not param_diffFiles:
                #jO.addOption("inputTree",joblist[iJob]['objectTree'])
                #print("=> I am in not param_diffFiles")

            #else:
                #jO.addOption("inputTree","nominal")
                #jO.addOption("inputTree","nominal_Loose")
                #print("=> I am in inputTree,nominal_Loose")
                #######################################################################################################



            # isData flag
            if(sample['sampleType'].upper().find("DATA")>-1):
                jO.addOption("isData","true")
            else:
                jO.addOption("isData","false")
           
            ## SKIPPING ALREADY PROCESSED FILES
            if(os.path.exists(outputDir+"/"+OFileName)):
                printWarning("=> Already processed: skipping")
                continue
 
            chkFile.write(outputDir+"/"+OFileName+"\n")
           
            JOSet.addJob(jO)
   
            #if ( JOSet.size()==nMerge or joblist[iJob]['objSyst'].upper()=="NOMINAL" ):
            if ( JOSet.size()==nMerge or joblist[iJob]['objSyst'].upper()=="NOMINAL_LOOSE" ):
                JOSet.writeScript()
                JOSet.submitSet()#####
                JOSet.clear()
 
        if(JOSet.size()>0):
            JOSet.writeScript()
            JOSet.submitSet()###########
            JOSet.clear()
##........................................................
 
chkFile.close()
