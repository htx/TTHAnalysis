import sys
import os
import time, getpass
import datetime
from ROOT import *
#sys.path.append( os.getenv("ROOTCOREBIN") + "python/IFAETopFramework/" )
sys.path.append("../../IFAETopFramework/python/")
from BatchTools import *
from Job import *
from TTH_Samples import *
from Samples import *

now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")

## OPTIONS
debug=True
nFilesSplit = 1000
nMerge=1
#channels = ["./."]
wgtSysSplit=False

## Defines some useful variables
platform = socket.gethostname()
now = datetime.datetime.now().strftime("%Y_%m_%d_%H%M")
here = os.getcwd()

#inputDir="/nfs/at3/scratch2/mcasolino/ttHbbFramework/Ntuples_20_11/"
#inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-12-02Production/"
#inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-13-01/"
inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-15-01/"

#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_04_1116"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_06_1721"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_07_1357"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_08_0959"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_08_2028"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_09_1714"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_10_1700"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_11_1032"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_11_2251"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_12_1635"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_13_1634"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_17_1813"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_19_1214"
#listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_20_1312"
listFolder=here+"/Lists_Analysis_TTHAnalysisOutput_2016_07_22_1547"

# Creating the config file
configFile = open("configFile_"+now+".dat","w")
configFile.write("#\n#\n")
#configFile.write("# Path to files: " + inputDir + "\n")
configFile.write("# Date: " + now + "\n")
configFile.write("#\n#\n")

Samples = []
#Samples += GetTtbarSamples ( ttbarSystSamples = False, hfSplitted = False )
Samples += GetTtbarSamples ( ttbarSystSamples = False, hfSplitted = True )
Samples += GetTtHSamples()
Samples += GetOtherSamples()
Samples += GetDataSamples()


#Samples += GetTtbarSamples(hfSplitted=False,ttbarSystSamples=True,useHTSlices=True)
#for channel in channels:
    #print("lalala!")
## Loop over samples
#for sample in Samples:
        
    #SName = sample['name'] # sample name
    #print(SName)

for sample in Samples:#############loop dsid
    #print(sample)
    SName = sample['name'] # sample name
    SType = sample['sampleType']
    #print ("SName = "+SName)
    #print ("SType = "+SType)
    #iDir = inputDir
    excluded = []
    #print ("debug1 ")

    joblist = getSampleJobs(sample,InputDir=inputDir,NFiles=nFilesSplit,UseList=False,ListFolder=listFolder,exclusions=excluded,useDiffFilesForObjSyst=False)
    #print ("debug2 ")
    if(len(joblist)!=1):
        print ("The job for sample "+SName+" isn't standard: please check")
        #printError("<!> The job for sample "+SName+" isn't standard: please check")
        #continue
    if(len(joblist)<1):
        print("The job for sample "+SName+"not found!")
        continue
    #print ("debug3 ")
    #print(xsfile)
    
    files = joblist[0]['filelist'].split(",")
    
    nEventsWeighted = 0
    crossSection = -1
    dsid = SName

    #print ("debug4 ")

    print ("SName = "+SName)
    print ("SType = "+SType)

    #if dsid == "410000.": name = "1_ttbar"
    if SType == "ttbarbb": name = "3_ttbarbb"
    elif SType == "ttbarcc": name = "2_ttbarcc"
    elif SType == "ttbarlight": name = "1_ttbarlight"
    #elif SType == "bfilter": name = "3_ttbarbb"
    elif SType == "Singletop": name = "4_singletop"
    elif SType == "W+jets": name = "5_Wjets"
    elif SType == "Z+jets": name = "6_Zjets"
    elif SType == "Dibosons": name = "7_Dibosons"
    elif SType == "ttV": name = "8_ttV"
    elif SType == "ttH": name = "9_ttH"
    elif SType == "Data":
        #name = "0_Data"
        continue
    #elif dsid == "410011." or "410012." or "410013." or "410014.": name = "2_singletop"
    #print ("name= "+name)
    
    a, b, c, d, e, f, g = dsid
    dsid2 = a, b, c, d, e, f
    #print (dsid2)
    dsidsearchstring = ''.join(dsid2)
    #print ("dsidsearchstring:")
    #print (dsidsearchstring)

    #xsfile = open("XSection-MC15-13TeV-fromSusyGrp.data.txt","read")
    xsfile = open("XSection-MC15-13TeV.data","read")

    #print(xsfile)
    #print xsfile.readline( )
    #print xsfile.readline( )
    #print (dsid)
    #print "Going to search in text file for dsidsearchstring"
         
    #print (dsidsearchstring)
    searchlines = xsfile.readlines()
    xsfile.close()
      
    for i, line in enumerate(searchlines):
        if dsidsearchstring in line:
            #for l in searchlines[i]:print l,
            #print "Found dsidsearchstring!!!"
            #print searchlines[i]
            #print "Going to split!!!"

            dsidthisline, xsthisline, kfactorthisline =  searchlines[i].split()
            #print(xsthisline)
            xs = xsthisline
            kfactor=kfactorthisline
            xskfactor = float(xs)*float(kfactor)
            print("xs= "+xs)
            print("kfactor= "+kfactor)
            print("xskfactor= "+str(xskfactor))
            continue
    if xs == "":
       printError("<!> The job for sample "+SName+" isn't standard: please check")
       
    nEventsWeightedFullSample = 0
    
    for f in files:########loop per files to take N events
        print("f= "+f)
        root_f = TFile(f,"read")
        
        if(root_f.IsZombie()):
            printError("<!> The file "+f+" is corrupted ... removes it from the list")
            continue
        #print(root_f)

        #h_nEvents = root_f.Get("ejets/cutflow_mc_pu_zvtx").Clone()
        h_nEvents = root_f.Get("ejets_2016/cutflow_mc_pu_zvtx_Loose").Clone()

        #h_nEvents_2016 = root_f.Get("ejets_2016/cutflow_mc_pu_zvtx_Loose").Clone()
        #h_nEvents_2015 = root_f.Get("ejets_2015/cutflow_mc_pu_zvtx_Loose").Clone()
        h_nEvents.SetDirectory(0)

        #h_nEvents_2016.SetDirectory(0)
        #h_nEvents_2015.SetDirectory(0)

        nEventsWeighted = 0
        #nEventsWeighted2015 = 0
        #nEventsWeighted2016 = 0
        nEventsWeighted=h_nEvents.GetBinContent(1)
        #nEventsWeighted2015 = h_nEvents_2015.GetBinContent(1)
        #nEventsWeighted2016=h_nEvents_2016.GetBinContent(1)
        # nEventsWeighted=nEventsWeighted2015+nEventsWeighted2016
        #print("nEventsWeighted2015 = ")
        #print(nEventsWeighted2015)
        #print("nEventsWeighted2016 = ")
        #print(nEventsWeighted2016)
        #print("nEventsWeighted = ")
        #print(nEventsWeighted)
        
        nEventsWeightedFullSample += nEventsWeighted
        #print("nEventsWeightedFullSample = ")
        #print(nEventsWeightedFullSample)
        
        
                #for word in searchlines[i].split():
                    #print(word)
        
    #configFile.write(str(dsid)+" "+str(nEventsWeightedFullSample)+" "+str(xs)+" "+name+"\n")
    configFile.write(str(dsid)+" "+str(nEventsWeightedFullSample)+" "+str(xskfactor)+" "+name+"\n")
 
        #if dsidsearchstring in xsfile:
        #if "410000" in line: print "Found dsidsearchstring!!!"
            #print "Found dsidsearchstring!!!"
            #print (dsidsearchstring)

        
        #if word in mystring: 
   #print 'success'
      


        #for i in range(0, 3):
            #configFile.write("a"+" "+"b"+" "+"c"+"\n")

            #print(nEventsWeighted)
#for TTH_Sample in TTH_Samples:
##print(TTH_Sample)
    

#rootfilename="/nfs/at3/scratch2/mcasolino/ttHbbFramework/Ntuples_20_11/user.mcasolin.410000.PowhegPythiaEvtGen.DAOD_TOPQ1.e3698_s2608_s2183_r6765_r6282_p2442.13-11-1_output.root/user.mcasolin.6978538._000001.output.root"
#rootfile=TFile(rootfilename,"read")
#h_nEventsTest = rootfile.Get("ejets/cutflow_mc_pu_zvtx").Clone()
#h_nEventsTest.SetDirectory(0)
#nEventsWeightedTest = 0
#nEventsWeightedTest = h_nEventsTest.GetBinContent(1)

#print(nEventsWeightedTest)

#h_nEventsTest2 = rootfile.Get("ejets/cutflow").Clone()
#h_nEventsTest2.SetDirectory(0)
#nEventsWeightedTest2 = 0
#nEventsWeightedTest2 = h_nEventsTest2.GetBinContent(1)

#print(nEventsWeightedTest2)
