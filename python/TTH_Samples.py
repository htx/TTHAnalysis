#!/bin/python
 
import sys
sys.path.append("../../IFAETopFramework/python/")
 
from BatchTools import *
from Samples import *
 
##______________________________________________________________________
##
## Object systematics
##
## Nominal
CommonObjectSystematics =  []
CommonObjectSystematics += [getSystematics(name="nominal",nameUp="nominal",oneSided=True)] # the nominal is considered as a systematic variation

 
##______________________________________________________________________
##
## Common weight systematics
##
## b-tagging
#CommonWeightSystematics =  []
#CommonWeightSystematics += [getSystematics(name="BTAG_B_EV1",nameUp="BTAG_B_EV1_UP",nameDown="BTAG_B_EV1_DOWN",oneSided=False)]

##______________________________________________________________________
##
## ttbb weight systematics
##

 
##______________________________________________________________________
##
def GetTtbarSamples( useWeightSyst=False, useObjectSyst=False, hfSplitted=True, style="simple", ttbarSystSamples=False):
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
        WeightSystematics += TtbarWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples = []
    if not hfSplitted:
        Samples     += [getSampleUncertainties("ttbar",             "410000.", ObjectSystematics , WeightSystematics)]
        if ttbarSystSamples:
            Samples     += [getSampleUncertainties("ttbar_radHi",       "410001.", ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbar_radLow",      "410002.", ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbar_aMCHer",      "410003.", ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbar_PowHer",      "410004.", ObjectSystematics , WeightSystematics)]
    else:
        if style=="simple":
            Samples     += [getSampleUncertainties("ttbarlight","410000.", ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbarbb","410000.",ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbarcc","410000.",ObjectSystematics , WeightSystematics)]
            #Samples     += [getSampleUncertainties("bfilter","410120.",ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbarlight","410120.",ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbarbb","410120.",ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("ttbarcc","410120.",ObjectSystematics , WeightSystematics)]

            if ttbarSystSamples:
                Samples     += [getSampleUncertainties("ttbarlight_radHi","410001.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties("ttbarbb_radHi","410001.",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties("ttbarcc_radHi","410001.",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties("ttbarlight_radLow","410002.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties("ttbarbb_radLow","410002.",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties("ttbarcc_radLow","410002.",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties("ttbar_aMCHer", "410003.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
                Samples     += [getSampleUncertainties("ttbar_PowHer", "410004.", [getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
        else:
            Samples     += [getSampleUncertainties("t#bar{t}+light","ttbarlight", ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("t#bar{t}+bb","ttbarbb",ObjectSystematics , WeightSystematics)]
            Samples     += [getSampleUncertainties("t#bar{t}+cc","ttbarcc",ObjectSystematics , WeightSystematics)]
 
    return Samples
##______________________________________________________________________
##



##______________________________________________________________________
##
def GetTtHSamples( useWeightSyst=False, useObjectSyst=False, name="ttH"):
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
    Samples =  []
    Samples     += [getSampleUncertainties(name,"343365.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"343366.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"343367.", ObjectSystematics , WeightSystematics)]
        #Samples     += [getSampleUncertainties(name,"341177.", ObjectSystematics , WeightSystematics)]
        #Samples     += [getSampleUncertainties(name,"341270.", ObjectSystematics , WeightSystematics)]
        #Samples     += [getSampleUncertainties(name,"341271.", ObjectSystematics , WeightSystematics)]
    return Samples








##______________________________________________________________________
##
def GetOtherSamples( useWeightSyst=False, useObjectSyst=False):
   
    Samples =  []
    Samples += GetWSamples( useWeightSyst, useObjectSyst )
    Samples += GetZSamples( useWeightSyst, useObjectSyst )
    Samples += GetSingleTopSamples( useWeightSyst, useObjectSyst )
    #Samples += GetTopEWSamples( useWeightSyst, useObjectSyst )
    Samples += GetDibosonSamples( useWeightSyst, useObjectSyst )
    Samples += GetTtVSamples( useWeightSyst, useObjectSyst )

    #   Samples += GetDijetSamples( )
    #   Samples += Get4TopsSamples( useWeightSyst, useObjectSyst )
    return Samples
 
##______________________________________________________________________
##
def GetWSamples( useWeightSyst=False, useObjectSyst=False, name = "W+jets"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples =  []
    Samples     += [getSampleUncertainties(name,"361300.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361301.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361302.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361303.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361304.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361305.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361306.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361307.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361308.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361309.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361310.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361311.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361312.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361313.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361314.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361315.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361316.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361317.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361318.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361319.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361320.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361321.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361322.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361323.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361324.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361325.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361326.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361327.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361328.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361329.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361330.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361331.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361332.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361333.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361334.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361335.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361336.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361337.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361338.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361339.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361340.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361341.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361342.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361343.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361344.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361345.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361346.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361347.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361348.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361349.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361350.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361351.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361352.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361353.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361354.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361355.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361356.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361357.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361358.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361359.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361360.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361361.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361362.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361363.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361364.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361365.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361366.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361367.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361368.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361369.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361370.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361371.", ObjectSystematics , WeightSystematics)]
    return Samples
 
##______________________________________________________________________
##
def GetZSamples( useWeightSyst=False, useObjectSyst=False, name = "Z+jets"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples =  []
   
    Samples     += [getSampleUncertainties(name,"361372.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361373.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361374.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361375.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361376.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361377.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361378.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361379.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361380.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361381.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361382.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361383.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361384.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361385.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361386.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361387.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361388.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361389.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361390.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361391.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361392.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361393.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361394.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361395.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361396.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361397.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361398.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361399.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361400.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361401.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361402.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361403.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361404.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361405.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361406.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361407.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361408.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361409.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361410.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361411.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361412.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361413.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361414.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361415.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361416.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361417.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361418.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361419.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361420.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361421.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361422.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361423.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361424.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361425.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361426.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361427.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361428.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361429.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361430.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361431.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361432.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361433.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361434.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361435.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361436.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361437.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361438.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361439.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361440.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361441.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361442.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361443.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361468.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361469.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361470.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361471.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361472.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361473.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361474.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361475.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361476.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361477.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361478.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361479.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361480.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361481.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361482.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361483.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361484.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361485.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361486.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361487.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361488.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361489.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361490.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361491.", ObjectSystematics , WeightSystematics)]

    #Samples     += [getSampleUncertainties(name,"361444.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361455.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361458.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361459.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361460.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361461.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361462.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361463.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361464.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361465.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361466.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361467.", ObjectSystematics , WeightSystematics)]
    return Samples
 
##______________________________________________________________________
##

def GetTtVSamples( useWeightSyst=False, useObjectSyst=False, name = "ttV"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
    Samples =  []
    Samples     += [getSampleUncertainties(name,"410050.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410215.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410217.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410066.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410067.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410068.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410073.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410074.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410075.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410111.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410112.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410113.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410114.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410115.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410116.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410080.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410081.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410155.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410156.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410157.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410218.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410219.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410220.", ObjectSystematics , WeightSystematics)]
    return Samples



##______________________________________________________________________
##
    

def GetSingleTopSamples( useWeightSyst=False, useObjectSyst=False, name = "Singletop"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples     =  []
    Samples     += [getSampleUncertainties(name,"410011.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410012.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410013.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410014.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410015.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"410016.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410025.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410026.", ObjectSystematics , WeightSystematics)]
    return Samples
 
##______________________________________________________________________
##
def GetDibosonSamples( useWeightSyst=False, useObjectSyst=False, name = "Dibosons"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples =  []
    Samples     += [getSampleUncertainties(name,"361063.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361064.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361065.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361066.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361067.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361068.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361069.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361070.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361071.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361072.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361073.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361077.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361091.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361092.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361093.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361094.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361095.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361096.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361097.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361085.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361086.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361087.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361083.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361084.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361082.", ObjectSystematics , WeightSystematics)]
    #Samples     += [getSampleUncertainties(name,"361081.", ObjectSystematics , WeightSystematics)]
    return Samples
 
##______________________________________________________________________
##
def GetTopEWSamples( useWeightSyst=False, useObjectSyst=False, name = "topEW"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples     =  []
    Samples     += [getSampleUncertainties(name,"341177.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410066.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410067.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410068.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410069.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410070.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410073.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410074.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"410075.", ObjectSystematics , WeightSystematics)]
    return Samples
 
##______________________________________________________________________
##
def GetDijetSamples( useWeightSyst=False, useObjectSyst=False, name = "Dijets"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples     =  []
    Samples     += [getSampleUncertainties(name,"361020.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361021.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361022.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361023.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361024.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361025.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361026.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361027.", ObjectSystematics , WeightSystematics)]
    Samples     += [getSampleUncertainties(name,"361028.", ObjectSystematics , WeightSystematics)]
    return Samples
 
##______________________________________________________________________
##
def Get4TopsSamples( useWeightSyst=False, useObjectSyst=False, name = "4tops"):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples     =  []
    Samples     += [getSampleUncertainties(name,"410080.", ObjectSystematics , WeightSystematics)]
    return Samples
 
##______________________________________________________________________
##
def GetVLQSamples( useWeightSyst=False, useObjectSyst=False):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
   
    Samples =  []
    Samples     += [getSampleUncertainties("VLQ_TT_600","302469.",  ObjectSystematics , WeightSystematics)]#TT 600
    Samples     += [getSampleUncertainties("VLQ_TT_700","302470.",  ObjectSystematics , WeightSystematics)]#TT 700
    Samples     += [getSampleUncertainties("VLQ_TT_750","302471.",  ObjectSystematics , WeightSystematics)]#TT 750
    Samples     += [getSampleUncertainties("VLQ_TT_800","302472.",  ObjectSystematics , WeightSystematics)]#TT 800
    Samples     += [getSampleUncertainties("VLQ_TT_850","302473.",  ObjectSystematics , WeightSystematics)]#TT 850
    Samples     += [getSampleUncertainties("VLQ_TT_900","302474.",  ObjectSystematics , WeightSystematics)]#TT 900
    Samples     += [getSampleUncertainties("VLQ_TT_950","302475.",  ObjectSystematics , WeightSystematics)]#TT 950
    Samples     += [getSampleUncertainties("VLQ_TT_1000","302476.", ObjectSystematics , WeightSystematics)]#TT 1000
    Samples     += [getSampleUncertainties("VLQ_TT_1050","302477.", ObjectSystematics , WeightSystematics)]#TT 1050
    Samples     += [getSampleUncertainties("VLQ_TT_1100","302478.", ObjectSystematics , WeightSystematics)]#TT 1100
    Samples     += [getSampleUncertainties("VLQ_TT_1150","302479.", ObjectSystematics , WeightSystematics)]#TT 1150
    Samples     += [getSampleUncertainties("VLQ_TT_1200","302480.", ObjectSystematics , WeightSystematics)]#TT 1200
    Samples     += [getSampleUncertainties("VLQ_TT_1300","302481.", ObjectSystematics , WeightSystematics)]#TT 1300
    Samples     += [getSampleUncertainties("VLQ_TT_1400","302482.", ObjectSystematics , WeightSystematics)]#TT 1400
    return Samples
 
##______________________________________________________________________
##
def GetUEDRPPSamples( useWeightSyst=False, useObjectSyst=False):
   
    ObjectSystematics = []
    WeightSystematics = []
    if useWeightSyst:
        WeightSystematics += CommonWeightSystematics
    if useObjectSyst:
        ObjectSystematics += CommonObjectSystematics
    else:
        ObjectSystematics = [getSystematics(name="nominal",nameUp="",oneSided=True)]
 
    Samples =  []
    Samples     += [getSampleUncertainties("UEDRPP_1000","302055.", ObjectSystematics , WeightSystematics)]#UED RPP 1 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1200","302056.", ObjectSystematics , WeightSystematics)]#UED RPP 1.2 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1400","302057.", ObjectSystematics , WeightSystematics)]#UED RPP 1.4 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1600","302058.", ObjectSystematics , WeightSystematics)]#UED RPP 1.6 TeV
    Samples     += [getSampleUncertainties("UEDRPP_1800","302059.", ObjectSystematics , WeightSystematics)]#UED RPP 1.8 TeV
    return Samples
 
##______________________________________________________________________
##
def GetDataSamples():
   
    Samples     =  []
    #Samples     += [getSampleUncertainties("Data","DATA.",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]
    Samples     += [getSampleUncertainties("Data","physics_Main.",[getSystematics(name="nominal",nameUp="",oneSided=True)] ,[])]

    return Samples;
