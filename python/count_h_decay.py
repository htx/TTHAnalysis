#!/bin/env python

"""Count the Higgs decay for each selection region


davide.gerbaudo@gmail.com
Feb 2017
"""

import sys
import pprint
from collections import defaultdict, Counter
import ROOT as R
R.gROOT.SetBatch(1)

def main():
    if len(sys.argv)!=2:
        print "Usage: %s input_file.root"
        print "example input file:"
        print ("BLAH")
        print 'len sys argv ',len(sys.argv)
        print '\n'.join(sys.argv)
        return
    input_filename = sys.argv[1]
    tree_name = 'nominal'
    max_entries_to_process = -1 # 1000 # -1 : process all

    input_tree = R.TChain(tree_name)
    input_filemames = [input_filename if input_filename.strip().endswith('.root') else
                       l.strip() for l in open(input_filename).readlines()
                       if l.strip().endswith('.root')]
    for in_fn in input_filemames:
        input_tree.Add(in_fn)

    counts = defaultdict(Counter)
    iEntry = 0
    for iEntry in xrange(input_tree.GetEntries()):
        input_tree.GetEntry(iEntry)
        jets_pt   = input_tree.jets_pt
        jets_eta  = input_tree.jets_eta
        jets_phi  = input_tree.jets_phi
        jets_btag = input_tree.jets_btag_weight
        jets_e    = input_tree.jets_e
        jets_n    = input_tree.jets_n
        mc_pdg    = input_tree.mc_pdgId
        mc_ch_ids = input_tree.mc_children_index

        jet_indices = [i for i, (pt, eta) in enumerate(zip(jets_pt, jets_eta))
                       if pt >= 25.0 and abs(eta)<2.5]
        bjet_indices = [i for i in jet_indices
                        if jets_btag[i] >= 0.8244273] # mv2c10 70OP
        n_j = len(jet_indices)
        n_b = len(bjet_indices)
        if n_j < 4 or n_b < 2:
            continue
        selection = ("6j4b"                if n_j>=6 and n_b>=4 else
                     "6j%db" % n_b         if n_j>=6 and n_b>=2 else
                     "%dj4b" % n_j         if n_j>=4 and n_b>=4 else
                     "%dj%db" % (n_j, n_b) if n_j>=4 and n_b>=2 else
                     "unknown")
        h_pdg = 25
        i_higgs = next((i for i, pdg in enumerate(mc_pdg) if pdg==h_pdg), None)
        if i_higgs is None:
            print "skip : no H this event"
            continue
        h_children_pdg = sorted([abs(int(mc_pdg[i])) for i in mc_ch_ids[i_higgs]])
        h_decay = ''.join(pdg2str(p) for p in h_children_pdg)

        print "%s : %s %s" % (selection, h_decay, str(h_children_pdg) if h_decay.count('?') or not h_children_pdg else '')
        counts[selection][h_decay] += 1
        if max_entries_to_process!=-1 and iEntry>max_entries_to_process:
            break
    print "processed %d entries" % iEntry
    pprint.pprint({s : dict(sc) for s, sc in counts.iteritems()})

def pdg2str(pdg):
    apdg = abs(pdg)
    l = ( 'd' if apdg==1 else
          'u' if apdg==2 else
          's' if apdg==3 else
          'c' if apdg==4 else
          'b' if apdg==5 else
          't' if apdg==6 else
          'e' if apdg==11 else
         've' if apdg==12 else
         'mu' if apdg==13 else
         'vm' if apdg==14 else
         'ta' if apdg==15 else
         'vt' if apdg==16 else
          'g' if apdg==21 else
          'y' if apdg==22 else
          'Z' if apdg==23 else
          'W' if apdg==24 else
          'h' if apdg==25 else
          '?')
    l = l if pdg > 0 else (l+'~')
    return l
#___________________________________________________________

if __name__=='__main__':
    main()
