#!/bin/env python

"""Make a table using the counter dict from count_Hdecay.py

Setup (requires python >=2.7):

virtualenv -p `which python` table2.7
cd table2.7/
source ./bin/activate
pip install tabulate
../../TTHAnalysis/python/make_table_hdecays.py ../h_decays_thq_dict.py

The input dict.py is something like (from count_Hdecay.py)
{'4j2b': {'WW': 1060,
          'ZZ': 280,
          'bb': 23482,
          'yy': 4},
 '4j3b': {'WW': 25,
          'ZZ': 42}}

Then the tex output can be put in a minimal doc like:

\documentclass{article}
\usepackage{booktabs}
\begin{document}
% table here
\end{document}

davide.gerbaudo@gmail.com
Feb 2017
"""

import sys
from collections import Counter, OrderedDict
import tabulate

def main():

    if len(sys.argv)!=2:
        print "Usage: %s counter_dict.py"
        return
    input_filename = sys.argv[1]
    counters = eval(open(input_filename).read())
    selections = sorted(counters.keys())
    print 'selections: ',selections
    [cnts.pop('') for cnts in counters.values() if '' in cnts] # drop '' counts due to h w/out children
    all_decay_counts = Counter()
    for sel, cnts in counters.iteritems():
        for d, c in cnts.iteritems():
            all_decay_counts[d] += c
    all_decays = sorted(all_decay_counts, key=all_decay_counts.get, reverse=True)
    tot_counts_selec = {sel : sum(counters_sel.values())
                        for sel, counters_sel in counters.iteritems()}
    print tot_counts_selec
    print 'all_decays: ',all_decays
    percentages = OrderedDict([(decay,
                                [(100.0 * cnts_sel[decay] / tot_counts_selec[selection]) if decay in cnts_sel else 0.0
                                 for selection, cnts_sel in counters.iteritems()])
                               for decay in all_decays])
    percentages_lists_txt = [[pretty_decay(k, 'txt')] + vals
                             for k, vals in percentages.iteritems()]

    print tabulate.tabulate(percentages_lists_txt, headers=['h decay']+selections, floatfmt=".1f")

    percentages_lists_tex = [["$%s$"%pretty_decay(k, 'tex')] + vals
                             for k, vals in percentages.iteritems()]
    tabulate.LATEX_ESCAPE_RULES={} # prevent from mangling math symbols; \tau still converted to tab
    # more details: http://stackoverflow.com/questions/35418787/generate-proper-latex-table-using-tabulate-python-package
    print tabulate.tabulate(percentages_lists_tex, headers=['h decay']+selections, floatfmt=".1f", tablefmt='latex_booktabs')

    # these are the counters formatted for the google chart sankey diagram
    # https://developers.google.com/chart/interactive/docs/gallery/sankey
    # -- for selection in selections:
    # --     cnts = counters[selection]
    # --     for decay in sorted(cnts, key=cnts.get, reverse=True):
    # --         print "['%s', '%s', %d]," % (decay, selection, cnts[decay])

def pretty_decay(decay, fmt):
    allowed_fmt = ['txt', 'tex']
    allowed_decays = ['bb', 'WW', 'tata', 'gg', 'ZZ', 'cc', 'yZ', 'yy', 'mumu', 'ss'] # todo 2017-02-08 investigate gg
    if fmt not in allowed_fmt:
        raise NotImplementedError("only txt and tex are valid choices, not '%s'" % fmt)
    if decay not in allowed_decays:
        raise NotImplementedError("decay not implemented '%s'" % decay)
    idx = allowed_fmt.index(fmt)
    rosetta = {'tata': ['tautau', '\tau{}\tau{}'],
               'yZ'  : ['yZ', '\gamma{}Z'],
               'yy'  : ['yy', '\gamma{}\gamma{}'],
               'mumu': ['mumu', '\mu{}\mu{}'],
               }
    pretty = rosetta[decay][idx] if decay in rosetta else decay
    return pretty

if __name__=='__main__':
    main()
