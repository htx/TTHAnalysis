#!/bin/env python

# Plot the matching fractions and the missing match for each selection
#
# davide.gerbaudo@gmail.com
# Nov 2017

import os
import re
import sys
from collections import OrderedDict

import ROOT as R
R.gROOT.SetBatch(1)

def main():

    """
    Example:

    EXE="TTHAnalysis/python/plot_matching.py"
    INDIR="/nfs/at3/scratch2/gerbaudo/fcnc/output/VLQAnalysisOutputs_test_VLQ_bare_2017_11_07_1654/"
    ${EXE} ${INDIR}/out*FcncTree.root out_match
    """

    input_filenames = guess_input_files(sys.argv[1:-1])
    output_dir = mkdir_if_needed(sys.argv[-1])
    if not output_dir:
        print "please provide a valid output directory"
        return

    tree_name = 'fcncTree'
    base_selection = R.TCut('4jin2bin', 'jets_n>=4 && bjets_n>=2')
    all_match_selection = R.TCut('all_match', 'nmatchedjet==4')
    miss1match_selection = R.TCut('miss1match', 'nmatchedjet==3')
    def is_sig(sample_name):
        fname = os.path.basename(sample_name)
        return 'uHb' in sample_name or 'uHc' in sample_name
    # fill histograms
    histograms_mfraction_bkg = {}
    histograms_mfraction_sig = {}
    histograms_missmatch_bkg = {}
    histograms_missmatch_sig = {}
    for sample, filenames in input_filenames.iteritems():
        if sample not in ['uHbW',
                          'uHcW',
                          'ttbarlight_410000']:
            continue
        sig_or_bkg = 'sig' if is_sig(sample) else 'bkg'
        plot_directory = mkdir_if_needed(os.path.join(output_dir, sig_or_bkg))
        chain = R.TChain(tree_name)
        for filename in filenames:
            chain.Add(filename)
        print("%d entries for %s" % (chain.GetEntries(), sample))
        histograms_mfraction = (histograms_mfraction_sig if is_sig(sample) else
                                histograms_mfraction_bkg)
        histograms_missmatch = (histograms_missmatch_sig if is_sig(sample) else
                                histograms_missmatch_bkg)
        all_kin_selections = [R.TCut('4jin2bin', 'jets_n>=4 && bjets_n>=2'), # 4j
                              R.TCut('4jex2bex', 'jets_n==4 && bjets_n==2'),
                              R.TCut('4jex3bex', 'jets_n==4 && bjets_n==3'),
                              R.TCut('4jex4bin', 'jets_n==4 && bjets_n>=4'),
                              R.TCut('5jex2bex', 'jets_n==5 && bjets_n==2'), # 5j
                              R.TCut('5jex3bex', 'jets_n==5 && bjets_n==3'),
                              R.TCut('5jex4bin', 'jets_n==5 && bjets_n>=4'),
                              R.TCut('6jin2bex', 'jets_n>=6 && bjets_n==2'), # 6j
                              R.TCut('6jin3bex', 'jets_n>=6 && bjets_n==3'),
                              R.TCut('6jin4bin', 'jets_n>=6 && bjets_n>=4')]
        for kin_selection in all_kin_selections:
            h = fill_Nmatch(chain,
                            kin_selection,
                            sample_label=sample)
            histograms_mfraction[h.GetName()] = h
            print "mfractions %s : %d " % (h.GetName(), h.GetEntries())
            h = fill_missing_match(chain,
                                   kin_selection,
                                   miss1match_selection,
                                   is_sig=is_sig(sample),
                                   is_bkg=not is_sig(sample),
                                   sample_label=sample,
                                   plot_directory=plot_directory)
            histograms_missmatch[h.GetName()] = h

        # draw matching fraction
        canvas = R.TCanvas('Matching fraction '+sample, '')
        canvas.cd()
        colors = [rainbowColors(0, len(all_kin_selections), i)
                  for i in range(len(all_kin_selections))]
        pad_master = None
        leg1 = R.TLegend(0.15, 0.55, 0.475, 0.875)
        tex = R.TLatex(0.0, 0.0, '')
        tex.SetNDC(True)
        for iSel, sel in enumerate(all_kin_selections):
            h = next((h for k,h in histograms_mfraction.iteritems()
                      if sel.GetName() in k and sample in k), None)
            if not h:
                print "missing matching frac histogram for ",sel.GetName()
                continue
            if not pad_master:
                pad_master = h
                pad_master.SetMaximum(1.0)
                pad_master.SetMinimum(0.0)
                pad_master.Draw('axis')
                pad_master.SetLineWidth(2*pad_master.GetLineWidth())
            h.SetLineWidth(2*h.GetLineWidth())
            h.SetLineColor(colors[iSel])
            h.SetMarkerColor(h.GetLineColor())
            def miss1_allfrac(h):
                all_match = h.GetBinContent(h.FindFixBin(4))
                miss1match = h.GetBinContent(h.FindFixBin(3))
                tot = all_match+miss1match
                return ((all_match / tot, miss1match / tot)
                        if tot else
                        (0.0, 0.0))
            am_frac, mm_frac = miss1_allfrac(h)
            leg1.AddEntry(h, sel.GetName()+" %.2E" % h.GetEntries()
                         +" (%.2f, %.2f)" % (am_frac, mm_frac),
                         'l')
            h.Draw('same')
        leg1.SetHeader('(all-match, miss-1-match)')
        leg1.Draw()
        tex.DrawLatex(0.25, 0.95, 'Number of matches: '+sample)
        canvas.Update()
        canvas.SaveAs(os.path.join(plot_directory,
                                   sample+'_matching_fraction'
                                   +'.png'))

        # draw missing match
        canvas = R.TCanvas('Missing match '+sample, '')
        canvas.cd()
        colors = [rainbowColors(0, len(all_kin_selections), i)
                  for i in range(len(all_kin_selections))]
        pad_master = None
        leg2 = R.TLegend(0.15, 0.55, 0.475, 0.875)
        tex = R.TLatex(0.0, 0.0, '')
        tex.SetNDC(True)
        for iSel, sel in enumerate(all_kin_selections):
            h = next((h for k,h in histograms_missmatch.iteritems()
                      if sel.GetName() in k and sample in k), None)
            if not h:
                print "missing matching histogram for %s %s " % (sample, sel.GetName())
                continue
            if not pad_master:
                pad_master = h
                pad_master.SetMaximum(1.0)
                pad_master.SetMinimum(0.0)
                pad_master.Draw('axis')
                pad_master.SetLineWidth(2*pad_master.GetLineWidth())
            h.SetLineWidth(2*h.GetLineWidth())
            h.SetLineColor(colors[iSel])
            h.SetMarkerColor(h.GetLineColor())
            leg2.AddEntry(h, sel.GetName()+" %.2E" % h.GetEntries(),
                         'l')
            h.Draw('same')
        leg2.Draw()
        tex.DrawLatex(0.25, 0.95, 'Missing match: '+sample)
        canvas.Update()
        canvas.SaveAs(os.path.join(plot_directory,
                                   sample+'_missing_match'
                                   +'.png'))

def guess_input_files(filenames=[]):
    "given filenames, guess which one is ttbarlight (bkg), thc, and thu"
    keys = ['uHbWm', 'uHbWp',
            'uHcWm', 'uHcWp',
            'ttbarlight_410000']
    counts = OrderedDict({k : sum(1 for f in filenames if k in os.path.basename(f)) for k in keys})
    print "identified %s input files" % ' '.join("%d %s"%(c, k) for k, c in counts.iteritems())

    filenames = OrderedDict({ k : [f for f in filenames if k in os.path.basename(f)]
                              for k in keys})
    filenames['uHbW'] = filenames['uHbWm'] + filenames['uHbWp']
    filenames['uHcW'] = filenames['uHcWm'] + filenames['uHcWp']
    return filenames


def fill_Nmatch(chain,
                kin_selection,
                sample_label=''):
    h = R.TH1F('reg_'+sample_label+'_'+kin_selection.GetName()+'_matchingcounts',
               'Matching counts for '+sample_label+', '+kin_selection.GetTitle(),
               5, -0.5, +4.5) # expect number of matches in [0,4]
    chain.Draw('nmatchedjet >> '+h.GetName(),
               kin_selection.GetTitle(),
               'goff')
    h.SetStats(False)
    integral = h.Integral()
    if integral:
        h.Scale(1.0/integral)
    return h

def fill_missing_match(chain, kin_selection, match_selection,
                       is_sig=False, is_bkg=False,
                       sample_label='',
                       plot_directory='./'):
    if match_selection.GetName()!='miss1match':
        print "The variable 'nottruthmatch' is well-defined only for 1 missing match"
        return
    if is_bkg==is_sig:
        print "Please specify whether this is signal or background"
        return
    h = R.TH1F('reg_'+sample_label+'_'
               +kin_selection.GetName()
               +'_'+match_selection.GetName()+'_missingmatch',
               'Missing match for '+sample_label,
               4, -0.5, 3.5)
    chain.Draw('nottruthmatch >> '+h.GetName(),
               (kin_selection+match_selection).GetTitle(),
               'goff')
    integral = h.Integral()
    if integral:
        h.Scale(1.0/integral)
    xAxis = h.GetXaxis()
    bin_labels = (['b1', 'b2', 'blep', 'j'] if is_sig else
                  ['q1', 'q2', 'blep', 'bhad'] if is_bkg else
                  [str(v) for v in range(xAxis.GetNbins())])
    for b,l in zip(range(1, 1+xAxis.GetNbins()),
                   bin_labels):
        xAxis.SetBinLabel(b, l)
    h.SetMinimum(0.0)
    h.SetMaximum(1.0)
    h.SetStats(False)
    return h

def mkdir_if_needed(dirname, verbose=False) :
    dest_dir = None
    if os.path.exists(dirname) and os.path.isdir(dirname) :
        dest_dir = dirname
    elif not os.path.exists(dirname) :
        os.makedirs(dirname)
        if verbose: print "created %s" % dirname
        dest_dir = dirname
    return dest_dir

def rainbowColors(minVal, maxVal, curVal):
    """Given a range between minVal and maxVal, return the color of
    the visible spectrum proportional to the curVal"""
    # make sure we're daling with numbers
    minVal = float(minVal)
    maxVal = float(maxVal)
    curVal = float(curVal)
    if minVal >= maxVal:
        print "rainbowColors: %s <=%s  (invalid minVal >= maxVal)" % (minVal,maxVal)
        return R.kBlack
    if curVal<minVal or curVal>maxVal:
        print "rainbowColors: value %s out of range [%s,%s]" % (curVal, minVal, maxVal)
        return R.kBlack
    colLo = 51
    colHi = 101
    return colLo + int((colHi-colLo)*(curVal-minVal)/(maxVal-minVal)+0.5)


if __name__=='__main__':
    main()
