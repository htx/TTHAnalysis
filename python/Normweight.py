import sys
import os
import time, getpass
import datetime
from ROOT import *
#sys.path.append( os.getenv("ROOTCOREBIN") + "python/IFAETopFramework/" )
sys.path.append("../../IFAETopFramework/python/")
from BatchTools import *
from Job import *
from TTH_Samples import *
from Samples import *

inputDir="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-15-01-TRF/user.jwebster.mc15_13TeV.410000.PoPy6_ttbar_lep.e3698s2608r7725p2669.TTHbbL-2-4-15-1.1l.v1_out.root/"
#inputDir410120="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-15-01-TRF/user.jwebster.mc15_13TeV.410120.PoPy6_ttbar_lep_bfilter.e4373s2608r7725p2669.TTHbbL-2-4-15-1.1l.v1_out.root/"

#inputDir410000="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-15-01-TRF/user.jwebster.mc15_13TeV.410000.PoPy6_ttbar_lep.e3698s2608r7725p2669.TTHbbL-2-4-15-1.1l.v1_out.root/"
#inputDir410120="/nfs/at3/scratch2/rodina/TTHbbLeptonic-02-04-15-01-TRF/user.jwebster.mc15_13TeV.410120.PoPy6_ttbar_lep_bfilter.e4373s2608r7725p2669.TTHbbL-2-4-15-1.1l.v1_out.root/"

ttbarfilelist = open("/nfs/pic.es/user/r/rodina/ttH/TTHAnalysis/run/list_ttbar-02-04-15-01.txt", "read")
nEventsWeightedFullSample = 0
searchlines = ttbarfilelist.readlines()
ttbarfilelist.close()
for i, line in enumerate(searchlines):
    #print searchlines[i][:-1]
    root_f = TFile(searchlines[i][:-1],"read")
    if(root_f.IsZombie()):
        printError("<!> The file "+searchlines[i]+" is corrupted ... removes it from the list")
        continue
    h_nEvents = root_f.Get("ejets_2016/cutflow_mc_pu_zvtx_Loose").Clone()
    h_nEvents.SetDirectory(0)
    nEventsWeighted = 0
    nEventsWeighted=h_nEvents.GetBinContent(1)
    nEventsWeightedFullSample += nEventsWeighted
ttbarnEventsWeightedFullSample=nEventsWeightedFullSample
print("ttbarnEventsWeightedFullSample = ")
print(ttbarnEventsWeightedFullSample)

bfilterfilelist = open("/nfs/pic.es/user/r/rodina/ttH/TTHAnalysis/run/list_bfilter-02-04-15-01.txt", "read")
nEventsWeightedFullSample =0
searchlines = bfilterfilelist.readlines()
bfilterfilelist.close()
for i, line in enumerate(searchlines):
    #print searchlines[i][:-1]
    root_f = TFile(searchlines[i][:-1],"read")
    if(root_f.IsZombie()):
        printError("<!> The file "+f+" is corrupted ... removes it from the list")
        continue
    h_nEvents = root_f.Get("ejets_2016/cutflow_mc_pu_zvtx_Loose").Clone()
    h_nEvents.SetDirectory(0)
    nEventsWeighted = 0
    nEventsWeighted=h_nEvents.GetBinContent(1)
    nEventsWeightedFullSample += nEventsWeighted
bfilternEventsWeightedFullSample=nEventsWeightedFullSample
print("bfilternEventsWeightedFullSample = ")
print(bfilternEventsWeightedFullSample)




xsttbar= 377.9932 
kfactorttbar=1.1949
xskfactorttbar=xsttbar*kfactorttbar
print("xskfactorttbar = ")
print(xskfactorttbar)
normweightttbar=xskfactorttbar/ttbarnEventsWeightedFullSample
print("normweightttbar = ")
print(normweightttbar)
xsbfilter=24.1063
kfactorbfilter=1.1949
xskfactorbfilter=xsbfilter*kfactorbfilter
print("xskfactorbfilter = ")
print(xskfactorbfilter)
normweightbfilter=xskfactorbfilter/bfilternEventsWeightedFullSample


print("normweightttbar = ")
print(normweightttbar)
print("normweightbfilter = ")
print(normweightbfilter)
