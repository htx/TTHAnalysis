#!/bin/env python

# hack the mbbj-mbb template and suppress the high-end tail
#
# davide.gerbaudo@gmail.com
# Mar 2017

import os
import shutil
import sys
from collections import OrderedDict

import ROOT as R
R.gROOT.SetBatch(1)

def main():
    default_input_dir = ('/afs/cern.ch/work/g/gerbaudo/public/fcnc/dev-template/run/out/'
                         'templates_7ee7be3/'
                         # 'templates_650f62a/'
                         )
    input_dir = sys.argv[1] if len(sys.argv)>1 else default_input_dir
    input_filename = (input_dir+'/'+'thc_410698._nominal_0Hist.root')
    output_filename = input_filename.replace('_0Hist.root', '_suppressed_0Hist.root')

    print input_filename
    print output_filename
    region = '4jin2bin' # 'all'
    variable = 'MbbjMinusMbb'

    histoname = 'reg_'+region+'_'+variable
    shutil.copy2(input_filename, output_filename)
    output_file = R.TFile.Open(output_filename, 'UPDATE')
    histo = output_file.Get(histoname)

    bcens = [histo.GetBinCenter (i) for i in range(0, histo.GetNbinsX()+2)]
    bcons = [histo.GetBinContent(i) for i in range(0, histo.GetNbinsX()+2)]

    def bc2str(bcs=[], format='%.2f'):
        return ', '.join(format % b for b in bcs)

    print "bin centers: ",bc2str(bcens)
    print "conts (bef): ",bc2str(bcons, '%.2e')
    
    for i in range(0, histo.GetNbinsX()+2):
        bcen = histo.GetBinCenter(i)
        bcon = histo.GetBinContent(i)
        berr = histo.GetBinError(i)
        if bcen < 75.0:
            continue
        else:
            suppression_factor = 0.01 if bcen > 100.0 else 0.1
            bcon = suppression_factor * bcon
            berr = suppression_factor * berr
            histo.SetBinContent(i, bcon)
            histo.SetBinError(i, berr)
    bcons = [histo.GetBinContent(i) for i in range(0, histo.GetNbinsX()+2)]
    print "conts (aft): ",bc2str(bcons, '%.2e')
    output_file.Write()
    output_file.Close()

if __name__=='__main__':
    main()
