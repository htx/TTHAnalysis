#!/bin/env python

# Plot the truth flavor jet1 vs jet2 for hadronic W
#
# davide.gerbaudo@gmail.com
# Nov 2016

import sys
import ROOT as R
R.gROOT.SetBatch(1)
from VLQAnalysis import utils

def main():
    filename = sys.argv[1]
    inputFile = R.TFile.Open(filename)
    keys = inputFile.GetListOfKeys()
    th2class = R.TH2.Class()
    def isTH2(k) : return R.TClass(k.GetClassName()).InheritsFrom(th2class)
    def isTruthFlav(k) : return 'fromWtruthflav' in k.GetName()
    th2_keys = [k for k in keys if isTruthFlav(k) and isTH2(k)]
    suffix = utils.common_suffix([k.GetName() for k in th2_keys])
    print '\n'.join(k.GetName() for k in th2_keys)
    histograms = [inputFile.Get(k.GetName()) for k in th2_keys]
    canvas = R.TCanvas('W had truth jet flavor', '')
    canvas.cd()
    for h in histograms:
        integral = h.Integral()
        if not integral:
            continue
        h.Scale(1.0/integral)
        canvas.Clear()
        xAxis = h.GetXaxis()
        yAxis = h.GetYaxis()
        for b,l in zip(range(1, 1+xAxis.GetNbins()),
                       ['light', 'c', 'b']):
            xAxis.SetBinLabel(b, l)
            yAxis.SetBinLabel(b, l)
        xAxis.SetTitle('Leading quark from W truth flavor')
        yAxis.SetTitle('Sub-leading quark from W truth flavor')
        h.SetStats(False)
        h.SetTitle(h.GetName().replace(suffix, ''))
        h.SetMaximum(0.5)
        h.Draw('colz text')
        canvas.Update()
        canvas.SaveAs(h.GetName()+'.png')

    

if __name__=='__main__':
    main()
