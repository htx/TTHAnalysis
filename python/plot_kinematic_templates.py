#!/bin/env python

# Plot the kinematic mass templates and store them to output file
#
# In addition to the (mass) kinematic templates, these distributions are determined:
# - W true flavor (only for background)
# - missing match true flavor
#
# davide.gerbaudo@gmail.com
# Dec 2016

import os
import re
import sys
from collections import OrderedDict

import ROOT as R
R.gROOT.SetBatch(1)

def main():

    """
    Example:

    EXE="TTHAnalysis/python/plot_kinematic_templates.py"
    INDIR="/nfs/at3/scratch2/gerbaudo/fcnc/output/VLQAnalysisOutputs_test_VLQ_bare_2017_11_07_1451"
    # background templates
    ${EXE} ${INDIR}/out*ttbar*FcncTree.root out_bkg
    # signal templates
    ${EXE} ${INDIR}/out*uH*W*FcncTree.root out_sig
    """

    input_filenames = guess_input_files(sys.argv[1:-1])
    output_dir = mkdir_if_needed(sys.argv[-1])
    if not output_dir:
        print "please provide a valid output directory"
        return

    tree_name = 'fcncTree'
    variables_sample = { 'sig' : ['Mbb', 'Mbbj', 'MbbjMinusMbb', 'Mlvb1', 'Mlvb2', 'Mlvb0',] ,
                         'bkg' : ['Mqq', 'Mqqb', 'MqqbMinusMqq', 'Mlvb1', 'Mlvb2', 'Mlvb0',] }

    base_selection = R.TCut('4jin2bin', 'jets_n>=4 && bjets_n>=2')
    all_match_selection = R.TCut('all_match', 'nmatchedjet==4')
    miss1match_selection = R.TCut('miss1match', 'nmatchedjet==3')
    # CAJR
    miss1match_selection_hyp0 = R.TCut('miss1match hypothesis 0', 'nmatchedjet==3 && nottruthmatch==0')
    miss1match_selection_hyp1 = R.TCut('miss1match hypothesis 1', 'nmatchedjet==3 && nottruthmatch==1')
    miss1match_selection_hyp2 = R.TCut('miss1match hypothesis 2', 'nmatchedjet==3 && nottruthmatch==2')
    miss1match_selection_hyp3 = R.TCut('miss1match hypotehsis 3', 'nmatchedjet==3 && nottruthmatch==3')     
    # CAJR
    def is_sig(sample_name):
        fname = os.path.basename(sample_name)
        return 'uHb' in sample_name or 'uHc' in sample_name
    # fill histograms
    histograms_template_sig = {}
    histograms_template_bkg = {}
    histograms_wfraction_bkg = {}
    histograms_wfraction_sig = {}
    histograms_mfraction_bkg = {}
    histograms_mfraction_sig = {}

    for sample, filenames in input_filenames.iteritems():
        if sample not in ['uHbW', 'uHcW', 'ttbarlight_410000']:
            continue
        sig_or_bkg = 'sig' if is_sig(sample) else 'bkg'
        plot_directory = mkdir_if_needed(os.path.join(output_dir, sig_or_bkg))
        chain = R.TChain(tree_name)
        for filename in filenames:
            chain.Add(filename)
        print("%d entries for %s" % (chain.GetEntries(), sample))
        variables = variables_sample[sig_or_bkg]
        histograms_template = (histograms_template_sig if is_sig(sample) else
                               histograms_template_bkg)
        histograms_wfraction = (histograms_wfraction_sig if is_sig(sample) else
                                histograms_wfraction_bkg)
        histograms_mfraction = (histograms_mfraction_sig if is_sig(sample) else
                                histograms_mfraction_bkg)
        # CAJR for match_selection in [all_match_selection, miss1match_selection]:
	for match_selection in [all_match_selection, miss1match_selection, miss1match_selection_hyp0, miss1match_selection_hyp1, miss1match_selection_hyp2, miss1match_selection_hyp3]:
            for var in variables:
                h_name = "reg_%s_%s_%s_%s" % (sample,
                                              base_selection.GetName(),
                                              match_selection.GetName(),
                                              var)
                h_title = "%s: %s (%s %s)" % (sample, var,
                                              base_selection.GetName(),
                                              match_selection.GetName())
                h = R.TH1F(h_name, h_title, 100, 0.0, 1000)
                chain.Draw(var+'>>'+h.GetName(), base_selection+match_selection, 'goff')
                histograms_template[h.GetName()] = h
        # compute match fractions and plot
        # if is_sig(sample):
        #     continue # for now don't need W fraction for signal
        for selection in [R.TCut('4jin2bin', 'jets_n>=4 && bjets_n>=2'), # 4j
                          R.TCut('4jex2bex', 'jets_n==4 && bjets_n==2'),
                          R.TCut('4jex3bex', 'jets_n==4 && bjets_n==3'),
                          R.TCut('4jex4bin', 'jets_n==4 && bjets_n>=4'),
                          R.TCut('5jex2bex', 'jets_n==5 && bjets_n==2'), # 5j
                          R.TCut('5jex3bex', 'jets_n==5 && bjets_n==3'),
                          R.TCut('5jex4bin', 'jets_n==5 && bjets_n>=4'),
                          R.TCut('6jin2bex', 'jets_n>=6 && bjets_n==2'), # 6j
                          R.TCut('6jin3bex', 'jets_n>=6 && bjets_n==3'),
                          R.TCut('6jin4bin', 'jets_n>=6 && bjets_n>=4')]:
            # CAJR for match_selection in [all_match_selection, miss1match_selection]:
	    for match_selection in [all_match_selection, miss1match_selection, miss1match_selection_hyp0, miss1match_selection_hyp1, miss1match_selection_hyp2, miss1match_selection_hyp3]:
                full_selection = selection+match_selection
                full_selection.SetName('_'.join([s.GetName()
                                                 for s in [full_selection, match_selection]]))
                h = plot_Wflavor_fraction(chain,
                                          full_selection,
                                          sample_label=sample,
                                          plot_directory=plot_directory)
                histograms_wfraction[h.GetName()] = h
            h = compute_all_matched_fractions(chain,
                                              selection,
                                              all_match_selection,
                                              miss1match_selection,
                                              sample_label=sample)
            histograms_mfraction[h.GetName()] = h

    # draw templates
    canvas = R.TCanvas('Kinematic templates', '')
    canvas.cd()
    colors = [R.kBlue, R.kRed, R.kOrange, R.kCyan, R.kViolet]
    for logy in [True, False]:
        for sig_or_bkg, histograms_any_var in zip(['sig', 'bkg'],
                                                  [histograms_template_sig,
                                                   histograms_template_bkg]):
            print "Processing %s" % sig_or_bkg
            plot_directory = mkdir_if_needed(os.path.join(output_dir, sig_or_bkg))
            for variable in variables_sample[sig_or_bkg]:
                canvas.Clear()
                histograms = dict([(k, h) for k, h in histograms_any_var.iteritems()
                                   if k.endswith('_'+variable)])
                histograms = OrderedDict(sorted(histograms.items()))
                if not histograms:
                    continue
                labels = extract_labels(histograms.keys(), '_')
                pad_master = None
                x_label = ''
                leg = R.TLegend(0.55, 0.65, 0.875, 0.875)
                def normalize(h):
                    integral = h.Integral()
                    h.Scale((1.0/integral) if integral else 1.0)
                    h.SetStats(False)
                for h in histograms.values():
                    normalize(h)
                max_y = max([h.GetMaximum() for h in histograms.values()])
                for (key, histo), label in zip(histograms.iteritems(), labels):
                    if not pad_master:
                        pad_master = histo
                        pad_master.SetMinimum(1.0e-4 if logy else 0.0)
                        pad_master.SetMaximum(1.1*max_y)
                    leg.AddEntry(histo, label+" %.2E" % histo.GetEntries(), 'l')
                if not pad_master:
                    print "missing histograms for %s" % variable
                    continue
                pad_master.SetTitle('Signal' if sig_or_bkg=='sig' else 'Background')
                x_label = variable
                pad_master.GetXaxis().SetTitle(x_label)
                if 'Mqq' in variable:
                    pad_master.GetXaxis().SetRangeUser(0.0, 400.0)
                elif 'Mlvb' in variable:
                    pad_master.GetXaxis().SetRangeUser(0.0, 600.0)

                for histo, color in zip(histograms.values(), colors):
                    histo.SetLineColor(color)
                pad_master.Draw('hist')
                canvas.SetLogy(logy)
                for histo in [h for h in histograms.values() if h!=pad_master]:
                    histo.Draw('hist same')
                leg.Draw()
                canvas.Update()
                canvas.SaveAs(os.path.join(plot_directory,
                                           sig_or_bkg+'_'+variable
                                           +('_log' if logy else '_lin')+'.png'))
    # save
    for sig_or_bkg in ['sig', 'bkg']:
        plot_directory = mkdir_if_needed(os.path.join(output_dir, sig_or_bkg))
        histograms_template = (histograms_template_sig if sig_or_bkg=='sig' else
                               histograms_template_bkg)
        histograms_wfraction = (histograms_wfraction_sig if sig_or_bkg=='sig' else
                                histograms_wfraction_bkg)
        histograms_mfraction = (histograms_mfraction_sig if sig_or_bkg=='sig' else
                                histograms_mfraction_bkg)
        sample_subsets = ['uHbW', 'uHcW'] if sig_or_bkg=='sig' else ['ttbarlight_410000']
        for sample_subset in sample_subsets:
            for match_selection in [all_match_selection, miss1match_selection]:
                out_filename = os.path.join(plot_directory,
                                            'templates_'+sample_subset
                                            +'_'+match_selection.GetName()+'.root')
                templates = OrderedDict([(k,v) for k,v in histograms_template.iteritems()
                                         if (sample_subset in k and
                                             match_selection.GetName() in k)])
                wfractions = OrderedDict([(k,v) for k,v in histograms_wfraction.iteritems()
                                          if (sample_subset in k and
                                              match_selection.GetName() in k)])
                # missmatches don't depend on the match_selection
                mfractions = OrderedDict([(k,v) for k,v in histograms_mfraction.iteritems()
                                          if (sample_subset in k)])
                print '-'*20
                print "saving histograms for %s %s" % (sample_subset, match_selection.GetName())
                print '-'*20
                print '    --- templates ---'
                print '\n'.join(k for k in templates.keys())
                print '    --- wfractions ---'
                print '\n'.join(k for k in wfractions.keys())
                print '    --- mfractions ---'
                print '\n'.join(k for k in mfractions.keys())
                out_file = R.TFile(out_filename, 'recreate')
                out_file.cd()
                num_written_histos = 0
                tokens_to_drop = [match_selection.GetName(), sample_subset]
                for k, h in templates.iteritems():
                    h.Write(clean_histogram_name(k, drop=tokens_to_drop))
                    num_written_histos += 1
                for k, h in wfractions.iteritems():
                    h.Write(clean_histogram_name(k, drop=tokens_to_drop))
                    num_written_histos += 1
                for k, h in mfractions.iteritems():
                    h.Write(clean_histogram_name(k, drop=tokens_to_drop))
                    num_written_histos += 1
                out_file.Close()
                out_file.Delete()
                print "Written %d histograms to %s" %(num_written_histos, out_filename)




def guess_input_files(filenames=[]):
    "given filenames, guess which one is ttbarlight (bkg), thc, and thu"
    keys = ['uHbWm', 'uHbWp',
            'uHcWm', 'uHcWp',
            'ttbarlight_410000']
    counts = OrderedDict({k : sum(1 for f in filenames if k in os.path.basename(f)) for k in keys})
    print "identified %s input files" % ' '.join("%d %s"%(c, k) for k, c in counts.iteritems())

    filenames = OrderedDict({ k : [f for f in filenames if k in os.path.basename(f)]
                              for k in keys})
    filenames['uHbW'] = filenames['uHbWm'] + filenames['uHbWp']
    filenames['uHcW'] = filenames['uHcWm'] + filenames['uHcWp']
    return filenames

def common_prefix(lst, delim=None):
    prefix = os.path.commonprefix(lst)
    print 'prefix before truncation : ',prefix
    truncate_at_delim = delim and delim in prefix
    prefix = (prefix[:prefix.rfind(delim)] if truncate_at_delim else prefix)
    return prefix

def common_suffix(lst, delim=None):
    return common_prefix(lst[::-1])[::-1]

def common_tokens(lst, delim=' '):
    all_tokens = [set(l.split(delim)) for l in lst]
    common_tokens = set.intersection(*all_tokens)
    return common_tokens

def clean_histogram_name(hname, drop=[]):
    'drop substrings and then leftover multiple consecutive _'
    orig_name = hname[:]
    for d in drop:
        hname = hname.replace(d, '')
    hname = re.sub(r'\_+', '_', hname)
    print 'cleaned ',orig_name,' -> ',hname
    return hname

def extract_labels(lst, delim):
    "strip away the common tokens from each name in lst"
    commons = common_tokens(lst, delim)
    labels = []
    for l in lst:
        ts = [t for t in l.split(delim) if t not in commons]
        label = ' '.join(ts).strip()
        labels.append(label)
    return labels

def compute_all_matched_fractions(chain,
                                  kin_selection,
                                  all_match_selection,
                                  miss1match_selection,
                                  sample_label=''):
    print "Fraction of all-match / miss-1-match for %s" % sample_label
    labels = ['tot', 'allmatch', 'miss1match', 'other']
    h = R.TH1F('reg_'+sample_label+'_'
               +kin_selection.GetName()
               +'_matchingcounts',
               'Matching counts for '+sample_label+', '+kin_selection.GetTitle(),
               4, -0.5, 3.5)
    for i, l in enumerate(labels):
        h.GetXaxis().SetBinLabel(i+1, l)
    tot = chain.GetEntries(kin_selection.GetTitle())
    all_match = chain.GetEntries((kin_selection+all_match_selection).GetTitle())
    miss1match = chain.GetEntries((kin_selection+miss1match_selection).GetTitle())
    other = tot - all_match - miss1match
    if tot:
        print "total       : %d" % tot
        print "all matched : %d (%.3f)" % (all_match, float(all_match)/float(tot))
        print "miss 1 match: %d (%.3f)" % (miss1match, float(miss1match)/float(tot))
    for i, (l, v) in enumerate(zip(labels,
                                   [tot, all_match, miss1match, other])):
        h.SetBinContent(i+1, v)
    else:
        print "empty selection"
    return h

def plot_Wflavor_fraction(chain, selection, sample_label='', plot_directory='./'):
    "Determine the relative fractions for each flavor of the 2 quarks from the W"
    h = R.TH2F("reg_"+sample_label
               +'_'+selection.GetName()
               +'_subqfromWtruthflav_vs_leadqfromWtruthflav',
               "W truth flavor for "+selection.GetName(),
               3, 0.0, 3.0,
               3, 0.0, 3.0)
    chain.Draw('subqfromWtruthflav:leadqfromWtruthflav >> '+h.GetName(),
               selection.GetTitle(),
               'goff')
    integral = h.Integral()
    if integral:
        h.Scale(1.0/integral)
    canvas = R.TCanvas('W had truth jet flavor '+selection.GetName()+' '+sample_label, '')
    canvas.cd()
    xAxis = h.GetXaxis()
    yAxis = h.GetYaxis()
    for b,l in zip(range(1, 1+xAxis.GetNbins()),
                   ['light', 'c', 'b']):
        xAxis.SetBinLabel(b, l)
        yAxis.SetBinLabel(b, l)
    xAxis.SetTitle('Leading quark from W truth flavor')
    yAxis.SetTitle('Sub-leading quark from W truth flavor')
    h.SetMaximum(0.5)
    h.SetStats(False)
    h.Draw('colz text')
    canvas.Update()
    canvas.SaveAs(os.path.join(plot_directory, h.GetName()+'.png'))
    return h

def mkdir_if_needed(dirname, verbose=False) :
    dest_dir = None
    if os.path.exists(dirname) and os.path.isdir(dirname) :
        dest_dir = dirname
    elif not os.path.exists(dirname) :
        os.makedirs(dirname)
        if verbose: print "created %s" % dirname
        dest_dir = dirname
    return dest_dir

if __name__=='__main__':
    main()
