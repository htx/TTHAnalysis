#!/bin/env python

import ROOT as R
R.gROOT.SetBatch(True)
R.gStyle.SetOptStat(0)

import sys
from collections import OrderedDict



def main():
    input_77op ='/nfs/at3/scratch2/gerbaudo/fcnc/output/VLQAnalysisOutputs_test_VLQ_bare_2017_10_27_1714_merged/'
    input_basedir='/nfs/at3/scratch2/lpereira/fcnc/output/'
    input_70op = "%s/VLQAnalysisOutputs_test_VLQ_bare_2017_11_10_1420_merged" % input_basedir
    input_60op = "%s/VLQAnalysisOutputs_test_VLQ_bare_2017_11_11_2218_merged" % input_basedir

    selections = OrderedDict({'4jin2bin' : 'njet>=4 && nbjet>=2',
                              '4jin2bin' : 'njet>=4 && nbjet>=2',

                              '4jex2bex' : 'njet==4 && nbjet==2',
                              '4jex3bex' : 'njet==4 && nbjet==3',
                              '4jex4bin' : 'njet==4 && nbjet>=4',

                              '5jex2bex' : 'njet==5 && nbjet==2',
                              '5jex3bex' : 'njet==5 && nbjet==3',
                              '5jex4bin' : 'njet==5 && nbjet>=4',

                              '6jin2bex' : 'njet>=6 && nbjet==2',
                              '6jin3bex' : 'njet>=6 && nbjet==3',
                              '6jin4bin' : 'njet>=6 && nbjet>=4'})
    # variables = ['ProbSig', 'ProbBkg', 'Discriminant']
    variables = ['Discriminant']
    filenames = {'bkg' : ['ttbarlight.root'],
                 'thu' : ['uHbWm.root', 'uHbWp.root'],
                 'thc' : ['uHcWm.root', 'uHcWp.root'],
                 }

    class Separation(object):
        def __init__(self, op=None, sample1=None, sample2=None, sep=None):
            self.op = op
            self.sample1 = sample1
            self.sample2 = sample2
            self.sep = sep

    separations = []
    for op, input_dir in [('btag60op', input_60op),
                          ('btag70op', input_70op),
                          ('btag77op', input_77op)]:
        files = {k: [R.TFile.Open(input_dir+'/'+fn) for fn in fns]
                 for k, fns in filenames.iteritems()}
        for selection in selections.keys():
            histoname = "c1lep%s_FcncDiscriminant" % selection
            hs = {}
            for sample, fns in files.iteritems():
                for fn in fns:
                    if sample in hs:
                        hs[sample].Add(fn.Get(histoname))
                    else:
                        hs[sample] = fn.Get(histoname)
            if all([(s in hs) for s in ['bkg', 'thu', 'thc']]):
                bkg, thu, thc = hs['thu'], hs['thc'], hs['bkg']
                separation = R.TMVA.gTools().GetSeparation(thu, bkg)
                print "separation %s %s thu-bkg : %f" % (op, selection, separation)
                separations.append(Separation(op=op, sample1='thu', sample2='bkg', sep=separation))
                separation = R.TMVA.gTools().GetSeparation(thc, bkg)
                print "separation %s %s thc-bkg : %f" % (op, selection, separation)
                separations.append(Separation(op=op, sample1='thc', sample2='bkg', sep=separation))
            else:
                print 'missing one of the samples for ',selection,' op ',op
                print 'h_bkg : ',('bkg' in hs)
                print 'h_thu : ',('thu' in hs)
                print 'h_thc : ',('thc' in hs)


if __name__=='__main__':
    main()
