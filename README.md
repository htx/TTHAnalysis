Setup area
```
setupATLAS
rcSetup Base,2.4.14
```

Get packages
```
git clone ssh://git@gitlab.cern.ch:7999/htx/IFAETopFramework.git
git clone ssh://git@gitlab.cern.ch:7999/htx/IFAEReweightingTools.git
cd IFAEReweightingTools && git checkout IFAEReweightingTools-00-01-06-04 && cd ..
git clone ssh://git@gitlab.cern.ch:7999/htx/VLQAnalysis.git
rc checkout VLQAnalysis/share/packages.txt
cd BtaggingTRFandRW && patch -p0 -i $ROOTCOREBIN/../VLQAnalysis/share/trf_patch_extrapc.patch && cd -
git clone ssh://git@gitlab.cern.ch:7999/gerbaudo/TTHAnalysis.git
svn co svn+ssh://svn.cern.ch/reps/atlasphys-hsg8/Physics/Higgs/HSG8/AnalysisCode/NNLOReweighter/trunk NNLOReweighter
```

Compile
```
rc find_packages
rc compile
```

Run on a file from `user.juste.410120.ttbar.DAOD_TOPQ1.e4373_s2608_r7725_r7676_p2669`
```
  VLQAnalysis \
--outputFile=out_ttbar_410120._nominal_0.root \
--inputFile=filelist.txt \
--textFileList=true \
--sampleName=Dibosons \
--useLeptonsSF=true \
--RECOMPUTETTBBRW=false \
--COMPUTETTCCNLO=false \
--RWTTFRACTIONS=false \
--filterType=NOFILTER \
--doTRF=false \
--recomputeTRF=false \
--otherVariables=false \
--btagOP=77 \
--doBlind=false \
--computeWeightSys=false \
--useMiniIsolation=false \
--usePUWeight=true \
--splitVLQDecays=true \
--dumpHistos=true \
--dumpTree=false \
--applyMetMtwCuts=true \
--invertMetMtwCuts=false \
--doTruthAnalysis=false \
--uselargeRjets=false \
--doOneLeptonAna=true \
--doZeroLeptonAna=true \
--doLowBRegions=false \
--useLeptonTrigger=true \
--useMETTrigger=true \
--jetPtCut=30 \
--RCJetPtCut=300 \
--RCNSUBJETSCUT=2 \
--applydeltaphicut=true \
--invertdeltaphicut=false \
--WEIGHTCONFIGS=$ROOTCOREBIN/data/VLQAnalysis/list_weights.list \
--sampleID=361097. \
--inputTree=nominal \
--isData=false \
>& ttbar_410120_nom.log
```


How to request replicas (usually `CERN-PROD_SCRATCHDISK` or `IFAE_SCRATCHDISK`)
```
for  X in  $(rucio ls user.juste:user.juste.*2.4.20-2-0_output_tree.root --filter type=container --short)
do
        rucio add-rule $X 1 'CERN-PROD_SCRATCHDISK' 2>&1 | tee --append rules_cern_log.txt
done
```
this can then be renewed with
```
for X in $(cat rules_cern_log.txt)
do
    rucio update-rule --lifetime 1296000 ${X} 2>&1 |tee --append rules_cern_log_extend.txt
done
```
Make a filelist:
```
for X in $(rucio ls user.juste:user.juste.*2.4.20-2-0_output_tree.root --filter type=container --short)
do
X="user.juste.410120.ttbar.DAOD_TOPQ1.e4373_s2608_r7725_r7676_p2669_2.4.20-2-0_output_tree.root"
touch filelist/${X}.txt
for Y in $(rucio list-file-replicas --rse CERN-PROD_SCRATCHDISK  ${X} | grep CERN-PROD_SCRATCHDISK | sed 's@.*\(/eos/atlas.*tree.root\).*@\1@p' | sort | uniq)
do
    echo "root://eosatlas/${Y}" >> filelist/${X}.txt
done
```