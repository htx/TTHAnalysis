

# use this flag to change from ttlight to ttbb
SAMPLE_NAME="ttbarlight"
SAMPLE_NAME="ttbarbb"

VLQAnalysis \
--nEvents=100 \
--outputFile=outVLQAnalysis_${SAMPLE_NAME}_410501._nominal_0.root \
--inputFile=/nfs/at3/scratch/gerbaudo/MBJ-2.4.28-2/user.gerbaudo.410501.ttbar.DAOD_TOPQ1.e5458_s2726_r7772_r7676_p2952_2.4.28-3-test1_output_tree.root/user.gerbaudo.12687512._000001.output_tree.root \
--textFileList=false \
--sampleName=${SAMPLE_NAME} \
--RWTTFRACTIONS=true \
--computeWeightSys=true \
--sampleID=410501. \
--inputTree=nominal \
--isData=false \
--USELEPTONSSF=true \
--FILTERTYPE=APPLYFILTER \
--DOTRF=false \
--RECOMPUTETRF=false \
--RECOMPUTEBTAGSF=false \
--BTAGOP=77 \
--TRFCDI=xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root \
--DOBLIND=false \
--SCALETTBARHTSLICES=false \
--SPLITVLQDECAYS=false \
--DUMPHISTOS=true \
--DUMPTREE=false \
--DUMPOVERLAPTREE=false \
--APPLYMETMTWCUTS=false \
--INVERTMETMTWCUTS=false \
--DOTRUTHANALYSIS=false \
--USELARGERJETS=false \
--DOONELEPTONANA=true \
--DOZEROLEPTONANA=false \
--DOLOWBREGIONS=false \
--DOLOWJREGIONS=false \
--DOSPLITEMU=false \
--DOFITREGIONS=false \
--DOPRESELREGIONS=false \
--USELEPTONTRIGGER=true \
--USEMETTRIGGER=false \
--JETPTCUT=25 \
--RCJETPTCUT=200 \
--RCNSUBJETSCUT=2 \
--APPLYDELTAPHICUT=false \
--INVERTDELTAPHICUT=false \
--APPLYTTBBCORRECTION=true \
--SAMPLEDAT=samples_info_MBJ-2.4.28-2.dat \
--APPLYTTBARNNLOCORRECTION=true \
--APPLYVJETSSHERPA22RW=true \
--DOFCNC=true \
--DOFCNCREGIONS=true \
2>&1 | tee  ${SAMPLE_NAME}_410501._nominal_0.log \
