
BASE_INPUT_DIR="/nfs/at3/scratch2/juste/fcnc/output"
# BASE_INPUT_DIR="/nfs/at3/scratch2/gerbaudo/fcnc/output"

# # for reference, commands used to make the 77% OP templates
# ./TTHAnalysis/python/plot_matching.py ${BASE_INPUT_DIR}/VLQAnalysisOutputs_test_VLQ_bare_2017_12_15_1718/out*FcncTree.root out_match/pp6_77OP/ 2>&1 |tee out_match/pp6_77OP/log.txt
# ./TTHAnalysis/python/plot_kinematic_templates.py ${BASE_INPUT_DIR}/VLQAnalysisOutputs_test_VLQ_bare_2017_12_15_1718/out*FcncTree.root out_kin/pp6_77OP/ 2>&1 | tee out_kin/pp6_77OP/log.txt

# ./TTHAnalysis/python/plot_matching.py ${BASE_INPUT_DIR}/VLQAnalysisOutputs_test_VLQ_bare_2017_12_15_1720/out*FcncTree.root out_match/pp6_60OP/ 2>&1 |tee out_match/pp6_60OP/log.txt
# ./TTHAnalysis/python/plot_kinematic_templates.py ${BASE_INPUT_DIR}/VLQAnalysisOutputs_test_VLQ_bare_2017_12_15_1720/out*FcncTree.root out_kin/pp6_60OP/ 2>&1 | tee out_kin/pp6_60OP/log.txt

# CAJR
#./TTHAnalysis/python/plot_matching.py ${BASE_INPUT_DIR}/VLQAnalysisOutputs_test_VLQ_bare_2017_12_23_1741/out*FcncTree.root out_match/pp6_60OP/ 2>&1 |tee out_match/pp6_60OP/log.txt
./TTHAnalysis/python/plot_kinematic_templates.py ${BASE_INPUT_DIR}/VLQAnalysisOutputs_test_VLQ_bare_2017_12_23_1741/out*FcncTree.root out_kin/pp6_60OP_multhyp/ 2>&1 | tee out_kin/pp6_60OP_multhyp/log.txt
