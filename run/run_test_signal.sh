

VLQAnalysis \
--nEvents=100 \
--outputFile=outVLQAnalysis_uHbWm_410696._nominal_0.root \
--inputFile=\
/nfs/at3/scratch2/lvalery/VLQFiles/MBJ-2.4.28-2/././user.orlando.410696.FCNC.DAOD_TOPQ1.e5575_a766_a821_r7676_p2669_2.4.28-2-0_output_tree.root/user.orlando.10897958._000001.output_tree.root,\
/nfs/at3/scratch2/lvalery/VLQFiles/MBJ-2.4.28-2/././user.orlando.410696.FCNC.DAOD_TOPQ1.e5575_a766_a821_r7676_p2669_2.4.28-2-0_output_tree.root/user.orlando.10897958._000002.output_tree.root \
--textFileList=false \
--sampleName=uHbWm \
--computeWeightSys=false \
--sampleID=410696. \
--inputTree=nominal \
--isData=false \
--USELEPTONSSF=true \
--FILTERTYPE=APPLYFILTER \
--DOTRF=false \
--RECOMPUTETRF=false \
--RECOMPUTEBTAGSF=false \
--BTAGOP=77 \
--TRFCDI=xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root \
--DOBLIND=false \
--SCALETTBARHTSLICES=false \
--SPLITVLQDECAYS=false \
--DUMPHISTOS=true \
--DUMPTREE=false \
--DUMPOVERLAPTREE=false \
--APPLYMETMTWCUTS=false \
--INVERTMETMTWCUTS=false \
--DOTRUTHANALYSIS=false \
--USELARGERJETS=false \
--DOONELEPTONANA=true \
--DOZEROLEPTONANA=false \
--DOLOWBREGIONS=false \
--DOLOWJREGIONS=false \
--DOSPLITEMU=false \
--DOFITREGIONS=false \
--DOPRESELREGIONS=false \
--USELEPTONTRIGGER=true \
--USEMETTRIGGER=false \
--JETPTCUT=25 \
--RCJETPTCUT=200 \
--RCNSUBJETSCUT=2 \
--APPLYDELTAPHICUT=false \
--INVERTDELTAPHICUT=false \
--APPLYTTBBCORRECTION=true \
--SAMPLEDAT=samples_info_MBJ-2.4.28-2.dat \
--APPLYTTBARNNLOCORRECTION=true \
--APPLYVJETSSHERPA22RW=true \
--DOFCNC=true \
--DOFCNCREGIONS=true \
2>&1 | tee uHbWm_410696._nominal_0.log 

