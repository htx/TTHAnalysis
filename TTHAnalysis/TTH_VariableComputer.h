#ifndef TTH_VARIABLECOMPUTER_H
#define TTH_VARIABLECOMPUTER_H

#include <vector>
#include "TLorentzVector.h"

class TTH_VariableComputer {

public:
  static std::vector<TLorentzVector> NuSolution(TLorentzVector &l, TLorentzVector &nu);

};

#endif // TTH_VARIABLECOMPUTER_H
