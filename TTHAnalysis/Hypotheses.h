// emacs -*- C++ -*-

#ifndef TTH_HYPOTHESES_H
#define TTH_HYPOTHESES_H

#include "TTHAnalysis/Kinematic.h"

#include <cstddef> // size_t
#include <string>
#include <vector>

class TLorentzVector;

namespace tth
{

class RecoJet;
class TlvTruth;

/**
   @brief a TLV that also holds the reco info from btag and from truth matching
*/

struct SignalHypothesis {
    RecoJet* b1; ///< 1st highest-pt b from H > bb
    RecoJet* b2; ///< 2nd highest-pt b from H > bb
    RecoJet* blep; ///< b from t > Wb
    RecoJet* j; ///< j from q from t > Hq
    RecoJet* ej1; ///< extra jet 1
    RecoJet* ej2; ///< extra jet 2
    size_t num_extra_jets; ///< number of un-matched jets (including ej1, ej2)
    SignalHypothesis();
    /**
       @brief assign the given recojet to the approriate internal ptr based on RecoJet::truth_match
    */
    bool assignRecoJet(RecoJet &reco_jet);
    bool allMatched() const;
    /// @brief number of matched jets (from 0 to 4)
    size_t numberMatched() const;
    /// @brief number of unmatched jets (from 0 to 4)
    size_t numberUnmatched() const;
    /**
       @brief which jet is unmatched

       The value is [0=b1,...,3=j]; well-defined only for the 1-missing-parton case.
       \todo define enum
     */
    size_t whatIsMissing() const;
    /**
       @brief Compute the kinematic variables for this hypothesis with the given 4-momenta

       For now implemented only the all-matched and the
       1-missing-parton cases.
       For now we only compute the variables used for the templates,
       not all the datamembers of the kinematic configuration.
    */
    KinematicConfigurationSig
    kinematic(const TLorentzVector &tlv_lep,
              const std::vector<TLorentzVector> &neutrino_solutions) const;
    /// @brief brief printable summary
    std::string str() const;
    /// @brief long printable summary
    std::string str_detailed() const;
};

struct BackgroundHypothesis {
    RecoJet* q1; ///< 1st highest-pt j from t > Wb, W > qq'
    RecoJet* q2; ///< 2nd highest-pt j from t > Wb, W > qq'
    RecoJet* blep; ///< b from t > Wb, W > lv
    RecoJet* bhad; ///< b from t > Wb, W > qq'
    RecoJet* ej1; ///< extra jet 1
    RecoJet* ej2; ///< extra jet 2
    size_t num_extra_jets; ///< number of un-matched jets (including ej1, ej2)
    BackgroundHypothesis();
    /**
       @brief assign the given recojet to the approriate internal ptr based on RecoJet::truth_match
     */
    bool assignRecoJet(RecoJet &reco_jet);
    bool allMatched() const;
    /// @brief number of matched jets (from 0 to 4)
    size_t numberMatched() const;
    /// @brief number of unmatched jets (from 0 to 4)
    size_t numberUnmatched() const;
    /**
       @brief which jet is unmatched

       The value is [0=b1,...,3=j]; well-defined only for the 1-missing-parton case.
       \todo define enum
     */
    size_t whatIsMissing() const;
    /**
       @brief Compute the kinematic variables for this hypothesis with the given 4-momenta

       For now implemented only the all-matched and the
       1-missing-parton cases.
       For now we only compute the variables used for the templates,
       not all the datamembers of the kinematic configuration.
    */
    KinematicConfigurationBkg
    kinematic(const TLorentzVector &tlv_lep,
              const std::vector<TLorentzVector> &neutrino_solutions) const;
    /// @brief brief printable summary
    std::string str() const;
    /// @brief long printable summary
    std::string str_detailed() const;
};
} // tth
#endif // TTH_HYPOTHESES_H
