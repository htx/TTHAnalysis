#ifndef TTH_NTUPLEDATA_H
#define TTH_NTUPLEDATA_H

#include <vector>
#include "IFAETopFramework/NtupleData.h"

class TTH_NtupleData : public NtupleData {

public:
  //
  // Standard C++ functions
  //
  TTH_NtupleData();
  ~TTH_NtupleData();

public:
  //
  // Here, we declare the data variables specific to the package
  // By convention, these data variables have to be noted: d_*
  //

  ///////event variables
  int d_runNumber;
  int d_eventNumber;
  float d_trf_weight_77_4in;
  float d_trf_weight_77_3ex;
  float d_trf_weight_70_4in;
  float d_trf_weight_70_3ex;
  float d_mu;
  int d_HF_SimpleClassification;
  int d_TopHeavyFlavorFilterFlag;
  ///////jet variables
  float d_jets_n;
  std::vector<float> *d_jets_pt;
  std::vector<float> *d_jets_eta;
  std::vector<float> *d_jets_phi;
  std::vector<float> *d_jets_e;
  std::vector<float> *d_jets_mv2c10;
  std::vector<float> *d_jets_mv2c20;
  std::vector<int> *d_jets_truthflav;
  std::vector<int> *d_jets_truthmatch;
  std::vector<int> *d_truthHiggsDaughtersID;
  float d_truth_ttbar_pt;
  float d_truth_top_pt;
  float d_HhadT_jets;
  int d_e_jets;
  int d_mu_jets;
  int d_e_jets_2015;
  int d_mu_jets_2015;
  int d_e_jets_2016;
  int d_mu_jets_2016;
  int d_boosted_e_jets;
  int d_boosted_mu_jets;
  //////////////
  // std::vector < std::vector < int > > *d_jets_truth_index;
  ////large jets
  std::vector<float> *d_ljets_pt; //
  std::vector<float> *d_ljets_eta;
  std::vector<float> *d_ljets_m;
  std::vector<float> *d_ljets_sd12; //
  std::vector<float> *d_ljets_sd23;
  std::vector<float> *d_ljets_tau21;
  std::vector<float> *d_ljets_tau32;
  std::vector<float> *d_ljets_topTag_loose;
  std::vector<float> *d_ljets_topTag_tight;
  int d_ljets_topTagN_loose;
  int d_ljets_topTagN_tight;
  ////////electrons
  std::vector<float> *d_el_pt;
  std::vector<float> *d_el_eta;
  std::vector<float> *d_el_phi;
  std::vector<float> *d_el_e;
  ////////muons
  std::vector<float> *d_mu_pt;
  std::vector<float> *d_mu_eta;
  std::vector<float> *d_mu_phi;
  std::vector<float> *d_mu_e;
  //////met
  float d_met_met;
  float d_met_phi;
  //////////event weights
  // double d_eventWeight_Nom;
  float d_eventWeight_pileup;
  float d_eventWeight_mc;
  float d_eventWeight_leptonSF;
  float d_eventWeight_bTagSF;
  float d_weight_jvt;
  float d_weight_ttbar_FracRw;
  float d_weight_ttbb_Nominal;
  /* float d_eventWeight_Nom; */
  /* float d_eventWeight_Btag1; */
  /* float d_eventWeight_Btag2; */
  // double d_eventWeight_Btag1;
  // double d_eventWeight_Btag2;
};

#endif // TTH_NTUPLEDATA_H
