#ifndef TTH_NTUPLEREADER_H
#define TTH_NTUPLEREADER_H

#include <string>
#include "IFAETopFramework/NtupleReader.h"
class TTH_NtupleData;
// class OptionsBase;
class TTH_Options;

class TBranch;

class TTH_NtupleReader : public NtupleReader {

public:
  //
  // Standard C++ functions
  //
  // TTH_NtupleReader( OptionsBase* opt );
  TTH_NtupleReader(TTH_Options *opt);

  virtual ~TTH_NtupleReader() override;

  //
  // Virtual functions
  //
  int SetAllBranchAddresses();
  int SetEventBranchAddresses(const std::string &sj);
  int SetJetBranchAddresses(const std::string &sj);
  int SetLeptonBranchAddresses(const std::string &sj);
  int SetWeightBranchAddresses(const std::string &sj);
  int SetMETBranchAddresses(const std::string &sj);
  const TTH_NtupleData *Data() const { return m_TTH_ntupData; }

  /* virtual int SetEventBranchAddresses(); */
  /*   virtual int SetJetBranchAddresses(const std::string &sj); */
  /*   virtual int SetLeptonBranchAddresses(const std::string &sj); */
  /*   virtual int SetWeightBranchAddresses(const std::string &sj); */
  /*   virtual int SetMETBranchAddresses(const std::string &sj); */

  /* protected: */

  /*     // */
  /*     // Here are declared the TBranch objects needed t operform the */
  /*     // association of TTH_NtupleData objects and the tree content */
  /*     // */
  /*     ///////events */
  /*     TBranch* b_eventNumber; */
  /*     TBranch* b_runNumber; */
  /*     TBranch* b_trf_weight_77_4in; */
  /*     TBranch* b_trf_weight_77_3ex; */
  /*     TBranch* b_trf_weight_70_4in; */
  /*     TBranch* b_trf_weight_70_3ex; */
  /*     TBranch* b_mu; */
  /*     TBranch* b_HF_SimpleClassification; */
  /*     TBranch* b_TopHeavyFlavorFilterFlag; */
  /*     ////////jets */
  /*     TBranch* b_jets_n; //! */
  /*     TBranch* b_jets_pt; //! */
  /*     TBranch* b_jets_eta; //! */
  /*     TBranch* b_jets_phi; //! */
  /*     TBranch* b_jets_e; //! */
  /*     TBranch* b_jets_mv2c10; */
  /*     TBranch* b_jets_mv2c20; */
  /*     TBranch* b_jets_truthflav; */
  /*     TBranch* b_jets_truthmatch; */
  /*     TBranch* b_truthHiggsDaughtersID; */
  /*     TBranch* b_truth_ttbar_pt; */
  /*     TBranch* b_truth_top_pt; */
  /*     TBranch* b_HhadT_jets; */
  /*     //TBranch* b_e_jets; */
  /*     //TBranch* b_mu_jets; */
  /*        TBranch* b_e_jets_2015; */
  /*     TBranch* b_mu_jets_2015; */
  /*    TBranch* b_e_jets_2016; */
  /*     TBranch* b_mu_jets_2016; */

  /*     TBranch* b_boosted_e_jets; */
  /*     TBranch* b_boosted_mu_jets; */
  /*     //TBranch* b_jets_truth_index; //! */
  /*     TBranch* b_ljets_pt;//////////ljets */
  /*     TBranch* b_ljets_eta; */
  /*     TBranch* b_ljets_m; */
  /*     TBranch* b_ljets_sd12; */
  /*     TBranch* b_ljets_sd23; */
  /*     TBranch* b_ljets_tau21; */
  /*     TBranch* b_ljets_tau32; */
  /*     TBranch* b_ljets_topTag_loose; */
  /*     TBranch* b_ljets_topTag_tight; */
  /*     TBranch* b_ljets_topTagN_loose; */
  /*     TBranch* b_ljets_topTagN_tight; */
  /*     TBranch* b_el_pt;//////////electrons */
  /*     TBranch* b_el_eta; */
  /*     TBranch* b_el_phi; */
  /*     TBranch* b_el_e; */
  /*     TBranch* b_mu_pt;//////////muons */
  /*     TBranch* b_mu_eta; */
  /*     TBranch* b_mu_phi; */
  /*     TBranch* b_mu_e; */
  /*     TBranch* b_met_met;/////////met */
  /*     TBranch* b_met_phi; */
  /*     TBranch* b_eventWeight_pileup;////////////weights */
  /*     TBranch* b_eventWeight_mc; */
  /*     TBranch* b_eventWeight_leptonSF; */
  /*     TBranch* b_eventWeight_bTagSF; */
  /*     TBranch* b_weight_jvt; */
  /*     TBranch* b_weight_ttbar_FracRw; */
  /*     TBranch* b_weight_ttbb_Nominal; */
  /* TBranch* b_evt_w_Nom; //! */
  /* TBranch* b_evt_w_Btag1; //! */
  /* TBranch* b_evt_w_Btag2; //! */

private:
  //
  // Here we declare the local TTH_NtupleData object to store the
  // analysis specific variables
  //
  TTH_NtupleData *m_TTH_ntupData;
  // TTH_Options m_opt;
};

#endif // TTH_NTUPLEREADER_H
