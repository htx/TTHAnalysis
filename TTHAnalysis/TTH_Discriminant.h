// emacs -*- C++ -*-
#ifndef TTH_DISCRIMINANT_H
#define TTH_DISCRIMINANT_H

#include "TTHAnalysis/RecoJet.h"
#include "TTHAnalysis/TruthMatchingOutput.h"
#include "TTHAnalysis/TlvTruth.h"
#include "TTHAnalysis/Training.h"

#include "TFile.h"
#include "TH1.h"
#include <vector>
#include "TLorentzVector.h"

class AnalysisObject;
class HistManager;
class TTH_Options;

class TTH_Discriminant {
public:
    /** @brief a pair of values labeled numerator and denominator
     */
    struct NumDenPair {
        float num;
        float den;
        NumDenPair(float n, float d) { num = n; den = d; }
    };

    /**
       @brief Holds the info computed when the discriminant is evaluated for an event

       The discriminant itself, and the various probability components.
    */
    struct Result {
        float Discriminant; ///< p_sig / (p_sig + p_bkg)
        float ProbSig; ///< \Sum(p_kin * p_btag) / \Sum p_btag
        float ProbBkg; ///< \Sum(p_kin^bb * p_btag^bb + p_kin^bj * p_btag^bj) / \Sum(p_btag^bb + p_btag^bj)
        float ProbSigNum; ///< numerator of ProbSig
        float ProbSigDen; ///< denominator of ProbSig
        float ProbBkgNum; ///< numerator of ProbBkg
        float ProbBkgDen; ///< denominator of ProbBkg
        float ProbBkgNum_bb; ///< numerator or ProbBkg for 2b (i.e. when there are 2 extra b truth partons)
        float ProbBkgDen_bb; ///< denominator or ProbBkg for 2b
        float ProbBkgNum_bj; ///< numerator or ProbBkg for bj (i.e. when there is 1 extra b truth parton)
        float ProbBkgDen_bj; ///< denominator or ProbBkg for 1b
        Result& setDefaults();
    };

public:
  //
  // Standard C++ functions
  //
  TTH_Discriminant(TTH_Options *opt);
  TTH_Discriminant(const std::string &sampleid,
                   const bool &isdata);
  ~TTH_Discriminant();

  //
  // Specific functions
  //

  bool FillHistosDataMC();
    /**
       @brief main function to determine the templates

       Three steps:
       - for each reco jet determine the match with the truth particle
       - assign each reco jet to one of the partons for a given hypothesis (sig or bkg)
       - under the given hypotesis, compute kinematic variables for the template
     */
    tth::TruthMatchingOutput matchToTruth(const TLorentzVector &tlv_lep,
                                          const std::vector<TLorentzVector> &neutrino_solutions,
                                          const tth::TlvTruth &tlvt);
  bool ClearData();
    /**
       @brief same as convert the reco jets to the format used for matching

       Right now the output colletion should be stored to m_recoJets
    */
  std::vector<tth::RecoJet> fillRecoJets(const std::vector< AnalysisObject* > &jets) const;
    /**
       @brief store the truth info we need as a TlvTruth
    */

    tth::TlvTruth fillTruthTlvs(const vector<float> *pts,
                                const vector<float> *etas,
                                const vector<float> *phis,
                                const vector<float> *masses,
                                const vector<float> *pdgs,
                                const vector < vector < int > > *childrenIndices) const;

    std::vector<TLorentzVector> NuSolution(const TLorentzVector &l, const TLorentzVector &nu);
  void GetPartPermNoOrder(std::string sofar, std::string rest, int n); // DG-2016-11-25 still useful? TODO

    void print_unmatched_jets() const;
    std::vector<tth::RecoJet> m_recoJets;
    std::vector<tth::RecoJet> m_recoExtraJets;
    std::vector<tth::RecoJet> m_recoExtraJetsB;
    std::vector<tth::RecoJet> m_recoExtraJetsNotb;
    std::vector<std::string> ExtraJetCombinations;
    /// collect all signal and background training info
    void InitTrainings();
/** @brief loop over permutations and accumulate probSigNum and probSigDen

    This is for the all-matched hypothesis
    Note: for now it's the same for all-matched and miss-1-match.
    When I differentiate between different missing matches,
    then I will have to implement the
    accumulate*MissmatchProbabilities (for sig and bkg)
*/
    static NumDenPair accumulateSigAllMatchedProbabilities(const std::vector<tth::RecoJet> &recoJets,
                                                           const std::vector<TLorentzVector> &NuSolutions,
                                                           const TLorentzVector& tlv_lep,
                                                           const tth::TrainingSig &tr_sig);
/** @brief loop over permutations and accumulate probBkgNum and probBkgDen

    This is for the all-matched hypothesis
*/
    static NumDenPair accumulateBkgAllMatchedProbabilities(const std::vector<tth::RecoJet> &recoJets,
                                                           const std::vector<TLorentzVector> &NuSolutions,
                                                           const TLorentzVector& tlv_lep,
                                                           const tth::TrainingBkg &tr_bkg,
                                                           const float fWll,
                                                           const float fWlc);
    const Result& GetDiscriminant(const size_t &nj, const size_t &nb,
                                  const std::vector<tth::RecoJet> &recoJets,
                                  const std::vector<TLorentzVector> &NuSolutions,
                                  const TLorentzVector& tlv_lep);
    /** @brief list of valid regions
        Regions for which we expect to fill the histograms and have the training available.
     */
    static std::vector<std::string> training_regions();
    /** @name pickTraining

        For some of the training parameters, namely the W truth flavor
        fraction (for background) and the matching fraction (for
        signal and background), we need to pick the training set
        specific to a nj, nb selection.
        The functions below just pick the training specific to this
        nj-nb pair.
     */
///@{
    const tth::TrainingBkg& pickAllMatchBkgTraining(const size_t nj, const size_t nb) const;
    const tth::TrainingBkg& pickMiss1MatchBkgTraining(const size_t nj, const size_t nb) const;
    const tth::TrainingSig& pickMiss1MatchSigTraining(const size_t nj, const size_t nb) const;
///@}
    /** @name SignalTrainings
        Get inclusive kinematic templates (for all-match and miss-1-match hipotheses),
        then for each selection the matching fractions.
     */
///@{
    tth::TrainingSig m_sig_4jin2bin_am; // kin templates: all-match hypothesis
    tth::TrainingSig m_sig_4jin2bin_mm; // kin templates: miss-1-match hypothesis
    tth::TrainingSig m_sig_4jex2bex_mm; // 4j (matching fractions)
    tth::TrainingSig m_sig_4jex3bex_mm;
    tth::TrainingSig m_sig_4jex4bin_mm;
    tth::TrainingSig m_sig_5jex2bex_mm; // 5j
    tth::TrainingSig m_sig_5jex3bex_mm;
    tth::TrainingSig m_sig_5jex4bin_mm;
    tth::TrainingSig m_sig_6jin2bex_mm; // 6j
    tth::TrainingSig m_sig_6jin3bex_mm;
    tth::TrainingSig m_sig_6jin4bin_mm;
///@}
    /** @name BackgroundTrainings

        Get inclusive kinematic templates (for all-match and miss-1-match hipotheses),
        then for each selection the W flavor fraction and the matching fractions.
     */
///@{
    tth::TrainingBkg m_bkg_4jin2bin_am; // kin templates: all-match hypothesis
    tth::TrainingBkg m_bkg_4jin2bin_mm; // kin templates: miss-1-match hypothesis
    tth::TrainingBkg m_bkg_4jex2bex_am; // 4j (W flavor fractions)
    tth::TrainingBkg m_bkg_4jex3bex_am;
    tth::TrainingBkg m_bkg_4jex4bin_am;
    tth::TrainingBkg m_bkg_5jex2bex_am; // 5j
    tth::TrainingBkg m_bkg_5jex3bex_am;
    tth::TrainingBkg m_bkg_5jex4bin_am;
    tth::TrainingBkg m_bkg_6jin2bex_am; // 6j
    tth::TrainingBkg m_bkg_6jin3bex_am;
    tth::TrainingBkg m_bkg_6jin4bin_am;
    tth::TrainingBkg m_bkg_4jex2bex_mm; // 4j (matching fractions)
    tth::TrainingBkg m_bkg_4jex3bex_mm;
    tth::TrainingBkg m_bkg_4jex4bin_mm;
    tth::TrainingBkg m_bkg_5jex2bex_mm; // 5j
    tth::TrainingBkg m_bkg_5jex3bex_mm;
    tth::TrainingBkg m_bkg_5jex4bin_mm;
    tth::TrainingBkg m_bkg_6jin2bex_mm; // 6j
    tth::TrainingBkg m_bkg_6jin3bex_mm;
    tth::TrainingBkg m_bkg_6jin4bin_mm;
    bool m_sig, m_bkg;
///@}

    /**
       @brief cached matching output

       Mainly used to derive templates, not needed when just computing discriminant.
     */
    tth::TruthMatchingOutput m_truthMatch;
    /**
       @brief cached discriminant result
     */
    Result m_result;
    static bool IsLepton(int pdgid);
    static bool IsNu(int pdgid);
    static bool IsUdcs(int pdgid);
    static bool IsUc(int pdgid);
    static bool IsBot(int pdgid);
    static bool IsTop(int pdgid);
    static bool IsW(int pdgid);
    static bool IsHiggs(int pdgid);

protected:
  HistManager *m_histMngr;
    std::string m_sample_id;
    bool m_has_mc_branches;
};

#endif // TTH_DISCRIMINANT_H
