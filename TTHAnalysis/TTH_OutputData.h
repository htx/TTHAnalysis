#ifndef TTH_OUTPUTDATA_H
#define TTH_OUTPUTDATA_H

#include <vector>
#include "IFAETopFramework/OutputData.h"
#include "TLorentzVector.h"

// class TLorentzVector;
class TTH_OutputData : public OutputData {

public:
  //
  // Stadard C++ functions
  //
  TTH_OutputData();
  TTH_OutputData(const TTH_OutputData &);
  ~TTH_OutputData();

  //
  // Class specific functions
  //
  virtual void ClearOutputData();
  virtual void EmptyOutputData();

public:
  //
  // Declare here the output variables of your code. The naming convention
  // is o_*
  //
  /////
  int o_e_jets;
  int o_mu_jets;
  int o_runNumber;
  int o_eventNumber;
  float o_mu;
  int o_sampleName;
  float o_ht; ////////////
  int o_ntruth;
  float o_qFromTop_pt;
  float o_Mbb;
  float o_Mqqb;
  float o_Mqq;
  float o_MqqbMinusMqq;
  float o_Mlvb0;
  float o_Mlvb1;
  float o_Mlvb2;
  float o_Mbbj;
  float o_MbbjMinusMbb;
  int o_jetFromTop_truthflav;
  float o_ExtraJets;
  float o_ExtraJetsbb;
  float o_ExtraJetsbj;
  float o_ProbSig;
  float o_log10ProbSig;
  float o_log10ProbSig5matched;
  float o_log10ProbSig6matchedAnd5matched;
  float o_log10ProbSig10;
  float o_log10ProbSig3VOP;
  float o_log10ProbSigKin;
  // float o_log10ProbSig2;
  float o_log10ProbBkg;
  float o_log10ProbBkgTestbb;
  float o_log10ProbBkgPermB;
  float o_log10ProbBkgPermBAndLight;
  float o_log10ProbBkgPermBAndLightBTag;
  float o_log10ProbBkgKin;
  // float o_log10ProbBkg2;
  // float o_log10ProbBkg3;
  // float o_log10ProbBkg4;
  float o_PMbb;
  float o_log10PMbb;
  float o_Discriminant;
  float o_Discriminant6matchedAnd5matched;
  float o_DiscriminantKin;
  float o_leadJetPt;
  float o_leadJetEta;
  int o_jets_n;
  int o_bjets_n;
  int o_matchedjets_n;
  int o_matchedjetsttbar_n;
  int o_truthbextrajets_n;
  int o_truthnotbextrajets_n;
  float o_HhadT_jets;
  std::vector<float> *o_ExtraJetsPerm;
  std::vector<float> *o_ExtraJetsPerm_ll;
  std::vector<float> *o_ExtraJetsPerm_lc;
  std::vector<float> *o_ExtraJetsPerm_lb;
  std::vector<float> *o_ExtraJetsPerm_cc;

  std::vector<float> *o_ExtraJetsPerm_bc;
  std::vector<float> *o_ExtraJetsPerm_bb;
  std::vector<float> *o_ExtraJetsPerm_notbb;

  std::vector<float> *o_Mqq_NoLeadJetW_b;
  std::vector<float> *o_Mqq_NoLeadJetW_c;
  std::vector<float> *o_Mqq_NoLeadJetW_l;
  std::vector<float> *o_Mqqb_NoLeadJetW_b;
  std::vector<float> *o_Mqqb_NoLeadJetW_c;
  std::vector<float> *o_Mqqb_NoLeadJetW_l;
  std::vector<float> *o_MqqbMinusMqq_NoLeadJetW_b;
  std::vector<float> *o_MqqbMinusMqq_NoLeadJetW_c;
  std::vector<float> *o_MqqbMinusMqq_NoLeadJetW_l;
  std::vector<float> *o_Mqq_NoSubLeadJetW_b;
  std::vector<float> *o_Mqq_NoSubLeadJetW_c;
  std::vector<float> *o_Mqq_NoSubLeadJetW_l;
  std::vector<float> *o_Mqqb_NoSubLeadJetW_b;
  std::vector<float> *o_Mqqb_NoSubLeadJetW_c;
  std::vector<float> *o_Mqqb_NoSubLeadJetW_l;
  std::vector<float> *o_MqqbMinusMqq_NoSubLeadJetW_b;
  std::vector<float> *o_MqqbMinusMqq_NoSubLeadJetW_c;
  std::vector<float> *o_MqqbMinusMqq_NoSubLeadJetW_l;
  std::vector<int> *o_ExtraJetNoLeadJetW_truthflav;
  std::vector<int> *o_ExtraJetNoSubLeadJetW_truthflav;

  float o_Mqq_NoLeadJetW_Jet_1_b;
  float o_Mqq_NoLeadJetW_Jet_1_c;
  float o_Mqq_NoLeadJetW_Jet_1_l;
  float o_Mqqb_NoLeadJetW_Jet_1_b;
  float o_Mqqb_NoLeadJetW_Jet_1_c;
  float o_Mqqb_NoLeadJetW_Jet_1_l;
  float o_MqqbMinusMqq_NoLeadJetW_Jet_1_b;
  float o_MqqbMinusMqq_NoLeadJetW_Jet_1_c;
  float o_MqqbMinusMqq_NoLeadJetW_Jet_1_l;
  float o_Mqq_NoSubLeadJetW_Jet_1_b;
  float o_Mqq_NoSubLeadJetW_Jet_1_c;
  float o_Mqq_NoSubLeadJetW_Jet_1_l;
  float o_Mqqb_NoSubLeadJetW_Jet_1_b;
  float o_Mqqb_NoSubLeadJetW_Jet_1_c;
  float o_Mqqb_NoSubLeadJetW_Jet_1_l;
  float o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b;
  float o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c;
  float o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l;
  int o_ExtraJetNoLeadJetW_Jet_1_truthflav;
  int o_ExtraJetNoSubLeadJetW_Jet_1_truthflav;
  ////////////////////////////////////////
  std::vector<float> *o_jets_pt;
  std::vector<float> *o_jets_eta;
  std::vector<float> *o_jets_mv2c10;
  std::vector<float> *o_jets_mv2c20;
  // std::vector < int > * o_leadJetFromW_truthflav;
  // std::vector < int > * o_subJetFromW_truthflav;
  int o_leadJetFromW_truthflav;
  int o_subJetFromW_truthflav;
  int o_extraJet1_truthflav;
  int o_extraJet2_truthflav;
  int o_extrajetleadnotb_truthflav;
  ////////////////////////////////
  float o_Mqq_Jet_1;
  float o_Mqqb_Jet_1;
  float o_MqqbMinusMqq_Jet_1;
  ////////////////////////////////
  int o_jetFromW5matched_truthflav;
  int o_extrajet5matched_truthflav;
  //////////////////////////////////
  std::vector<int> *o_extraJet1Perm_truthflav;
  std::vector<int> *o_extraJet2Perm_truthflav;
  std::vector<int> *o_jets_truthflav;
  std::vector<int> *o_jets_truthmatch;
  int o_nottruthmatch;
  int o_ljets_n; ////////////////
  float o_leadLJetPt;
  float o_subLJetPt;
  float o_leadLJetEta;
  float o_subLJetEta;
  float o_leadLJetM;
  float o_subLJetM;
  float o_leadLJet_SPLIT12;
  float o_subLJet_SPLIT12;
  float o_leadLJet_SPLIT23;
  float o_subLJet_SPLIT23;
  float o_leadLJet_Tau21;
  float o_subLJet_Tau21;
  float o_leadLJet_Tau32;
  float o_subLJet_Tau32;
  int o_nTopTag_loose;
  int o_nTopTag_tight;
  std::vector<float> *o_ljets_pt;
  std::vector<float> *o_ljets_eta;
  std::vector<float> *o_ljets_m;
  std::vector<float> *o_allLJet_SPLIT12;
  std::vector<float> *o_allLJet_SPLIT23;
  std::vector<float> *o_allLJet_Tau21;
  std::vector<float> *o_allLJet_Tau32;
  float o_el_pt; //////////
  float o_el_eta;
  float o_el_phi;
  float o_el_e;
  float o_mu_pt; //////////
  float o_mu_eta;
  float o_mu_phi;
  float o_mu_e;
  ////////////////
  float o_met_met; /////////met
  float o_met_phi;
  float o_mtw;
  int o_ind_leadTopTag_loose;
  int o_ind_leadTopTag_tight;
  float o_eventWeight_pileup; ////////////weights
  float o_eventWeight_mc;
  float o_eventWeight_leptonSF;
  float o_eventWeight_bTagSF;
  float o_eventWeight;
  TLorentzVector o_tlv_lep;
  TLorentzVector o_tlv_met;
  /* double o_eventWeight_Btag1; */
  /* double o_eventWeight_Btag2; */
};

#endif // TTH_OUTPUTDATA_H
