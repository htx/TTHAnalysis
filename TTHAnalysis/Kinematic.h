// emacs -*- C++ -*-

#ifndef TTH_KINEMATIC_H
#define TTH_KINEMATIC_H

namespace tth
{
/**
   @brief Kinematic configuration for the signal hypothesis
*/
struct KinematicConfigurationSig {
    float Mlvb0 = -9; ///< one neutrino solution
    float Mlvb1 = -9; ///< two neutrino solutions, 1st one
    float Mlvb2 = -9; ///< two neutrino solutions, 2nd one
    float Mbb = -9;
    float Mbbq = -9;
    float MbbqMinusMbb = -9;
    KinematicConfigurationSig():
        Mlvb0(-9),
        Mlvb1(-9),
        Mlvb2(-9),
        Mbb(-9),
        Mbbq(-9),
        MbbqMinusMbb(-9)
        {};
};

/**
   @brief Kinematic configuration for the background hypothesis
*/
struct KinematicConfigurationBkg {
    float Mlvb0 = -9; ///< one neutrino solution
    float Mlvb1 = -9; ///< two neutrino solutions, 1st one
    float Mlvb2 = -9; ///< two neutrino solutions, 2nd one
    float Mqq = -9;
    float Mqqb = -9;
    float MqqbMinusMqq = -9;
    KinematicConfigurationBkg():
        Mlvb0(-9),
        Mlvb1(-9),
        Mlvb2(-9),
        Mqq(-9),
        Mqqb(-9),
        MqqbMinusMqq(-9)
        {};
};

} // tth
#endif // TTH_KINEMATIC_H
