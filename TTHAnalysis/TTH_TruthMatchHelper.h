#ifndef TTH_TruthMatchHelper_HH
#define TTH_TruthMatchHelper_HH
#include "IFAETopFramework/AnalysisObject.h"

//#include "PhysObjects.h"
//#include "RecoObjects.h"
#include "TTHAnalysis/TTH_GeneralTruthHelper.h"

using namespace TTHbbTruth;

/// for now only static functions
/// will transform to a full class if needed
class JetTruthMatchHelper {

public:
  static bool bitMatch(int mask, int bit);
  static bool bitMatchOnly(int mask, int bit);

  static bool matchLeadingBFromHiggs(AnalysisObject *jet);
  static bool matchSubLeadingBFromHiggs(AnalysisObject *jet);
  static bool matchLeadingJetFromW(AnalysisObject *jet);
  static bool matchSubLeadingJetFromW(AnalysisObject *jet);
  static bool matchBFromHadTop(AnalysisObject *jet);
  static bool matchBFromLepTop(AnalysisObject *jet);

  static bool matchOnlyLeadingBFromHiggs(AnalysisObject *jet);
  static bool matchOnlySubLeadingBFromHiggs(AnalysisObject *jet);
  static bool matchOnlyLeadingJetFromW(AnalysisObject *jet);
  static bool matchOnlySubLeadingJetFromW(AnalysisObject *jet);
  static bool matchOnlyBFromHadTop(AnalysisObject *jet);
  static bool matchOnlyBFromLepTop(AnalysisObject *jet);

  static bool matchBFromHiggs(AnalysisObject *jet);
  static bool matchOnlyOneBFromHiggs(AnalysisObject *jet);
  static bool matchJetFromW(AnalysisObject *jet);
  static bool matchOnlyOneJetFromW(AnalysisObject *jet);
  //
    static bool matchLeadingBFromHiggs(int match);
  static bool matchSubLeadingBFromHiggs(int match);
  static bool matchLeadingJetFromW(int match);
  static bool matchSubLeadingJetFromW(int match);
  static bool matchBFromHadTop(int match);
  static bool matchBFromLepTop(int match);

  static bool matchOnlyLeadingBFromHiggs(int match);
  static bool matchOnlySubLeadingBFromHiggs(int match);
  static bool matchOnlyLeadingJetFromW(int match);
  static bool matchOnlySubLeadingJetFromW(int match);
  static bool matchOnlyBFromHadTop(int match);
  static bool matchOnlyBFromLepTop(int match);

  static bool matchBFromHiggs(int match);
  static bool matchOnlyOneBFromHiggs(int match);
  static bool matchJetFromW(int match);
  static bool matchOnlyOneJetFromW(int match);
  
  static std::string getJetMatchName(AnalysisObject *jet);
  static std::string getJetMatchName(int match);

private:
  JetTruthMatchHelper(); /// for now only static methods so no one need an
                         /// instance
};

#endif
