// emacs -*- C++ -*-
#ifndef TTH_TRAINING_H
#define TTH_TRAINING_H

class TFile;
class TH1;

#include <string>

namespace tth
{
/**
   @brief Holds information about the signal training

   A signal training is made of histogram templates and flavor fractions.
*/
struct TrainingSig {
    TrainingSig();
    TH1* hmbb; ///< mass h > bb candidate
    TH1* hmbbjminusmbb; ///< (m_bbj - m_bb) from t > hq > bbq candidate
    TH1* hmlvb0; ///< mass top candidate with 1 neutrino solution
    TH1* hmlvb1; ///< mass top candidate 1st neutrino (two solutions)
    TH1* hmlvb2; ///< mass top candidate 2nd neutrino (two solutions)
    float fWll; ///< flavor of quarks from W 2b case: light-light
    float fWlc; ///< flavor of quarks from W 2b case: light-charm
    float fAllMatch; ///< fraction of events with all partons matched
    float fMiss1Match; ///< fraction of events with 1 missing match
    TrainingSig& retrieveMassTemplates(TFile *f, const std::string &region);
    /**
       @brief fractions of events with all matched partons
    */
    TrainingSig& retrieveMatchingFractions(TFile *f, const std::string &region);
};
/**
   @brief Holds information about the background training

   A background training is made of histogram templates and flavor fractions.
   Do we have a reference for the numbering? (b0, b1, b2,...)? e.g.
   https://indico.cern.ch/event/569498/contributions/2316118/attachments/1344352/2029164/Likelihood_discriminant.pdf
*/
struct TrainingBkg {
    TrainingBkg();
    TH1* hmlvb0; ///< mass top candidate with 1 neutrino solution
    TH1* hmlvb1; ///< mass top candidate 1st neutrino (two solutions)
    TH1* hmlvb2; ///< mass top candidate 2nd neutrino (two solutions)
    TH1* hmqq; ///< mass W > qq candidate
    TH1* hmqqbminusmqq; ///< (m_qqb - m_qq) from t > Wb > qqb candidate
    float fWll; ///< fraction of had W decays with both light quarks
    float fWlc; ///< fraction of had W decays with one charm quark
    float fAllMatch; ///< fraction of events with all partons matched
    float fMiss1Match; ///< fraction of events with 1 missing match
    TrainingBkg& retrieveMassTemplates(TFile *f, const std::string &region);
    /**
       @brief flavor fractions of quarks from W>qq
    */
    TrainingBkg& retrieveWflavorFractions(TFile *f, const std::string &region);
    /**
       @brief fractions of events with all matched partons
    */
    TrainingBkg& retrieveMatchingFractions(TFile *f, const std::string &region);
};
} // tth
#endif // TTH_TRAINING_H

