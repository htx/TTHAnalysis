// emacs -*- c++ -*-
#ifndef TTH_TRUTHJETMATCH_H
#define TTH_TRUTHJETMATCH_H

#include <string>

namespace tth
{

class RecoJet;
class TlvTruth;
/**
   @brief Possible matches of a truth jet to a parton

   davide.gerbaudo@gmail.com
   Mar 2017
 */
enum TruthJetMatch {
    UnknownTruthJetMatch= 0, ///< not matched to anything yet
    bLepTop             = 1, ///< matched to b quark from semileptonic top
    bHadTop             = 2, ///< matched to b quark from hadronic top
    LeadJetW            = 3, ///< matched to highest-pt jet from W
    SubLeadJetW         = 4, ///< matched to second-highest-pt jet from W
    LeadbHiggs          = 5, ///< matched to highest-pt b jet from H
    SubLeadbHiggs       = 6, ///< matched to second-highest-pt b jet from H
    JetHiggsTop         = 7, ///< matched to quark from FCNC-decaying top (t>hq)
    DoubleTruthJetMatch = 25 ///< jet matched to multiple partons
};

/// Human-readable names
std::string TruthJetMatch2str(const TruthJetMatch &t);

/**
   @brief determine how the RecoJet is matched to a given set of truth TLorentzVectors

   Determine to which truth particles the reco jet is matched, and
   determine potential double matches.

*/
TruthJetMatch determineJetTruthMatch(const RecoJet &reco_jet,
                                     const TlvTruth &tlvt);


} // tth

#endif // TTH_DISCRIMINANT_H
