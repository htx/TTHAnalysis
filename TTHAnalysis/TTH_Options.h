#ifndef TTH_OPTIONS_H
#define TTH_OPTIONS_H

#include <string>
#include "IFAETopFramework/OptionsBase.h"

class TTH_Options : public OptionsBase {

public:
  enum HFFilteringType {
    NOFILTER = 0,
    TTBARLIGHT = 1,
    TTBARBB = 2,
    TTBARCC = 3,
    // HTFILTER_VETO = 4
  };
  //____________________________________________________________
  //
  TTH_Options();
  TTH_Options(const TTH_Options &);
  ~TTH_Options();

  //____________________________________________________________
  //
  virtual void PrintOptions();

  //____________________________________________________________
  inline bool UseTopPtWeight() const { return m_useTopPtWeight; }
  inline bool UseLeptonsSF() const { return m_useLeptonsSF; }
  inline bool UseBtagSF() const { return m_useBtagSF; }
  inline bool UsePileUpWeight() const { return m_usePileUpWeight; }
  inline bool DumpTree() const { return m_dumpTree; }
  inline bool DumpHistos() const { return m_dumpHistos; }
  inline bool RunDiscriminant() const { return m_runDiscriminant; }
  inline bool RunTemplate() const { return m_runTemplate; }
  inline bool RunDataMC() const { return m_runDataMC; }
  inline bool RunBoosted() const { return m_runBoosted; }
  inline std::string BtagOP() const { return m_btagOP; }
  inline double JetsPtCut() const { return m_jetsPtCut; }
  inline HFFilteringType HFFilterType() const { return m_HFfilterType; }
  inline bool DoTRF() const { return m_doTRF; }
  inline bool NormWeight() const { return m_normWeight; }
  inline bool Run3b() const { return m_run3b; }
  inline bool RunS1() const { return m_runS1; }
  inline bool RejectOdd() const { return m_rejectOdd; }
  inline bool RejectEven() const { return m_rejectEven; }
  inline int NStart() const { return m_nStart; }
  inline int NFinish() const { return m_nFinish; }
  inline bool Data2015() const { return m_data2015; }
  inline bool Data2016() const { return m_data2016; }

protected:
  virtual bool IdentifyOption(const std::string &, const std::string &);

private:
  bool m_useTopPtWeight;
  bool m_useLeptonsSF;
  bool m_useBtagSF;
  bool m_usePileUpWeight;
  bool m_dumpTree;
  bool m_dumpHistos;
  bool m_runDiscriminant;
  bool m_runTemplate;
  bool m_runDataMC;
  bool m_runBoosted;
  double m_jetsPtCut;
  std::string m_btagOP;
  HFFilteringType m_HFfilterType;
  bool m_doTRF;
  bool m_normWeight;
  bool m_run3b;
  bool m_runS1;
  bool m_rejectOdd;
  bool m_rejectEven;
  bool m_data2015;
  bool m_data2016;
  bool m_doTruthPlots;
  double m_nStart;
  double m_nFinish;
};

#endif // TTH_OPTIONS_H
