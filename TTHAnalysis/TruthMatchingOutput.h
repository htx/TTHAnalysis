// emacs -*- C++ -*-

#ifndef TTH_TRUTHMATCHINGOUTPUT_H
#define TTH_TRUTHMATCHINGOUTPUT_H

#include <string>
#include <vector>

class VLQ_OutputData;

namespace tth
{
/**
   @brief Holder of the variables computed from the truth match

   davide.gerbaudo@gmail.com
   Mar 2017
 */

class TruthMatchingOutput {
public:
    TruthMatchingOutput();
    void ClearOutputData();
    const TruthMatchingOutput& copyDataTo(VLQ_OutputData *vlqod) const;
    /// readable string representation
    std::string str() const;
    float o_Mbb;
    float o_Mqqb;
    float o_Mqq;
    float o_MqqbMinusMqq;
    float o_Mlvb0;
    float o_Mlvb1;
    float o_Mlvb2;
    float o_Mbbj;
    float o_MbbjMinusMbb;
    int o_jetFromTop_truthflav;
    float o_qFromTop_pt;
    float o_ExtraJetsbb;
    float o_ExtraJetsbj;
    float o_leadJetPt;
    float o_leadJetEta;
    int o_matchedjets_n;
    int o_matchedjetsttbar_n;
    int o_truthbextrajets_n;
    int o_truthnotbextrajets_n;
    std::vector<float> o_ExtraJetsPerm;
    std::vector<float> o_ExtraJetsPerm_ll;
    std::vector<float> o_ExtraJetsPerm_lc;
    std::vector<float> o_ExtraJetsPerm_lb;
    std::vector<float> o_ExtraJetsPerm_cc;
    std::vector<float> o_ExtraJetsPerm_bc;
    std::vector<float> o_ExtraJetsPerm_bb;
    std::vector<float> o_ExtraJetsPerm_notbb;
    std::vector<float> o_Mqq_NoLeadJetW_b;
    std::vector<float> o_Mqq_NoLeadJetW_c;
    std::vector<float> o_Mqq_NoLeadJetW_l;
    std::vector<float> o_Mqqb_NoLeadJetW_b;
    std::vector<float> o_Mqqb_NoLeadJetW_c;
    std::vector<float> o_Mqqb_NoLeadJetW_l;
    std::vector<float> o_MqqbMinusMqq_NoLeadJetW_b;
    std::vector<float> o_MqqbMinusMqq_NoLeadJetW_c;
    std::vector<float> o_MqqbMinusMqq_NoLeadJetW_l;
    std::vector<float> o_Mqq_NoSubLeadJetW_b;
    std::vector<float> o_Mqq_NoSubLeadJetW_c;
    std::vector<float> o_Mqq_NoSubLeadJetW_l;
    std::vector<float> o_Mqqb_NoSubLeadJetW_b;
    std::vector<float> o_Mqqb_NoSubLeadJetW_c;
    std::vector<float> o_Mqqb_NoSubLeadJetW_l;
    std::vector<float> o_MqqbMinusMqq_NoSubLeadJetW_b;
    std::vector<float> o_MqqbMinusMqq_NoSubLeadJetW_c;
    std::vector<float> o_MqqbMinusMqq_NoSubLeadJetW_l;
    std::vector<int> o_ExtraJetNoLeadJetW_truthflav;
    std::vector<int> o_ExtraJetNoSubLeadJetW_truthflav;
    float o_Mqq_NoLeadJetW_Jet_1_b;
    float o_Mqq_NoLeadJetW_Jet_1_c;
    float o_Mqq_NoLeadJetW_Jet_1_l;
    float o_Mqqb_NoLeadJetW_Jet_1_b;
    float o_Mqqb_NoLeadJetW_Jet_1_c;
    float o_Mqqb_NoLeadJetW_Jet_1_l;
    float o_MqqbMinusMqq_NoLeadJetW_Jet_1_b;
    float o_MqqbMinusMqq_NoLeadJetW_Jet_1_c;
    float o_MqqbMinusMqq_NoLeadJetW_Jet_1_l;
    float o_Mqq_NoSubLeadJetW_Jet_1_b;
    float o_Mqq_NoSubLeadJetW_Jet_1_c;
    float o_Mqq_NoSubLeadJetW_Jet_1_l;
    float o_Mqqb_NoSubLeadJetW_Jet_1_b;
    float o_Mqqb_NoSubLeadJetW_Jet_1_c;
    float o_Mqqb_NoSubLeadJetW_Jet_1_l;
    float o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b;
    float o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c;
    float o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l;
    int o_ExtraJetNoLeadJetW_Jet_1_truthflav;
    int o_ExtraJetNoSubLeadJetW_Jet_1_truthflav;
    std::vector<float> o_jets_pt;
    std::vector<float> o_jets_eta;
    int o_leadJetFromW_truthflav;
    int o_subJetFromW_truthflav;
    int o_extraJet1_truthflav;
    int o_extraJet2_truthflav;
    int o_extrajetleadnotb_truthflav;
    float o_Mqq_Jet_1;
    float o_Mqqb_Jet_1;
    float o_MqqbMinusMqq_Jet_1;
    int o_jetFromW5matched_truthflav;
    int o_extrajet5matched_truthflav;
    std::vector<int> o_extraJet1Perm_truthflav;
    std::vector<int> o_extraJet2Perm_truthflav;
    std::vector<int> o_jets_truthflav;
    std::vector<int> o_jets_truthmatch;
    size_t o_nottruthmatch; ///< flag indicating who is not matched in the nmatched==3 case
    int o_nextrajets;
};
} // tth

#endif // TTH_TRUTHMATCHINGOUTPUT_H
