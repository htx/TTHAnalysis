// emacs -*- C++ -*-

#ifndef TTH_RECOJET_H
#define TTH_RECOJET_H

#include "TTHAnalysis/TruthJetMatch.h"
#include "TLorentzVector.h"

#include <string>

namespace tth
{

class BtagWeights;

/**
   @brief a TLV that also holds the reco info from btag and from truth matching
*/
class RecoJet : public TLorentzVector {
public:
    RecoJet();
    RecoJet& setPtEtaPhiEnergy(const float &pt, const float &eta, const float &phi, const float &energy);
    RecoJet& setBtagAttributes(const tth::BtagWeights &weights);
    RecoJet& setTruthFlavor(const int &flavor);
    RecoJet& setTruthMatch(const TruthJetMatch &match);
    double tagger_output;
    double probL;
    double probC;
    double probB;
    double probBbinary;
    int truthflav; ///< truth flavor from xAOD::Jet HadronConeExclTruthLabelID attribute
    TruthJetMatch truthmatch; ///< matching info we build from the top and higgs decays
    /// @brief one-line printable summary
    std::string str() const;
    /// @brief one-line printable summary w/ truthmatch
    std::string str_truthmatch() const;
};
} // tth
#endif // TTH_RECOJET_H
