// emacs -*- C++ -*-

#ifndef TTH_BTAGWEIGHTS_H
#define TTH_BTAGWEIGHTS_H


namespace tth
{
struct BtagWeights {
    double tagger_output; ///< output of the tagger (input, for bookkeeping)
    double probability_l_tag; ///< p of light parton to be above b-tag threshold
    double probability_c_tag; ///< p of charm quark to be above b-tag threshold
    double probability_b_tag; ///< p of bottom quark to be above b-tag threshold
    double probability_b_tag_binary; ///< binary probabibily, 0 below thr, 1 above
};

/**
   @brief from the output of mv2c10, determine the jet weight (or probability) to be tagged b,c, or light

   Using the 77% operating point.

   For b jet, this is equivalent to the tagging eff (if above
   threshold) or to 1 - eff (if below threshold). Similarly for
   the c and light efficiencies.

   @verbatim
   p(tag_out | b-jet)

   ^
   |            +---------
   |            |
   |  ----------+
   |  100%-77%      77%
   +----------------------->   tag_out
      <th_77      >th_77
   @endverbatim

   See the last paragraph from section 6.2.1 of CERN-THESIS-2010-325.
*/
BtagWeights GetBTagWeights77OP(const float &taggerOutput);
/**
   @brief from the output of mv2c10, determine the jet weight (or probability) to be tagged b,c, or light

   A generalization of GetBTagWeights77OP, where we think about
   the tagger output for a given b-jet to be the discrete PDF with
   edges given by the available OPs:

   @verbatim
   p(tag_out | b-jet)

   ^                               +--------
   |                               |
   |________                       |
   |        |              +-------+
   |        +------+_______|
   |
   |  15%      8%      7%     10%      60%
   +-------------------------------------------->  tag_out
     <th_85   >th_85  >th_77   >th_70   >th_60
   @endverbatim
*/
BtagWeights GetBTagWeightsVarOP(const float &taggerOutput);

} // tth
#endif // TTH_BTAGWEIGHTS_H
