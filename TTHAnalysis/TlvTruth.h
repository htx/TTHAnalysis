// emacs -*- C++ -*-

#ifndef TTH_TLVTRUTH_H
#define TTH_TLVTRUTH_H

#include "TLorentzVector.h"

namespace tth
{
/**
   @brief Holder of the TLorentzVector we need from the truth record

   davide.gerbaudo@gmail.com
   Oct 2017
 */
struct TlvTruth {
    TLorentzVector bleptop1;
    TLorentzVector bleptop2;
    TLorentzVector bhadtop;
    TLorentzVector leadqW;
    TLorentzVector subqW;
    TLorentzVector leadbH;
    TLorentzVector subbH;
    TLorentzVector qfromHtop;
    TLorentzVector bfromH1;// first b from Higgs found before ranking them in pt
    TLorentzVector bfromH2;// second b from Higgs found before ranking them in pt
    bool has_bleptop1;
    bool has_bleptop2;
    bool has_bhadtop;
    bool has_leadqW;
    bool has_subqW;
    bool has_leadbH;
    bool has_subbH;
    bool has_qfromHtop;
    bool has_bfromH1;
    bool has_bfromH2;
    TlvTruth():
        has_bleptop1(false),
        has_bleptop2(false),
        has_bhadtop(false),
        has_leadqW(false),
        has_subqW(false),
        has_leadbH(false),
        has_subbH(false),
        has_qfromHtop(false),
        has_bfromH1(false),
        has_bfromH2(false)
        {};
};
} // tth

#endif // TTH_TLVTRUTH_H

