// emacs -*- C++ -*-
#ifndef TTH_TRUTHPRINTER_H
#define TTH_TRUTHPRINTER_H
#include <vector>


namespace tth
{
/**
   @brief Helper to print the decay chain from a "IFAETopFramework-like" ntuple

   At some point we might want to rename it and do some truth
   classification with it.

   davide.gerbaudo@gmail.com
   Nov 2016
 */
class TruthPrinter {
public:
    /// print the decay table
    void printTable(const std::vector < float > &mc_pt,
                    const std::vector < float > &mc_eta,
                    const std::vector < float > &mc_phi,
                    const std::vector < float > &mc_m,
                    const std::vector < float > &mc_pdgId,
                    const std::vector < std::vector < int > > &mc_children_index) const;
    /// index of the hadronic W; -1 if none, first one if more than one
    int indexHadronicW(const std::vector < float > &mc_pdgId,
                       const std::vector < std::vector < int > > &mc_children_index) const;
    /// conver the 'tth' truth flavor to the bins used in the "jet flavor" (light, c, and b)
    static int their_truthflav_to_mytruthflav(const int flav_in);
    /// convert the pdg truth flavor to the bins used in the "jet flavor" (light, c, and b)
    static int pdg_truthflav_to_mytruthflav(const int &pdg);
    static bool is_thc_event(const std::vector < float > &mc_pdgId,
                             const std::vector < std::vector < int > > &mc_children_index);

}; // TruthPrinter
} // tth

#endif // TTH_DISCRIMINANT_H
