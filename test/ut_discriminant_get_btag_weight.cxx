/**
 * Unit test for TTH_Discriminant::GetBTagWeights70OP
 *
 * Expected output:
 *
 * btag: -0.5 : weights[3] : 0.992556  0.838969 0.23
 * btag: 0    : weights[3] : 0.992556  0.838969 0.23
 * btag: 0.01 : weights[3] : 0.992556  0.838969 0.23
 * btag: 0.1  : weights[3] : 0.992556  0.838969 0.23
 * btag: 1    : weights[3] : 0.0074438 0.161031 0.77
 *
 * davide.gerbaudo@gmail.com
 * Oct 2016
 */

#include "TTHAnalysis/BtagWeights.h"

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<float> btags = {-0.5, 0.0, 0.01, 0.10, 1.0};
    for(float btag : btags){
        cout<<"btag: "<<btag<<" : ";
        tth::BtagWeights weights = tth::GetBTagWeights77OP(btag);
        cout<<"weights[3] : "
            <<weights.probability_l_tag<<" "
            <<weights.probability_c_tag<<" "
            <<weights.probability_b_tag<<" "
            <<endl;
    }
    return 0;
}

