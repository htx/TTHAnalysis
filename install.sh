#!/bin/bash

#
# This script allows to install the specific packages needed to compile this one.
# WARNING: The first check is the existence of IFAETopFramework which is mandatory
# Usage:
#    source instal.sh <svn username>
#


problemsIFAETop=0
if [ ! -d $ROOTCOREBIN/../IFAETopFramework ]; then
    echo $red"===> Problem: IFAETopFramework doesn't seem to be installed !"$normal
    problemsIFAETop = 1
else
    echo $green"=> IFAETopFramework already installed !"$normal
fi

#
# Now this is checked, instal the needed packages
#
if [[ "$problemsIFAETop" == "0" ]]; then
    echo $green"=> No additional packages needed !"$normal
fi
