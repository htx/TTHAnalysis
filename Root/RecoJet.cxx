#include "TTHAnalysis/RecoJet.h"

#include "TTHAnalysis/BtagWeights.h"
#include "TTHAnalysis/TruthJetMatch.h"

#include <sstream>

namespace tth
{
//----------------------------------------------------------
RecoJet::RecoJet():
    tagger_output(0.0),
    probL(0.0),
    probC(0.0),
    probB(0.0),
    probBbinary(0.0),
    truthflav(0),
    truthmatch(TruthJetMatch::UnknownTruthJetMatch)
{
}
//----------------------------------------------------------
//----------------------------------------------------------
RecoJet& RecoJet::setPtEtaPhiEnergy(const float &pt,
                                    const float &eta,
                                    const float &phi,
                                    const float &energy)
{
    SetPtEtaPhiE(pt, eta, phi, energy);
    return *this;
}
//----------------------------------------------------------
RecoJet& RecoJet::setBtagAttributes(const BtagWeights &weights)
{
    tagger_output = weights.tagger_output;
    probL = weights.probability_l_tag;
    probC = weights.probability_c_tag;
    probB = weights.probability_b_tag;
    probBbinary = weights.probability_b_tag_binary;
    return *this;
}
//----------------------------------------------------------
RecoJet& RecoJet::setTruthFlavor(const int &value)
{
    truthflav = value;
    return *this;
}
//----------------------------------------------------------
RecoJet& RecoJet::setTruthMatch(const TruthJetMatch &value)
{
    truthmatch = value;
    return *this;
}
//----------------------------------------------------------
std::string RecoJet::str() const
{
    std::ostringstream oss;
    oss<<"("<<Pt()<<", "<<Eta()<<", "<<Phi()<<")";
    return oss.str();
}
//----------------------------------------------------------
std::string RecoJet::str_truthmatch() const
{
    std::ostringstream oss;
    oss<<"("<<Pt()<<", "<<Eta()<<", "<<Phi()<<", "<<TruthJetMatch2str(truthmatch)<<")";
    return oss.str();
}
//----------------------------------------------------------

} // tth
