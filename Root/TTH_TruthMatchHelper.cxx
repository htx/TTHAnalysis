#include "TTHAnalysis/TTH_TruthMatchHelper.h"

bool JetTruthMatchHelper::bitMatch(int mask, int bit) {
  return mask & (1 << bit);
}
bool JetTruthMatchHelper::bitMatchOnly(int mask, int bit) {
  return (mask & 63) == (1 << bit);
}

bool JetTruthMatchHelper::matchLeadingBFromHiggs(AnalysisObject *jet) {
  return bitMatch(jet->GetMoment("truthmatch"), JetMatchLeadingBHiggs);
}
bool JetTruthMatchHelper::matchSubLeadingBFromHiggs(AnalysisObject *jet) {
  return bitMatch(jet->GetMoment("truthmatch"), JetMatchSubLeadingBHiggs);
}
bool JetTruthMatchHelper::matchLeadingJetFromW(AnalysisObject *jet) {
  return bitMatch(jet->GetMoment("truthmatch"), JetMatchLeadingJetW);
}
bool JetTruthMatchHelper::matchSubLeadingJetFromW(AnalysisObject *jet) {
  return bitMatch(jet->GetMoment("truthmatch"), JetMatchSubLeadingJetW);
}
bool JetTruthMatchHelper::matchBFromHadTop(AnalysisObject *jet) {
  return bitMatch(jet->GetMoment("truthmatch"), JetMatchBHadTop);
}
bool JetTruthMatchHelper::matchBFromLepTop(AnalysisObject *jet) {
  return bitMatch(jet->GetMoment("truthmatch"), JetMatchBLepTop);
}

bool JetTruthMatchHelper::matchOnlyLeadingBFromHiggs(AnalysisObject *jet) {
  return bitMatchOnly(jet->GetMoment("truthmatch"), JetMatchLeadingBHiggs);
}
bool JetTruthMatchHelper::matchOnlySubLeadingBFromHiggs(AnalysisObject *jet) {
  return bitMatchOnly(jet->GetMoment("truthmatch"), JetMatchSubLeadingBHiggs);
}
bool JetTruthMatchHelper::matchOnlyLeadingJetFromW(AnalysisObject *jet) {
  return bitMatchOnly(jet->GetMoment("truthmatch"), JetMatchLeadingJetW);
}
bool JetTruthMatchHelper::matchOnlySubLeadingJetFromW(AnalysisObject *jet) {
  return bitMatchOnly(jet->GetMoment("truthmatch"), JetMatchSubLeadingJetW);
}
bool JetTruthMatchHelper::matchOnlyBFromHadTop(AnalysisObject *jet) {
  return bitMatchOnly(jet->GetMoment("truthmatch"), JetMatchBHadTop);
}
bool JetTruthMatchHelper::matchOnlyBFromLepTop(AnalysisObject *jet) {
  return bitMatchOnly(jet->GetMoment("truthmatch"), JetMatchBLepTop);
}

bool JetTruthMatchHelper::matchBFromHiggs(AnalysisObject *jet) {
  return matchLeadingBFromHiggs(jet) || matchSubLeadingBFromHiggs(jet);
}
bool JetTruthMatchHelper::matchOnlyOneBFromHiggs(AnalysisObject *jet) {
  return matchOnlyLeadingBFromHiggs(jet) || matchOnlySubLeadingBFromHiggs(jet);
}
bool JetTruthMatchHelper::matchJetFromW(AnalysisObject *jet) {
  return matchLeadingJetFromW(jet) || matchSubLeadingJetFromW(jet);
}
bool JetTruthMatchHelper::matchOnlyOneJetFromW(AnalysisObject *jet) {
  return matchOnlyLeadingJetFromW(jet) || matchOnlySubLeadingJetFromW(jet);
}




//


bool JetTruthMatchHelper::matchLeadingBFromHiggs(int match) {
  return bitMatch(match, JetMatchLeadingBHiggs);
}
bool JetTruthMatchHelper::matchSubLeadingBFromHiggs(int match) {
  return bitMatch(match, JetMatchSubLeadingBHiggs);
}
bool JetTruthMatchHelper::matchLeadingJetFromW(int match) {
  return bitMatch(match, JetMatchLeadingJetW);
}
bool JetTruthMatchHelper::matchSubLeadingJetFromW(int match) {
  return bitMatch(match, JetMatchSubLeadingJetW);
}
bool JetTruthMatchHelper::matchBFromHadTop(int match) {
  return bitMatch(match, JetMatchBHadTop);
}
bool JetTruthMatchHelper::matchBFromLepTop(int match) {
  return bitMatch(match, JetMatchBLepTop);
}

bool JetTruthMatchHelper::matchOnlyLeadingBFromHiggs(int match) {
  return bitMatchOnly(match, JetMatchLeadingBHiggs);
}
bool JetTruthMatchHelper::matchOnlySubLeadingBFromHiggs(int match) {
  return bitMatchOnly(match, JetMatchSubLeadingBHiggs);
}
bool JetTruthMatchHelper::matchOnlyLeadingJetFromW(int match) {
  return bitMatchOnly(match, JetMatchLeadingJetW);
}
bool JetTruthMatchHelper::matchOnlySubLeadingJetFromW(int match) {
  return bitMatchOnly(match, JetMatchSubLeadingJetW);
}
bool JetTruthMatchHelper::matchOnlyBFromHadTop(int match) {
  return bitMatchOnly(match, JetMatchBHadTop);
}
bool JetTruthMatchHelper::matchOnlyBFromLepTop(int match) {
  return bitMatchOnly(match, JetMatchBLepTop);
}

bool JetTruthMatchHelper::matchBFromHiggs(int match) {
  return matchLeadingBFromHiggs(match) || matchSubLeadingBFromHiggs(match);
}
bool JetTruthMatchHelper::matchOnlyOneBFromHiggs(int match) {
  return matchOnlyLeadingBFromHiggs(match) || matchOnlySubLeadingBFromHiggs(match);
}
bool JetTruthMatchHelper::matchJetFromW(int match) {
  return matchLeadingJetFromW(match) || matchSubLeadingJetFromW(match);
}
bool JetTruthMatchHelper::matchOnlyOneJetFromW(int match) {
  return matchOnlyLeadingJetFromW(match) || matchOnlySubLeadingJetFromW(match);
}

//



std::string JetTruthMatchHelper::getJetMatchName(AnalysisObject *jet) {

  std::string smatch = "";

  // if(matchBFromHiggs(jet))smatch+="bHiggs";
  if (matchLeadingBFromHiggs(jet))
    smatch += "LeadbHiggs";
  if (matchSubLeadingBFromHiggs(jet))
    smatch += "SubLeadbHiggs";
  if (matchBFromLepTop(jet))
    smatch += "bLepTop";
  if (matchBFromHadTop(jet))
    smatch += "bHadTop";
  // if(matchJetFromW(jet))smatch+="jetW";
  if (matchLeadingJetFromW(jet))
    smatch += "LeadJetW";
  if (matchSubLeadingJetFromW(jet))
    smatch += "SubLeadJetW";

  return smatch;
}

std::string JetTruthMatchHelper::getJetMatchName(int match) {

  std::string smatch = "";

  // if(matchBFromHiggs(jet))smatch+="bHiggs";
  if (matchLeadingBFromHiggs(match))
    smatch += "LeadbHiggs";
  if (matchSubLeadingBFromHiggs(match))
    smatch += "SubLeadbHiggs";
  if (matchBFromLepTop(match))
    smatch += "bLepTop";
  if (matchBFromHadTop(match))
    smatch += "bHadTop";
  // if(matchJetFromW(match))smatch+="jetW";
  if (matchLeadingJetFromW(match))
    smatch += "LeadJetW";
  if (matchSubLeadingJetFromW(match))
    smatch += "SubLeadJetW";

  return smatch;
}
