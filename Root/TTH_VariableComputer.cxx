#include <iostream>

#include "IFAETopFramework/OptionsBase.h"
#include "IFAETopFramework/AnalysisObject.h"

#include "TTHAnalysis/TTH_VariableComputer.h"


std::vector<TLorentzVector>
TTH_VariableComputer::NuSolution(TLorentzVector &l, TLorentzVector &nu) {

  std::vector<TLorentzVector> nuSolutions;
  TLorentzVector nuSolution;
  //jr
  //double WMass = 80385;
  double WMass = 80.385;

  double MissingPt =
      sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py()); ///////////////?!!

  double alpha = 0.5 * (WMass * WMass - l.M() * l.M());
  double beta = alpha + nu.Px() * l.Px() + nu.Py() * l.Py();
  double gamma = -(beta * beta - (l.E() * l.E() * MissingPt * MissingPt)) /
                 (l.E() * l.E() - l.Pz() * l.Pz());
  double lambda = 2. * beta * l.Pz() / (l.E() * l.E() - l.Pz() * l.Pz());
  double delta = lambda * lambda - 4. * gamma;

  if (delta < 0) {
    double L = sqrt(l.E() * l.E() - l.Pz() * l.Pz());
    double M = sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py());
    double N = nu.Px() * l.Px() + nu.Py() * l.Py();
    double Alfa1 = -alpha * (N + M * L) / (N * N - M * M * L * L);
    double Alfa2 = -alpha * (N - M * L) / (N * N - M * M * L * L);
    double Alfa;
    if (Alfa1 > 0)
      Alfa = Alfa1;
    if (Alfa2 > 0)
      Alfa = Alfa2;
    TLorentzVector nu2;
    nu2.SetPxPyPzE(nu.Px() * Alfa, nu.Py() * Alfa, nu.Pz() * Alfa,
                   nu.E() * Alfa);
    double betaPrime2 = alpha + nu2.Px() * l.Px() + nu2.Py() * l.Py();
    double lambdaPrime2 =
        2. * betaPrime2 * l.Pz() / (l.E() * l.E() - l.Pz() * l.Pz());
    double Pz2 = lambdaPrime2 / 2;
    double E2 = sqrt(nu2.Px() * nu2.Px() + nu2.Py() * nu2.Py() + Pz2 * Pz2);
    nuSolution.SetPxPyPzE(nu2.Px(), nu2.Py(), Pz2, E2);
    nuSolutions.push_back(nuSolution);
    // return nuSolutions;
  }

  else {
    double Pz1 = (lambda - sqrt(delta)) / 2.;
    double Pz2 = (lambda + sqrt(delta)) / 2.;
    double E1 = sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py() + Pz1 * Pz1);
    double E2 = sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py() + Pz2 * Pz2);
    if (TMath::Abs(Pz1) < TMath::Abs(Pz2)) {
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz1, E1);
      nuSolutions.push_back(nuSolution);
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz2, E2);
      nuSolutions.push_back(nuSolution);
    } else {
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz2, E2);
      nuSolutions.push_back(nuSolution);
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz1, E1);
      nuSolutions.push_back(nuSolution);
    }
  }
  return nuSolutions;
}
