
#include "TTHAnalysis/TTH_NtupleReader.h"
#include "TTHAnalysis/TTH_NtupleData.h"
#include "TTHAnalysis/TTH_Options.h"

//#include "IFAETopFramework/OptionsBase.h"

#include "TChain.h"

//_________________________________________________________________________________
//
// TTH_NtupleReader::TTH_NtupleReader(TTH_NtupleData* ntupData, OptionsBase*
// opt):
// TTH_NtupleReader::TTH_NtupleReader(OptionsBase* opt):
TTH_NtupleReader::TTH_NtupleReader(TTH_Options *opt)
    : NtupleReader(opt), m_TTH_ntupData(NULL)
// m_opt(opt)
// b_eventNumber(0),////////events
// b_runNumber(0),
// //b_trf_weight_77_4in(0),
// //b_trf_weight_77_4in(0),
// b_trf_weight_70_4in(0),
// b_trf_weight_70_3ex(0),
// b_mu(0),
// b_HF_SimpleClassification(0),
// b_TopHeavyFlavorFilterFlag(0),
// b_jets_n(0),/////////jets
// b_jets_pt(0),
// b_jets_eta(0),
// b_jets_phi(0),
// b_jets_e(0),
// b_jets_mv2c10(0),
// b_jets_mv2c20(0),
// b_jets_truthflav(0),
// b_jets_truthmatch(0),
// b_truthHiggsDaughtersID(0),
// b_HhadT_jets(0),
// //b_e_jets(0),
// //b_mu_jets(0),
// b_e_jets_2015(0),
// b_mu_jets_2015(0),
// b_e_jets_2016(0),
// b_mu_jets_2016(0),
// b_boosted_e_jets(0),
// b_boosted_mu_jets(0),
// //b_jets_truth_index(0),
// b_ljets_pt(0),//////////ljets
// b_ljets_eta(0),
// b_ljets_m(0),
// b_ljets_sd12(0),
// b_ljets_sd23(0),
// b_ljets_tau21(0),
// b_ljets_tau32(0),
// b_ljets_topTag_loose(0),
// b_ljets_topTag_tight(0),
// b_ljets_topTagN_loose(0),
// b_ljets_topTagN_tight(0),
// b_el_pt(0),//////////electrons
// b_el_eta(0),
// b_el_phi(0),
// b_el_e(0),
// b_mu_pt(0),//////////muons
// b_mu_eta(0),
// b_mu_phi(0),
// b_mu_e(0),
// b_met_met(0),/////////met
// b_met_phi(0),
// b_eventWeight_pileup(0),////////////weights
// b_eventWeight_mc(0),
// b_eventWeight_leptonSF(0),
// b_eventWeight_bTagSF(0),
// b_weight_jvt(0),
// //b_evt_w_Nom(0),
// //b_evt_w_Btag1(0),
// //b_evt_w_Btag2(0),
// //m_ntupData(ntupData)
// m_ntupData(NULL),
// m_opt(opt)
{
  m_TTH_ntupData = new TTH_NtupleData();
  m_ntupData = dynamic_cast<NtupleData *>(m_TTH_ntupData);
}

//_________________________________________________________________________________
//
TTH_NtupleReader::~TTH_NtupleReader() {}
//{delete m_TTH_ntupData;}

//_________________________________________________________________________________
//

int TTH_NtupleReader::SetAllBranchAddresses() {
  // std::cout << "debug 2: Inside SetAllBranchAddresses" << std::endl;

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Entering in TTH_NtupleReader::setAllBranchAddresses()"
              << std::endl;
  // std::cout << "debug 2 1" << std::endl;

  // int stat=0;
  int stat = NtupleReader::SetAllBranchAddresses();
  // std::cout << "debug 2 2" << std::endl;

  if (stat != 0) {
    std::cerr << "TTH_NtupleReader::SetAllBranchAddresses --> Failed "
                 "NtupleReader::SetAllBranchAddress. Exiting" << std::endl;
    return stat;
  }
  // std::cout << "debug 2 3" << std::endl;

  stat = SetEventBranchAddresses("");
  // std::cout << "debug 2 4" << std::endl;

  if (stat != 0) {
    std::cerr << "TTH_NtupleReader::SetAllBranchAddresses --> Failed "
                 "TTH_NtupleReader::SetEventBranchAddress. Exiting"
              << std::endl;
    return stat;
  }
  // std::cout << "debug 2 5" << std::endl;

  stat = SetJetBranchAddresses("");
  if (stat != 0) {
    std::cerr << "TTH_NtupleReader::SetAllBranchAddresses --> Failed "
                 "TTH_NtupleReader::SetJetBranchAddress. Exiting" << std::endl;
    return stat;
  }
  stat = SetLeptonBranchAddresses("");
  if (stat != 0) {
    std::cerr << "TTH_NtupleReader::SetAllBranchAddresses --> Failed "
                 "TTH_NtupleReader::SetLeptonBranchAddress. Exiting"
              << std::endl;
    return stat;
  }
  stat = SetMETBranchAddresses("");
  if (stat != 0) {
    std::cerr << "TTH_NtupleReader::SetAllBranchAddresses --> Failed "
                 "TTH_NtupleReader::SetMETBranchAddress. Exiting" << std::endl;
    return stat;
  }
  stat = SetWeightBranchAddresses("");
  if (stat != 0) {
    std::cerr << "TTH_NtupleReader::SetAllBranchAddresses --> Failed "
                 "TTH_NtupleReader::SetWeightBranchAddress. Exiting"
              << std::endl;
    return stat;
  }
  // std::cout << "m_ntupData -> d_eventNumber = " <<m_ntupData ->
  // d_eventNumber<< std::endl;
  // std::cout << "m_TTH_ntupData -> d_mu = " <<m_TTH_ntupData -> d_mu<<
  // std::endl;
  // std::cout << "m_TTH_ntupData -> d_jets_pt->size() = " <<m_TTH_ntupData ->
  // d_jets_pt->size()<< std::endl;
  // std::cout << "m_ntupData -> d_jets_pt->size() = " <<m_ntupData ->
  // d_jets_pt->size()<< std::endl;
  // std::cout << "m_TTH_ntupData -> d_el_pt->at(0) = " <<m_TTH_ntupData ->
  // d_el_pt->at(0)<< std::endl;
  // std::cout << "m_TTH_ntupData -> d_jets_pt->at(0) = " <<m_TTH_ntupData ->
  // d_jets_pt->at(0)<< std::endl;
  // std::cout << "m_TTH_ntupData -> d_el_pt->at(0) = " <<m_TTH_ntupData ->
  // d_el_pt->at(0)<< std::endl;

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Leaving TTH_NtupleReader::setAllBranchAddresses()"
              << std::endl;
  return stat;
}

int TTH_NtupleReader::SetEventBranchAddresses(const std::string & /*sj*/) {

  // std::cout << "debug 2 3 1" << std::endl;
  // std::cout << "m_opt->MsgLevel()= " << m_opt->MsgLevel() << std::endl;

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Entering in TTH_NtupleReader::SetEventBranchAddresses()"
              << std::endl;
  // std::cout << "debug 2 3 2" << std::endl;

  int stat = 0;

  ///////////////////////////////////////////////////////////////////////////
  // m_chain -> SetBranchStatus("eventNumber",1);
  // m_chain -> SetBranchStatus("runNumber",1);

  // //m_chain -> SetBranchStatus("trf_weight_77_4in",1);
  // //m_chain -> SetBranchStatus("trf_weight_77_3ex",1);

  // m_chain -> SetBranchStatus("trf_weight_70_4in",1);
  // m_chain -> SetBranchStatus("trf_weight_70_3ex",1);

  // m_chain -> SetBranchStatus("mu",1);
  // m_chain -> SetBranchStatus("HF_SimpleClassification",1);
  // m_chain -> SetBranchStatus("TopHeavyFlavorFilterFlag",1);

  // m_chain -> SetBranchStatus("truthHiggsDaughtersID",1);
  // m_chain -> SetBranchStatus("truth_ttbar_pt",1);
  // m_chain -> SetBranchStatus("truth_top_pt",1);

  ///////////////////////////////////////////////////////////////////////////
  stat += SetVariableToChain("eventNumber", &(m_TTH_ntupData->d_eventNumber));
  // if(stat != 0){ return stat; }
  stat += SetVariableToChain("runNumber", &(m_TTH_ntupData->d_runNumber));
  stat += SetVariableToChain("trf_weight_70_4in",
                             &(m_TTH_ntupData->d_trf_weight_70_4in));
  stat += SetVariableToChain("trf_weight_70_3ex",
                             &(m_TTH_ntupData->d_trf_weight_70_3ex));
  stat += SetVariableToChain("mu", &(m_TTH_ntupData->d_mu));
  stat += SetVariableToChain("HF_SimpleClassification",
                             &(m_TTH_ntupData->d_HF_SimpleClassification));
  stat += SetVariableToChain("TopHeavyFlavorFilterFlag",
                             &(m_TTH_ntupData->d_TopHeavyFlavorFilterFlag));
  // stat +=  SetVariableToChain("truthHiggsDaughtersID",
  // &(m_TTH_ntupData->d_truthHiggsDaughtersID) );
  stat +=
      SetVariableToChain("truth_ttbar_pt", &(m_TTH_ntupData->d_truth_ttbar_pt));
  stat += SetVariableToChain("truth_top_pt", &(m_TTH_ntupData->d_truth_top_pt));
  // std::cout << "2 m_TTH_ntupData -> d_mu = " <<m_TTH_ntupData -> d_mu<<
  // std::endl;

  // std::cout << "2 m_TTH_ntupData -> d_runNumber = " <<m_TTH_ntupData ->
  // d_runNumber<< std::endl;

  //////////////////////////////////////////////////////////////////////////////
  // m_chain->SetBranchAddress("eventNumber", &(m_ntupData -> d_eventNumber),
  // &b_eventNumber                     );
  // m_chain->SetBranchAddress("runNumber",   &(m_ntupData -> d_runNumber),
  // &b_runNumber                       );
  // //m_chain->SetBranchAddress("trf_weight_77_4in",   &(m_ntupData ->
  // d_trf_weight_77_4in),    &b_trf_weight_77_4in                       );
  // //m_chain->SetBranchAddress("trf_weight_77_3ex",   &(m_ntupData ->
  // d_trf_weight_77_3ex),    &b_trf_weight_77_3ex                  );
  // m_chain->SetBranchAddress("trf_weight_70_4in",   &(m_ntupData ->
  // d_trf_weight_70_4in),    &b_trf_weight_70_4in                       );
  // m_chain->SetBranchAddress("trf_weight_70_3ex",   &(m_ntupData ->
  // d_trf_weight_70_3ex),    &b_trf_weight_70_3ex                  );

  // m_chain->SetBranchAddress("mu",           &(m_ntupData -> d_mu),
  // &b_mu                       );
  // m_chain->SetBranchAddress("HF_SimpleClassification", &(m_ntupData ->
  // d_HF_SimpleClassification),  &b_HF_SimpleClassification);
  // m_chain -> SetBranchAddress("TopHeavyFlavorFilterFlag",        &(m_ntupData
  // -> d_TopHeavyFlavorFilterFlag),               &b_TopHeavyFlavorFilterFlag);
  // m_chain -> SetBranchAddress("truthHiggsDaughtersID",        &(m_ntupData ->
  // d_truthHiggsDaughtersID),               &b_truthHiggsDaughtersID);
  // m_chain -> SetBranchAddress("truth_ttbar_pt",        &(m_ntupData ->
  // d_truth_ttbar_pt),               &b_truth_ttbar_pt);
  // m_chain -> SetBranchAddress("truth_top_pt",        &(m_ntupData ->
  // d_truth_top_pt),               &b_truth_top_pt);

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Leaving TTH_NtupleReader::SetEventBranchAddresses()"
              << std::endl;

  // return 0;
  return stat;
}

//_________________________________________________________________________________
//
int TTH_NtupleReader::SetJetBranchAddresses(const std::string & /*sj*/) {

  //
  // Here are associated the branches related to jets.
  //

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Entering in TTH_NtupleReader::SetJetBranchAddresses()"
              << std::endl;
  int stat = 0;
  //////////////////////////////////////////////////////////////////////////////
  stat += SetVariableToChain("nJets", &(m_TTH_ntupData->d_jets_n));
  stat += SetVariableToChain("jet_pt", &(m_TTH_ntupData->d_jets_pt));
  stat += SetVariableToChain("jet_eta", &(m_TTH_ntupData->d_jets_eta));
  stat += SetVariableToChain("jet_phi", &(m_TTH_ntupData->d_jets_phi));
  stat += SetVariableToChain("jet_e", &(m_TTH_ntupData->d_jets_e));
  stat += SetVariableToChain("jet_mv2c10", &(m_TTH_ntupData->d_jets_mv2c10));
  stat += SetVariableToChain("jet_mv2c20", &(m_TTH_ntupData->d_jets_mv2c20));
  stat +=
      SetVariableToChain("jet_truthflav", &(m_TTH_ntupData->d_jets_truthflav));
  stat += SetVariableToChain("jet_truthmatch",
                             &(m_TTH_ntupData->d_jets_truthmatch));
  stat += SetVariableToChain("HT_jets", &(m_TTH_ntupData->d_HhadT_jets));
  stat += SetVariableToChain("ejets_2015", &(m_TTH_ntupData->d_e_jets_2015));
  stat += SetVariableToChain("mujets_2015", &(m_TTH_ntupData->d_mu_jets_2015));
  stat += SetVariableToChain("ejets_2016", &(m_TTH_ntupData->d_e_jets_2016));
  stat += SetVariableToChain("mujets_2016", &(m_TTH_ntupData->d_mu_jets_2016));
  // stat +=  SetVariableToChain("boosted_ejets",
  // &(m_TTH_ntupData->d_boosted_e_jets) );
  // stat +=  SetVariableToChain("boosted_mujets",
  // &(m_TTH_ntupData->d_boosted_mu_jets) );
  // std::cout << "2 m_TTH_ntupData -> d_jets_pt->size() = " <<m_TTH_ntupData ->
  // d_jets_pt->size()<< std::endl;

  ///////////////////////jets
  // m_chain -> SetBranchAddress("nJets",        &(m_ntupData -> d_jets_n),
  // &b_jets_n);
  // m_chain -> SetBranchAddress("jet_pt",       &(m_ntupData -> d_jets_pt),
  // &b_jets_pt);
  // m_chain -> SetBranchAddress("jet_eta",      &(m_ntupData -> d_jets_eta),
  // &b_jets_eta);
  // m_chain -> SetBranchAddress("jet_phi",       &(m_ntupData -> d_jets_phi),
  // &b_jets_phi);
  // m_chain -> SetBranchAddress("jet_e",      &(m_ntupData -> d_jets_e),
  // &b_jets_e);
  // m_chain -> SetBranchAddress("jet_mv2c10",      &(m_ntupData ->
  // d_jets_mv2c10),              &b_jets_mv2c10);
  // m_chain -> SetBranchAddress("jet_mv2c20",      &(m_ntupData ->
  // d_jets_mv2c20),              &b_jets_mv2c20);
  // m_chain -> SetBranchAddress("jet_truthflav",      &(m_ntupData ->
  // d_jets_truthflav),              &b_jets_truthflav);
  // m_chain -> SetBranchAddress("jet_truthmatch",      &(m_ntupData ->
  // d_jets_truthmatch),              &b_jets_truthmatch);
  // //////m_chain -> SetBranchAddress("HhadT_jets",      &(m_ntupData ->
  // d_HhadT_jets),              &b_HhadT_jets); the one used in old Ntuples
  // m_chain -> SetBranchAddress("HT_jets",      &(m_ntupData -> d_HhadT_jets),
  // &b_HhadT_jets);
  // // m_chain -> SetBranchAddress("e_jets",      &(m_ntupData -> d_e_jets),
  // &b_e_jets);
  // // m_chain -> SetBranchAddress("mu_jets",      &(m_ntupData -> d_mu_jets),
  // &b_mu_jets);
  // // m_chain -> SetBranchAddress("boosted_e_jets",      &(m_ntupData ->
  // d_boosted_e_jets),              &b_boosted_e_jets);
  // // m_chain -> SetBranchAddress("boosted_mu_jets",      &(m_ntupData ->
  // d_boosted_mu_jets),              &b_boosted_mu_jets);
  // // m_chain -> SetBranchAddress("ejets",      &(m_ntupData -> d_e_jets),
  // &b_e_jets);
  // // m_chain -> SetBranchAddress("mujets",      &(m_ntupData -> d_mu_jets),
  // &b_mu_jets);
  // m_chain -> SetBranchAddress("ejets_2015",      &(m_ntupData ->
  // d_e_jets_2015),              &b_e_jets_2015);
  // m_chain -> SetBranchAddress("mujets_2015",      &(m_ntupData ->
  // d_mu_jets_2015),              &b_mu_jets_2015);
  // m_chain -> SetBranchAddress("ejets_2016",      &(m_ntupData ->
  // d_e_jets_2016),              &b_e_jets_2016);
  // m_chain -> SetBranchAddress("mujets_2016",      &(m_ntupData ->
  // d_mu_jets_2016),              &b_mu_jets_2016);
  // m_chain -> SetBranchAddress("boosted_ejets",      &(m_ntupData ->
  // d_boosted_e_jets),              &b_boosted_e_jets);
  // m_chain -> SetBranchAddress("boosted_mujets",      &(m_ntupData ->
  // d_boosted_mu_jets),              &b_boosted_mu_jets);
  // //m_chain -> SetBranchAddress("jets_n",        &(m_ntupData -> d_jets_n),
  // &b_jets_n);
  // //m_chain -> SetBranchAddress("jets_pt",       &(m_ntupData -> d_jets_pt),
  // &b_jets_pt);
  // //m_chain -> SetBranchAddress("jets_eta",      &(m_ntupData -> d_jets_eta),
  // &b_jets_eta);
  // //m_chain -> SetBranchAddress("jets_truth",    &(m_ntupData ->
  // d_jets_truth_index),     &b_jets_truth_index);

  ///////////////////////large jets

  // m_chain -> SetBranchAddress("ljet_pt",       &(m_ntupData -> d_ljets_pt),
  // &b_ljets_pt);
  // m_chain -> SetBranchAddress("ljet_eta",      &(m_ntupData -> d_ljets_eta),
  // &b_ljets_eta);
  // m_chain -> SetBranchAddress("ljet_m",      &(m_ntupData -> d_ljets_m),
  // &b_ljets_m);
  // m_chain -> SetBranchAddress("ljet_sd12",       &(m_ntupData ->
  // d_ljets_sd12),              &b_ljets_sd12);
  // m_chain -> SetBranchAddress("ljet_sd23",       &(m_ntupData ->
  // d_ljets_sd23),              &b_ljets_sd23);
  // m_chain -> SetBranchAddress("ljet_tau21_wta",       &(m_ntupData ->
  // d_ljets_tau21),              &b_ljets_tau21);
  // m_chain -> SetBranchAddress("ljet_tau32_wta",       &(m_ntupData ->
  // d_ljets_tau32),              &b_ljets_tau32);
  // m_chain -> SetBranchAddress("ljet_topTag_loose",       &(m_ntupData ->
  // d_ljets_topTag_loose),              &b_ljets_topTag_loose);
  // m_chain -> SetBranchAddress("ljet_topTag",       &(m_ntupData ->
  // d_ljets_topTag_tight),              &b_ljets_topTag_tight);
  // m_chain -> SetBranchAddress("ljet_topTagN_loose",       &(m_ntupData ->
  // d_ljets_topTagN_loose),              &b_ljets_topTagN_loose);
  // m_chain -> SetBranchAddress("ljet_topTagN",       &(m_ntupData ->
  // d_ljets_topTagN_tight),              &b_ljets_topTagN_tight);

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Leaving TTH_NtupleReader::SetJetBranchAddresses()"
              << std::endl;

  return stat;
}

//_________________________________________________________________________________
//
int TTH_NtupleReader::SetLeptonBranchAddresses(const std::string & /*sfj*/) {

  //
  // Here are associated the branches related to Leptons.
  //

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Entering in TTH_NtupleReader::SetLeptonBranchAddresses()"
              << std::endl;
  int stat = 0;

  /////////////////////////////electrons
  stat += SetVariableToChain("el_pt", &(m_TTH_ntupData->d_el_pt));
  stat += SetVariableToChain("el_eta", &(m_TTH_ntupData->d_el_eta));
  stat += SetVariableToChain("el_phi", &(m_TTH_ntupData->d_el_phi));
  stat += SetVariableToChain("el_e", &(m_TTH_ntupData->d_el_e));

  // m_chain -> SetBranchAddress("el_pt",       &(m_ntupData -> d_el_pt),
  // &b_el_pt);
  // m_chain -> SetBranchAddress("el_eta",      &(m_ntupData -> d_el_eta),
  // &b_el_eta);
  // m_chain -> SetBranchAddress("el_phi",       &(m_ntupData -> d_el_phi),
  // &b_el_phi);
  // m_chain -> SetBranchAddress("el_e",      &(m_ntupData -> d_el_e), &b_el_e);
  //////////////////////muons
  stat += SetVariableToChain("mu_pt", &(m_TTH_ntupData->d_mu_pt));
  stat += SetVariableToChain("mu_eta", &(m_TTH_ntupData->d_mu_eta));
  stat += SetVariableToChain("mu_phi", &(m_TTH_ntupData->d_mu_phi));
  stat += SetVariableToChain("mu_e", &(m_TTH_ntupData->d_mu_e));
  // std::cout << "2 m_TTH_ntupData -> d_el_pt->size() = " <<m_TTH_ntupData ->
  // d_el_pt->size()<< std::endl;

  //     m_chain -> SetBranchAddress("mu_pt",       &(m_ntupData -> d_mu_pt),
  //     &b_mu_pt);
  // m_chain -> SetBranchAddress("mu_eta",      &(m_ntupData -> d_mu_eta),
  // &b_mu_eta);
  // m_chain -> SetBranchAddress("mu_phi",       &(m_ntupData -> d_mu_phi),
  // &b_mu_phi);
  // m_chain -> SetBranchAddress("mu_e",      &(m_ntupData -> d_mu_e), &b_mu_e);

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Leaving TTH_NtupleReader::SetLeptonBranchAddresses()"
              << std::endl;

  return stat;
}

//_________________________________________________________________________________
//
int TTH_NtupleReader::SetMETBranchAddresses(const std::string & /*sfj*/) {

  //
  // Here are associated the branches related to MET.
  //
  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Entering in TTH_NtupleReader::SetMETBranchAddresses()"
              << std::endl;
  int stat = 0;
  stat += SetVariableToChain("met_met", &(m_TTH_ntupData->d_met_met));
  stat += SetVariableToChain("met_phi", &(m_TTH_ntupData->d_met_phi));
  // std::cout << "2 m_TTH_ntupData -> d_met_met = " <<m_TTH_ntupData ->
  // d_met_met<< std::endl;

  // m_chain -> SetBranchAddress("met_met",      &(m_ntupData -> d_met_met),
  // &b_met_met);
  // m_chain -> SetBranchAddress("met_phi",       &(m_ntupData -> d_met_phi),
  // &b_met_phi);
  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Leaving in TTH_NtupleReader::SetMETBranchAddresses()"
              << std::endl;
  return stat;
}

//_________________________________________________________________________________
//
int TTH_NtupleReader::SetWeightBranchAddresses(const std::string & /*sfj*/) {

  //
  // Here are associated the branches related to weights.
  //

  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Entering in TTH_NtupleReader::SetWeightBranchAddresses()"
              << std::endl;
  int stat = 0;
  stat += SetVariableToChain("weight_pileup",
                             &(m_TTH_ntupData->d_eventWeight_pileup));
  stat += SetVariableToChain("weight_mc", &(m_TTH_ntupData->d_eventWeight_mc));
  stat += SetVariableToChain("weight_leptonSF",
                             &(m_TTH_ntupData->d_eventWeight_leptonSF));
  stat += SetVariableToChain("weight_bTagSF_70",
                             &(m_TTH_ntupData->d_eventWeight_bTagSF));
  stat += SetVariableToChain("weight_jvt", &(m_TTH_ntupData->d_weight_jvt));
  stat += SetVariableToChain("weight_ttbar_FracRw",
                             &(m_TTH_ntupData->d_weight_ttbar_FracRw));
  stat += SetVariableToChain("weight_ttbb_Nominal",
                             &(m_TTH_ntupData->d_weight_ttbb_Nominal));

  //     m_chain -> SetBranchAddress("weight_pileup",      &(m_ntupData ->
  //     d_eventWeight_pileup),      &b_eventWeight_pileup);
  // m_chain -> SetBranchAddress("weight_mc",      &(m_ntupData ->
  // d_eventWeight_mc),      &b_eventWeight_mc);
  //   m_chain -> SetBranchAddress("weight_leptonSF",    &(m_ntupData ->
  //   d_eventWeight_leptonSF),    &b_eventWeight_leptonSF);
  // 	//m_chain -> SetBranchAddress("weight_bTagSF_77",    &(m_ntupData ->
  // d_eventWeight_bTagSF),    &b_eventWeight_bTagSF);
  // m_chain -> SetBranchAddress("weight_bTagSF_70",    &(m_ntupData ->
  // d_eventWeight_bTagSF),    &b_eventWeight_bTagSF);
  //     m_chain -> SetBranchAddress("weight_jvt",      &(m_ntupData ->
  //     d_weight_jvt),      &b_weight_jvt);
  //     m_chain -> SetBranchAddress("weight_ttbar_FracRw",      &(m_ntupData ->
  //     d_weight_ttbar_FracRw),      &b_weight_ttbar_FracRw);
  // 	  m_chain -> SetBranchAddress("weight_ttbb_Nominal",      &(m_ntupData
  // -> d_weight_ttbb_Nominal),      &b_weight_ttbb_Nominal);

  // m_chain -> SetBranchAddress("weight_mc",      &(m_ntupData ->
  // d_eventWeight_Nom),      &b_evt_w_Nom);
  // m_chain -> SetBranchAddress("weight_bTagSF_77",    &(m_ntupData ->
  // d_eventWeight_Btag1),    &b_evt_w_Btag1);
  // m_chain -> SetBranchAddress("weight_bTagSF_85",    &(m_ntupData ->
  // d_eventWeight_Btag2),    &b_evt_w_Btag1);
  if (m_opt->MsgLevel() == Debug::DEBUG)
    std::cout << "Leaving TTH_NtupleReader::SetWeightBranchAddresses()"
              << std::endl;

  return stat;
}
