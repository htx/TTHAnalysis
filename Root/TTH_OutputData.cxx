#include <iostream>
#include "TTHAnalysis/TTH_OutputData.h"
//#include "TLorentzVector.h"

//______________________________________________________
//
TTH_OutputData::TTH_OutputData()
    : OutputData(), o_e_jets(0), o_mu_jets(0), o_runNumber(0), ////////
      o_eventNumber(0), o_mu(0.), o_sampleName(0), o_ht(0.),
      o_ntruth(0),//////
      o_qFromTop_pt(0.),
      o_Mbb(0.), o_Mqqb(0.), o_Mqq(0.), o_MqqbMinusMqq(0.), o_Mlvb0(0.),//FCNC
      o_Mlvb1(0.), o_Mlvb2(0.), o_Mbbj(0.), o_MbbjMinusMbb(0.),
      o_jetFromTop_truthflav(0),
      o_ExtraJets(0.), o_ExtraJetsbb(0.),
      o_ExtraJetsbj(0.), o_ProbSig(0.), o_log10ProbSig(0.),
      o_log10ProbSig5matched(0.), o_log10ProbSig6matchedAnd5matched(0.),
      o_log10ProbSig10(0.), o_log10ProbSig3VOP(0.), o_log10ProbSigKin(0.),
      o_log10ProbBkg(0.), o_log10ProbBkgTestbb(0.), o_log10ProbBkgPermB(0.),
      o_log10ProbBkgPermBAndLight(0.), o_log10ProbBkgPermBAndLightBTag(0.),
      o_log10ProbBkgKin(0.),
      // o_log10ProbBkg2(0.),
      // o_log10ProbBkg3(0.),
      // o_log10ProbBkg4(0.),
      o_PMbb(0.), o_log10PMbb(0.), o_Discriminant(0.),
      o_Discriminant6matchedAnd5matched(0.), o_DiscriminantKin(0.),
      o_leadJetPt(0.), o_leadJetEta(0.), o_jets_n(0), o_bjets_n(0),
      o_matchedjets_n(0), o_matchedjetsttbar_n(0), o_truthbextrajets_n(0),
      o_truthnotbextrajets_n(0), o_HhadT_jets(0.), o_ExtraJetsPerm(0),
      o_ExtraJetsPerm_ll(0),
      // o_ExtraJetsPerm_lc(0),
      o_ExtraJetsPerm_lc(0), o_ExtraJetsPerm_lb(0), o_ExtraJetsPerm_cc(0),
      o_ExtraJetsPerm_bc(0), o_ExtraJetsPerm_bb(0), o_ExtraJetsPerm_notbb(0),
      ///////////////////////
      o_Mqq_NoLeadJetW_b(0), o_Mqq_NoLeadJetW_c(0), o_Mqq_NoLeadJetW_l(0),
      o_Mqqb_NoLeadJetW_b(0), o_Mqqb_NoLeadJetW_c(0), o_Mqqb_NoLeadJetW_l(0),
      o_MqqbMinusMqq_NoLeadJetW_b(0), o_MqqbMinusMqq_NoLeadJetW_c(0),
      o_MqqbMinusMqq_NoLeadJetW_l(0), o_Mqq_NoSubLeadJetW_b(0),
      o_Mqq_NoSubLeadJetW_c(0), o_Mqq_NoSubLeadJetW_l(0),
      o_Mqqb_NoSubLeadJetW_b(0), o_Mqqb_NoSubLeadJetW_c(0),
      o_Mqqb_NoSubLeadJetW_l(0), o_MqqbMinusMqq_NoSubLeadJetW_b(0),
      o_MqqbMinusMqq_NoSubLeadJetW_c(0), o_MqqbMinusMqq_NoSubLeadJetW_l(0),
      o_ExtraJetNoLeadJetW_truthflav(0), o_ExtraJetNoSubLeadJetW_truthflav(0),
      ////////////////////////////////////////////////////////////////////
      o_Mqq_NoLeadJetW_Jet_1_b(0.), o_Mqq_NoLeadJetW_Jet_1_c(0.),
      o_Mqq_NoLeadJetW_Jet_1_l(0.), o_Mqqb_NoLeadJetW_Jet_1_b(0.),
      o_Mqqb_NoLeadJetW_Jet_1_c(0.), o_Mqqb_NoLeadJetW_Jet_1_l(0.),
      o_MqqbMinusMqq_NoLeadJetW_Jet_1_b(0.),
      o_MqqbMinusMqq_NoLeadJetW_Jet_1_c(0.),
      o_MqqbMinusMqq_NoLeadJetW_Jet_1_l(0.), o_Mqq_NoSubLeadJetW_Jet_1_b(0.),
      o_Mqq_NoSubLeadJetW_Jet_1_c(0.), o_Mqq_NoSubLeadJetW_Jet_1_l(0.),
      o_Mqqb_NoSubLeadJetW_Jet_1_b(0.), o_Mqqb_NoSubLeadJetW_Jet_1_c(0.),
      o_Mqqb_NoSubLeadJetW_Jet_1_l(0.),
      o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b(0.),
      o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c(0.),
      o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l(0.),
      o_ExtraJetNoLeadJetW_Jet_1_truthflav(0),
      o_ExtraJetNoSubLeadJetW_Jet_1_truthflav(0), o_jets_pt(0), o_jets_eta(0),
      o_jets_mv2c10(0), o_jets_mv2c20(0), o_leadJetFromW_truthflav(0),
      o_subJetFromW_truthflav(0), o_extraJet1_truthflav(0),
      o_extraJet2_truthflav(0), o_extrajetleadnotb_truthflav(0),
      /////////////////////////////
      o_Mqq_Jet_1(0.), o_Mqqb_Jet_1(0.), o_MqqbMinusMqq_Jet_1(0.),
      /////////////////////////////////
      o_jetFromW5matched_truthflav(0), o_extrajet5matched_truthflav(0),
      /////////////////////////////////////
      o_extraJet1Perm_truthflav(0), o_extraJet2Perm_truthflav(0),
      o_jets_truthflav(0), o_jets_truthmatch(0), o_nottruthmatch(0),
      o_ljets_n(0), //////////ljets
      o_leadLJetPt(0.), o_subLJetPt(0.), o_leadLJetEta(0.), o_subLJetEta(0.),
      o_leadLJetM(0.), o_subLJetM(0.), o_leadLJet_SPLIT12(0.),
      o_subLJet_SPLIT12(0.), o_leadLJet_SPLIT23(0.), o_subLJet_SPLIT23(0.),
      o_leadLJet_Tau21(0.), o_subLJet_Tau21(0.), o_leadLJet_Tau32(0.),
      o_subLJet_Tau32(0.), o_nTopTag_loose(0), o_nTopTag_tight(0),
      o_ljets_pt(0), o_ljets_eta(0), o_ljets_m(0), o_allLJet_SPLIT12(0),
      o_allLJet_SPLIT23(0), o_allLJet_Tau21(0), o_allLJet_Tau32(0),
      o_el_pt(0.),                                           //////////electrons
      o_el_eta(0.), o_el_phi(0.), o_el_e(0.), o_mu_pt(0.),   //////////muons
      o_mu_eta(0.), o_mu_phi(0.), o_mu_e(0.), o_met_met(0.), /////////met
      o_met_phi(0.), o_mtw(0.), o_ind_leadTopTag_loose(0),
      o_ind_leadTopTag_tight(0), o_eventWeight_pileup(0.), ////////////weights
      o_eventWeight_mc(0.), o_eventWeight_leptonSF(0.),
      o_eventWeight_bTagSF(0.), o_eventWeight(0.)
// o_eventWeight_Btag1(0),
// o_eventWeight_Btag2(0)
{
  // o_jets_pt = new std::vector < double >;
  o_jets_pt = new std::vector<float>;
  o_jets_eta = new std::vector<float>;
  // o_leadJetFromW_truthflav = new std::vector < int >;
  // o_subJetFromW_truthflav = new std::vector < int >;
  o_jets_truthflav = new std::vector<int>;
  o_jets_truthmatch = new std::vector<int>;
  o_jets_mv2c10 = new std::vector<float>;
  o_jets_mv2c20 = new std::vector<float>;
  o_ExtraJetsPerm = new std::vector<float>;
  o_ExtraJetsPerm_ll = new std::vector<float>;
  o_ExtraJetsPerm_lc = new std::vector<float>;
  o_ExtraJetsPerm_lb = new std::vector<float>;
  // o_ExtraJetsPerm_cl = new std::vector < float >;
  o_ExtraJetsPerm_cc = new std::vector<float>;
  // o_ExtraJetsPerm_cb = new std::vector < float >;
  // o_ExtraJetsPerm_bl = new std::vector < float >;
  o_ExtraJetsPerm_bc = new std::vector<float>;
  o_ExtraJetsPerm_bb = new std::vector<float>;
  o_ExtraJetsPerm_notbb = new std::vector<float>;
  o_Mqq_NoLeadJetW_b = new std::vector<float>;
  o_Mqq_NoLeadJetW_c = new std::vector<float>;
  o_Mqq_NoLeadJetW_l = new std::vector<float>;
  o_Mqqb_NoLeadJetW_b = new std::vector<float>;
  o_Mqqb_NoLeadJetW_c = new std::vector<float>;
  o_Mqqb_NoLeadJetW_l = new std::vector<float>;
  o_MqqbMinusMqq_NoLeadJetW_b = new std::vector<float>;
  o_MqqbMinusMqq_NoLeadJetW_c = new std::vector<float>;
  o_MqqbMinusMqq_NoLeadJetW_l = new std::vector<float>;
  o_Mqq_NoSubLeadJetW_b = new std::vector<float>;
  o_Mqq_NoSubLeadJetW_c = new std::vector<float>;
  o_Mqq_NoSubLeadJetW_l = new std::vector<float>;
  o_Mqqb_NoSubLeadJetW_b = new std::vector<float>;
  o_Mqqb_NoSubLeadJetW_c = new std::vector<float>;
  o_Mqqb_NoSubLeadJetW_l = new std::vector<float>;
  o_MqqbMinusMqq_NoSubLeadJetW_b = new std::vector<float>;
  o_MqqbMinusMqq_NoSubLeadJetW_c = new std::vector<float>;
  o_MqqbMinusMqq_NoSubLeadJetW_l = new std::vector<float>;
  o_ExtraJetNoLeadJetW_truthflav = new std::vector<int>;
  o_ExtraJetNoSubLeadJetW_truthflav = new std::vector<int>;
  o_extraJet1Perm_truthflav = new std::vector<int>;
  o_extraJet2Perm_truthflav = new std::vector<int>;
  o_ljets_pt = new std::vector<float>;
  o_ljets_eta = new std::vector<float>;
  o_ljets_m = new std::vector<float>;
  o_allLJet_SPLIT12 = new std::vector<float>;
  o_allLJet_SPLIT23 = new std::vector<float>;
  o_allLJet_Tau21 = new std::vector<float>;
  o_allLJet_Tau32 = new std::vector<float>;
}

//______________________________________________________
//
TTH_OutputData::TTH_OutputData(const TTH_OutputData &q) : OutputData(q) {
  o_e_jets = q.o_e_jets;
  o_mu_jets = q.o_mu_jets;
  o_eventNumber = q.o_eventNumber;
  o_runNumber = q.o_runNumber;
  o_mu = q.o_mu;
  o_sampleName = q.o_sampleName;
  o_ht = q.o_ht;
  o_ntruth = q.o_ntruth;
  o_qFromTop_pt = q.o_qFromTop_pt;
  o_Mbb = q.o_Mbb;
  o_Mqqb = q.o_Mqqb;
  o_Mqq = q.o_Mqq;
  o_MqqbMinusMqq = q.o_MqqbMinusMqq;
  o_Mlvb0 = q.o_Mlvb0;
  o_Mlvb1 = q.o_Mlvb1;
  o_Mlvb2 = q.o_Mlvb2;
  o_Mbbj = q.o_Mbbj;
  o_MbbjMinusMbb = q.o_MbbjMinusMbb;
  o_jetFromTop_truthflav = q.o_jetFromTop_truthflav,//
  o_ExtraJets = q.o_ExtraJets;
  o_ExtraJetsbb = q.o_ExtraJetsbb;
  o_ExtraJetsbj = q.o_ExtraJetsbj;
  o_ExtraJetsPerm = q.o_ExtraJetsPerm;
  o_ExtraJetsPerm_ll = q.o_ExtraJetsPerm_ll;
  o_ExtraJetsPerm_lc = q.o_ExtraJetsPerm_lc;
  o_ExtraJetsPerm_lb = q.o_ExtraJetsPerm_lb;
  // o_ExtraJetsPerm_cl =  o_ExtraJetsPerm_cl;
  o_ExtraJetsPerm_cc = q.o_ExtraJetsPerm_cc;
  // o_ExtraJetsPerm_cb =   o_ExtraJetsPerm_cb;
  // o_ExtraJetsPerm_bl = o_ExtraJetsPerm_bl;
  o_ExtraJetsPerm_bc = q.o_ExtraJetsPerm_bc;
  o_ExtraJetsPerm_bb = q.o_ExtraJetsPerm_bb;
  o_ExtraJetsPerm_notbb = q.o_ExtraJetsPerm_notbb;
  o_Mqq_NoLeadJetW_b = q.o_Mqq_NoLeadJetW_b;
  o_Mqq_NoLeadJetW_c = q.o_Mqq_NoLeadJetW_c;
  o_Mqq_NoLeadJetW_l = q.o_Mqq_NoLeadJetW_l;
  o_Mqqb_NoLeadJetW_b = q.o_Mqqb_NoLeadJetW_b;
  o_Mqqb_NoLeadJetW_c = q.o_Mqqb_NoLeadJetW_c;
  o_Mqqb_NoLeadJetW_l = q.o_Mqqb_NoLeadJetW_l;
  o_MqqbMinusMqq_NoLeadJetW_b = q.o_MqqbMinusMqq_NoLeadJetW_b;
  o_MqqbMinusMqq_NoLeadJetW_c = q.o_MqqbMinusMqq_NoLeadJetW_c;
  o_MqqbMinusMqq_NoLeadJetW_l = q.o_MqqbMinusMqq_NoLeadJetW_l;
  o_Mqq_NoSubLeadJetW_b = q.o_Mqq_NoSubLeadJetW_b;
  o_Mqq_NoSubLeadJetW_c = q.o_Mqq_NoSubLeadJetW_c;
  o_Mqq_NoSubLeadJetW_l = q.o_Mqq_NoSubLeadJetW_l;
  o_Mqqb_NoSubLeadJetW_b = q.o_Mqqb_NoSubLeadJetW_b;
  o_Mqqb_NoSubLeadJetW_c = q.o_Mqqb_NoSubLeadJetW_c;
  o_Mqqb_NoSubLeadJetW_l = q.o_Mqqb_NoSubLeadJetW_l;
  o_MqqbMinusMqq_NoSubLeadJetW_b = q.o_MqqbMinusMqq_NoSubLeadJetW_b;
  o_MqqbMinusMqq_NoSubLeadJetW_c = q.o_MqqbMinusMqq_NoSubLeadJetW_c;
  o_MqqbMinusMqq_NoSubLeadJetW_l = q.o_MqqbMinusMqq_NoSubLeadJetW_l;
  o_ExtraJetNoLeadJetW_truthflav = q.o_ExtraJetNoLeadJetW_truthflav;
  o_ExtraJetNoSubLeadJetW_truthflav = q.o_ExtraJetNoSubLeadJetW_truthflav;
  ////////////////////////////////////////////////////////////////////////
  o_Mqq_NoLeadJetW_Jet_1_b = q.o_Mqq_NoLeadJetW_Jet_1_b;
  o_Mqq_NoLeadJetW_Jet_1_c = q.o_Mqq_NoLeadJetW_Jet_1_c;
  o_Mqq_NoLeadJetW_Jet_1_l = q.o_Mqq_NoLeadJetW_Jet_1_l;
  o_Mqqb_NoLeadJetW_Jet_1_b = q.o_Mqqb_NoLeadJetW_Jet_1_b;
  o_Mqqb_NoLeadJetW_Jet_1_c = q.o_Mqqb_NoLeadJetW_Jet_1_c;
  o_Mqqb_NoLeadJetW_Jet_1_l = q.o_Mqqb_NoLeadJetW_Jet_1_l;
  o_MqqbMinusMqq_NoLeadJetW_Jet_1_b = q.o_MqqbMinusMqq_NoLeadJetW_Jet_1_b;
  o_MqqbMinusMqq_NoLeadJetW_Jet_1_c = q.o_MqqbMinusMqq_NoLeadJetW_Jet_1_c;
  o_MqqbMinusMqq_NoLeadJetW_Jet_1_l = q.o_MqqbMinusMqq_NoLeadJetW_Jet_1_l;
  o_Mqq_NoSubLeadJetW_Jet_1_b = q.o_Mqq_NoSubLeadJetW_Jet_1_b;
  o_Mqq_NoSubLeadJetW_Jet_1_c = q.o_Mqq_NoSubLeadJetW_Jet_1_c;
  o_Mqq_NoSubLeadJetW_Jet_1_l = q.o_Mqq_NoSubLeadJetW_Jet_1_l;
  o_Mqqb_NoSubLeadJetW_Jet_1_b = q.o_Mqqb_NoSubLeadJetW_Jet_1_b;
  o_Mqqb_NoSubLeadJetW_Jet_1_c = q.o_Mqqb_NoSubLeadJetW_Jet_1_c;
  o_Mqqb_NoSubLeadJetW_Jet_1_l = q.o_Mqqb_NoSubLeadJetW_Jet_1_l;
  o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b = q.o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b;
  o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c = q.o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c;
  o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l = q.o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l;
  o_ExtraJetNoLeadJetW_Jet_1_truthflav = q.o_ExtraJetNoLeadJetW_Jet_1_truthflav;
  o_ExtraJetNoSubLeadJetW_Jet_1_truthflav =
      q.o_ExtraJetNoSubLeadJetW_Jet_1_truthflav;
  ////////////////////////////////////////////////////////////////////
  o_ProbSig = q.o_ProbSig;
  o_log10ProbSig = q.o_log10ProbSig;
  o_log10ProbSig5matched = q.o_log10ProbSig5matched;
  o_log10ProbSig6matchedAnd5matched = q.o_log10ProbSig6matchedAnd5matched;
  o_log10ProbSig10 = q.o_log10ProbSig10;
  o_log10ProbSig3VOP = q.o_log10ProbSig3VOP;
  o_log10ProbSigKin = q.o_log10ProbSigKin;
  // o_log10ProbSig2 = q.o_log10ProbSig2;
  o_log10ProbBkg = q.o_log10ProbBkg;
  o_log10ProbBkgTestbb = q.o_log10ProbBkgTestbb;
  o_log10ProbBkgPermB = q.o_log10ProbBkgPermB;
  o_log10ProbBkgPermBAndLight = q.o_log10ProbBkgPermBAndLight;
  o_log10ProbBkgPermBAndLightBTag = q.o_log10ProbBkgPermBAndLightBTag;
  o_log10ProbBkgKin = q.o_log10ProbBkgKin;
  // o_log10ProbBkg2 = q.o_log10ProbBkg2;
  // o_log10ProbBkg3 = q.o_log10ProbBkg3;
  // o_log10ProbBkg4 = q.o_log10ProbBkg4;
  o_PMbb = q.o_PMbb;
  o_log10PMbb = q.o_log10PMbb;
  o_Discriminant = q.o_Discriminant;
  o_Discriminant6matchedAnd5matched = q.o_Discriminant6matchedAnd5matched;
  o_DiscriminantKin = q.o_DiscriminantKin;
  o_leadJetPt = q.o_leadJetPt;
  o_jets_n = q.o_jets_n;
  o_bjets_n = q.o_bjets_n;
  o_matchedjets_n = q.o_matchedjets_n;
  o_matchedjetsttbar_n = q.o_matchedjetsttbar_n;
  o_truthbextrajets_n = q.o_truthbextrajets_n;
  o_truthnotbextrajets_n = q.o_truthnotbextrajets_n;
  o_leadJetFromW_truthflav = q.o_leadJetFromW_truthflav;
  o_subJetFromW_truthflav = q.o_subJetFromW_truthflav;
  o_extraJet1_truthflav = q.o_extraJet1_truthflav;
  o_extraJet2_truthflav = q.o_extraJet2_truthflav;
  o_extrajetleadnotb_truthflav = q.o_extrajetleadnotb_truthflav;
  ////////////////////////////////
  o_Mqq_Jet_1 = q.o_Mqq_Jet_1;
  o_Mqqb_Jet_1 = q.o_Mqqb_Jet_1;
  o_MqqbMinusMqq_Jet_1 = q.o_MqqbMinusMqq_Jet_1;
  ////////////////////////////////
  o_jetFromW5matched_truthflav = q.o_jetFromW5matched_truthflav;
  o_extrajet5matched_truthflav = q.o_extrajet5matched_truthflav;
  ////////////////////////////////
  o_extraJet1Perm_truthflav = q.o_extraJet1Perm_truthflav;
  o_extraJet2Perm_truthflav = q.o_extraJet2Perm_truthflav;
  o_jets_truthflav = q.o_jets_truthflav;
  o_jets_truthmatch = q.o_jets_truthmatch;
  o_nottruthmatch = q.o_nottruthmatch;
  o_jets_pt = q.o_jets_pt;
  o_jets_eta = q.o_jets_eta;
  o_leadJetEta = q.o_leadJetEta;
  o_jets_mv2c10 = q.o_jets_mv2c10;
  o_jets_mv2c20 = q.o_jets_mv2c20;
  o_HhadT_jets = q.o_HhadT_jets;
  o_ljets_n = q.o_ljets_n;
  o_leadLJetPt = q.o_leadLJetPt;
  o_subLJetPt = q.o_subLJetPt;
  o_ljets_pt = q.o_ljets_pt;
  o_ljets_eta = q.o_ljets_eta;
  o_leadLJetEta = q.o_leadLJetEta;
  o_subLJetEta = q.o_subLJetEta;
  o_ljets_m = q.o_ljets_m;
  o_leadLJetM = q.o_leadLJetM;
  o_subLJetM = q.o_subLJetM;
  o_leadLJet_SPLIT12 = q.o_leadLJet_SPLIT12;
  o_subLJet_SPLIT12 = q.o_subLJet_SPLIT12;
  o_allLJet_SPLIT12 = q.o_allLJet_SPLIT12;
  o_leadLJet_SPLIT23 = q.o_leadLJet_SPLIT23;
  o_subLJet_SPLIT23 = q.o_subLJet_SPLIT23;
  o_allLJet_SPLIT23 = q.o_allLJet_SPLIT23;
  o_leadLJet_Tau21 = q.o_leadLJet_Tau21;
  o_subLJet_Tau21 = q.o_subLJet_Tau21;
  o_allLJet_Tau21 = q.o_allLJet_Tau21;
  o_leadLJet_Tau32 = q.o_leadLJet_Tau32;
  o_subLJet_Tau32 = q.o_subLJet_Tau32;
  o_allLJet_Tau32 = q.o_allLJet_Tau32;
  o_nTopTag_loose = q.o_nTopTag_loose;
  o_nTopTag_tight = q.o_nTopTag_tight;
  o_el_pt = q.o_el_pt; //////////electrons
  o_el_eta = q.o_el_eta;
  o_el_phi = q.o_el_phi;
  o_el_e = q.o_el_e;
  o_mu_pt = q.o_mu_pt; //////////muons
  o_mu_eta = q.o_mu_eta;
  o_mu_phi = q.o_mu_phi;
  o_mu_e = q.o_mu_e;
  o_tlv_lep = q.o_tlv_lep;
  o_met_met = q.o_met_met; /////////met
  o_met_phi = q.o_met_phi;
  o_tlv_met = q.o_tlv_met;
  o_mtw = q.o_mtw;
  o_ind_leadTopTag_loose = q.o_ind_leadTopTag_loose;
  o_ind_leadTopTag_tight = q.o_ind_leadTopTag_tight;
  o_eventWeight_pileup = q.o_eventWeight_pileup; ////////////weights
  o_eventWeight_mc = q.o_eventWeight_mc;
  o_eventWeight_leptonSF = q.o_eventWeight_leptonSF;
  o_eventWeight_bTagSF = q.o_eventWeight_bTagSF;
  o_eventWeight = q.o_eventWeight;
  // o_eventWeight_Btag1 = q.o_eventWeight_Btag1;
  // o_eventWeight_Btag2 = q.o_eventWeight_Btag2;
}

//______________________________________________________
//
TTH_OutputData::~TTH_OutputData() {}

//______________________________________________________
//
void TTH_OutputData::ClearOutputData() {

  OutputData::ClearOutputData();
  o_e_jets = 0;
  o_mu_jets = 0;
  o_runNumber = 0; //
  o_eventNumber = 0;
  o_mu = 0;
  o_sampleName = -1;
  o_ht = 0; //
  o_ntruth = 0;
  o_qFromTop_pt = 0;
  o_Mbb = 0;
  o_Mqqb = 0;
  o_Mqq = 0;
  o_MqqbMinusMqq = 0;
  o_Mlvb0 = 0;
  o_Mlvb1 = 0;
  o_Mlvb2 = 0;
  o_Mbbj = 0;
  o_MbbjMinusMbb = 0;
  o_jetFromTop_truthflav = -99;
  o_ExtraJets = 0;
  o_ExtraJetsbb = 0;
  o_ExtraJetsbj = 0;
  o_ExtraJetsPerm->clear();
  o_ExtraJetsPerm_ll->clear();
  o_ExtraJetsPerm_lc->clear();
  o_ExtraJetsPerm_lb->clear();
  // o_ExtraJetsPerm_cl =  o_ExtraJetsPerm_cl;
  o_ExtraJetsPerm_cc->clear();
  // o_ExtraJetsPerm_cb =   o_ExtraJetsPerm_cb;
  // o_ExtraJetsPerm_bl = o_ExtraJetsPerm_bl;
  o_ExtraJetsPerm_bc->clear();
  o_ExtraJetsPerm_bb->clear();
  o_ExtraJetsPerm_notbb->clear();
  //////////////////////////////////////////////////////////////
  o_Mqq_NoLeadJetW_b->clear();
  o_Mqq_NoLeadJetW_c->clear();
  o_Mqq_NoLeadJetW_l->clear();
  o_Mqqb_NoLeadJetW_b->clear();
  o_Mqqb_NoLeadJetW_c->clear();
  o_Mqqb_NoLeadJetW_l->clear();
  o_MqqbMinusMqq_NoLeadJetW_b->clear();
  o_MqqbMinusMqq_NoLeadJetW_c->clear();
  o_MqqbMinusMqq_NoLeadJetW_l->clear();
  o_Mqq_NoSubLeadJetW_b->clear();
  o_Mqq_NoSubLeadJetW_c->clear();
  o_Mqq_NoSubLeadJetW_l->clear();
  o_Mqqb_NoSubLeadJetW_b->clear();
  o_Mqqb_NoSubLeadJetW_c->clear();
  o_Mqqb_NoSubLeadJetW_l->clear();
  o_MqqbMinusMqq_NoSubLeadJetW_b->clear();
  o_MqqbMinusMqq_NoSubLeadJetW_c->clear();
  o_MqqbMinusMqq_NoSubLeadJetW_l->clear();
  o_ExtraJetNoLeadJetW_truthflav->clear();
  o_ExtraJetNoSubLeadJetW_truthflav->clear();
  ////////////////////////////////////////////////////////////////
  o_Mqq_NoLeadJetW_Jet_1_b = 0;
  o_Mqq_NoLeadJetW_Jet_1_c = 0;
  o_Mqq_NoLeadJetW_Jet_1_l = 0;
  o_Mqqb_NoLeadJetW_Jet_1_b = 0;
  o_Mqqb_NoLeadJetW_Jet_1_c = 0;
  o_Mqqb_NoLeadJetW_Jet_1_l = 0;
  o_MqqbMinusMqq_NoLeadJetW_Jet_1_b = 0;
  o_MqqbMinusMqq_NoLeadJetW_Jet_1_c = 0;
  o_MqqbMinusMqq_NoLeadJetW_Jet_1_l = 0;
  o_Mqq_NoSubLeadJetW_Jet_1_b = 0;
  o_Mqq_NoSubLeadJetW_Jet_1_c = 0;
  o_Mqq_NoSubLeadJetW_Jet_1_l = 0;
  o_Mqqb_NoSubLeadJetW_Jet_1_b = 0;
  o_Mqqb_NoSubLeadJetW_Jet_1_c = 0;
  o_Mqqb_NoSubLeadJetW_Jet_1_l = 0;
  o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b = 0;
  o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c = 0;
  o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l = 0;
  o_ExtraJetNoLeadJetW_Jet_1_truthflav = 0;
  o_ExtraJetNoSubLeadJetW_Jet_1_truthflav = 0;
  ///////////////////////////////////////////////////
  // o_ProbSigKin=0;
  o_ProbSig = 0;
  o_log10ProbSig = 0;
  o_log10ProbSig5matched = 0;
  o_log10ProbSig6matchedAnd5matched = 0;
  o_log10ProbSig10 = 0;
  o_log10ProbSig3VOP = 0;
  o_log10ProbSigKin = 0;

  // o_log10ProbSig2 = 0;
  o_log10ProbBkg = 0;
  o_log10ProbBkgTestbb = 0;
  o_log10ProbBkgPermB = 0;
  o_log10ProbBkgPermBAndLight = 0;
  o_log10ProbBkgPermBAndLightBTag = 0;
  o_log10ProbBkgKin = 0;
  // o_log10ProbBkg2 = 0;
  // o_log10ProbBkg3 = 0;
  // o_log10ProbBkg4 = 0;
  o_PMbb = 0;
  o_log10PMbb = 0;
  o_Discriminant = 0;
  o_Discriminant6matchedAnd5matched = 0;
  o_DiscriminantKin = 0;
  o_leadJetPt = 0;
  o_leadJetEta = 0;
  o_jets_n = 0;
  o_bjets_n = 0;
  o_matchedjets_n = 0;
  o_matchedjetsttbar_n = 0;
  o_truthbextrajets_n = 0;
  o_truthnotbextrajets_n = 0;
  // o_leadJetFromW_truthflav -> clear();
  // o_subJetFromW_truthflav -> clear();
  o_leadJetFromW_truthflav = -100;
  o_subJetFromW_truthflav = -100;
  o_extraJet1_truthflav = -100;
  o_extraJet2_truthflav = -100;
  o_extrajetleadnotb_truthflav = -100;
  o_Mqq_Jet_1 = 0;
  o_Mqqb_Jet_1 = 0;
  o_MqqbMinusMqq_Jet_1 = 0;
  ////////////////////////////////
  o_jetFromW5matched_truthflav = -100;
  o_extrajet5matched_truthflav = -100;
  o_extraJet1Perm_truthflav->clear();
  o_extraJet2Perm_truthflav->clear();
  o_jets_truthflav->clear();
  o_jets_truthmatch->clear();
  o_nottruthmatch = -9;
  o_jets_pt->clear();
  o_jets_eta->clear();
  o_jets_mv2c10->clear();
  o_jets_mv2c20->clear();
  o_HhadT_jets = 0;

  o_ljets_n = 0; //////////
  o_leadLJetPt = -1000;
  o_subLJetPt = -1000;
  o_leadLJetEta = -1000;
  o_subLJetEta = -1000;
  o_leadLJetM = -1000;
  o_subLJetM = -1000;
  o_leadLJet_SPLIT12 = -1000;
  o_subLJet_SPLIT12 = -1000;
  o_allLJet_SPLIT12->clear();
  o_leadLJet_SPLIT23 = -1000;
  o_subLJet_SPLIT23 = -1000;
  o_allLJet_SPLIT23->clear();
  o_leadLJet_Tau21 = -1000;
  o_subLJet_Tau21 = -1000;
  o_allLJet_Tau21->clear();
  o_leadLJet_Tau32 = -1000;
  o_subLJet_Tau32 = -1000;
  o_allLJet_Tau32->clear();
  o_nTopTag_loose = 0;
  o_nTopTag_tight = 0;
  o_ljets_pt->clear();
  o_ljets_eta->clear();
  o_ljets_m->clear();
  o_el_pt = -1000; //////////electrons
  o_el_eta = -1000;
  o_el_phi = -1000;
  o_el_e = -1000;
  o_mu_pt = -1000; //////////muons
  o_mu_eta = -1000;
  o_mu_phi = -1000;
  o_mu_e = -1000;
  o_met_met = 0; /////////met
  o_met_phi = 0;
  o_mtw = 0;
  o_ind_leadTopTag_loose = -1;
  o_ind_leadTopTag_tight = -1;
  o_eventWeight_pileup = 0; ////////////weights
  o_eventWeight_mc = 0;
  o_eventWeight_leptonSF = 0;
  o_eventWeight_bTagSF = 0;
  o_eventWeight = 0;
  o_tlv_lep.SetPtEtaPhiE(0., 0., 0., 0.);
  o_tlv_met.SetPtEtaPhiE(0., 0., 0., 0.);
}

//______________________________________________________
//
void TTH_OutputData::EmptyOutputData() {
  ClearOutputData();
  delete o_jets_pt;
  delete o_jets_eta;
  delete o_jets_mv2c10;
  delete o_jets_mv2c20;
  delete o_ExtraJetsPerm;
  delete o_ExtraJetsPerm_ll;
  delete o_ExtraJetsPerm_lc;
  delete o_ExtraJetsPerm_lb;
  // o_ExtraJetsPerm_cl =  o_ExtraJetsPerm_cl;
  delete o_ExtraJetsPerm_cc;
  // o_ExtraJetsPerm_cb =   o_ExtraJetsPerm_cb;
  // o_ExtraJetsPerm_bl = o_ExtraJetsPerm_bl;
  delete o_ExtraJetsPerm_bc;
  delete o_ExtraJetsPerm_bb;
  delete o_ExtraJetsPerm_notbb;
  delete o_Mqq_NoLeadJetW_b;
  delete o_Mqq_NoLeadJetW_c;
  delete o_Mqq_NoLeadJetW_l;
  delete o_Mqqb_NoLeadJetW_b;
  delete o_Mqqb_NoLeadJetW_c;
  delete o_Mqqb_NoLeadJetW_l;
  delete o_MqqbMinusMqq_NoLeadJetW_b;
  delete o_MqqbMinusMqq_NoLeadJetW_c;
  delete o_MqqbMinusMqq_NoLeadJetW_l;
  delete o_Mqq_NoSubLeadJetW_b;
  delete o_Mqq_NoSubLeadJetW_c;
  delete o_Mqq_NoSubLeadJetW_l;
  delete o_Mqqb_NoSubLeadJetW_b;
  delete o_Mqqb_NoSubLeadJetW_c;
  delete o_Mqqb_NoSubLeadJetW_l;
  delete o_MqqbMinusMqq_NoSubLeadJetW_b;
  delete o_MqqbMinusMqq_NoSubLeadJetW_c;
  delete o_MqqbMinusMqq_NoSubLeadJetW_l;
  delete o_ExtraJetNoLeadJetW_truthflav;
  delete o_ExtraJetNoSubLeadJetW_truthflav;
  delete o_extraJet1Perm_truthflav;
  delete o_extraJet2Perm_truthflav;
  // delete o_leadJetFromW_truthflav;
  // delete o_subJetFromW_truthflav;
  delete o_jets_truthflav;
  delete o_jets_truthmatch;
  delete o_ljets_pt; //////
  delete o_ljets_eta;
  delete o_ljets_m;
  delete o_allLJet_SPLIT12; //////
  delete o_allLJet_SPLIT23;
  delete o_allLJet_Tau21;
  delete o_allLJet_Tau32;
}
