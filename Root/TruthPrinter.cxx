#include "TTHAnalysis/TruthPrinter.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>

namespace tth
{
using std::vector;
using std::cout;
using std::endl;
using std::setw;
using std::string;

template<typename T>
std::string vecToString(const  std::vector< T > &vec)
{
    std::stringstream ss;
    std::copy(vec.begin(), vec.end(), std::ostream_iterator< T >(ss,", "));
    return ss.str();
}
//----------------------------------------------------------
/**
   @brief just convert pdg id to a string.

   Use the same convention used by MG5
   (see madgraph/madweight/Cards.py)
*/
std::string pdg2string(const int &pdg)
{
    string l = "unkn";
    int apdg = abs(pdg);
    switch(apdg){
    case  1: l =  "d"; break;
    case  2: l =  "u"; break;
    case  3: l =  "s"; break;
    case  4: l =  "c"; break;
    case  5: l =  "b"; break;
    case  6: l =  "t"; break;
    case 11: l =  "e"; break;
    case 12: l = "ve"; break;
    case 13: l = "mu"; break;
    case 14: l = "vm"; break;
    case 15: l = "ta"; break;
    case 16: l = "vt"; break;
    case 21: l =  "g"; break;
    case 22: l =  "A"; break;
    case 23: l =  "Z"; break;
    case 24: l =  "W"; break;
    case 25: l =  "h"; break;
    default:
        l = "unkn";
    }
    l = pdg > 0 ? l : (l+"~"); // correct most of the times...good enough
    return l;
}

//----------------------------------------------------------
void TruthPrinter::printTable(const std::vector < float > &mc_pt,
                              const std::vector < float > &mc_eta,
                              const std::vector < float > &mc_phi,
                              const std::vector < float > &mc_m,
                              const std::vector < float > &mc_pdgId,
                              const std::vector < std::vector < int > > &mc_children_index) const
{
    using std::left;
    using std::right;
    int maxNpartToPrint=70;
    int colW=12;
    cout<<"--------------------------------"<<endl
        <<left <<setw(colW)<<"i"
        <<right<<setw(colW)<<"id"
        <<right<<setw(3*colW)<<"(pt, eta, phi)"
        <<right<<setw(colW)<<"chil"
        <<endl
        <<"--------------------------------"<<endl;
    vector<int> ipdgs(mc_pdgId.begin(), mc_pdgId.end());
    int nPartToPrint = static_cast<int>(mc_pt.size());
    nPartToPrint = nPartToPrint < maxNpartToPrint ? nPartToPrint : maxNpartToPrint;
    for(int iP=0;iP<nPartToPrint; ++iP){
        int pdg = ipdgs.at(iP);
        const string ptetaphi = "("+ vecToString<double>({mc_pt[iP], mc_eta[iP], mc_phi[iP]})+")";
        cout<< left  << setw(colW)<<iP
            << right << setw(colW)<<pdg2string(pdg)
            << right << setw(3*colW)<<ptetaphi
            << right << setw(colW)<<vecToString<int>(mc_children_index.at(iP));
        cout<< endl;
  } // end for(iP)
}
//----------------------------------------------------------
int TruthPrinter::indexHadronicW(const std::vector < float > &mc_pdgId,
                                 const std::vector < std::vector < int > > &mc_children_index) const
{
    int index = -1;
    vector<int> hadronicWindices;
    vector<int> ipdgs(mc_pdgId.begin(), mc_pdgId.end());
    struct IsLepton {
        bool operator()(const int pdg) {
            const vector<int> leptons = { 11 /* e*/, 12 /*ve*/, 13 /*mu*/, 14 /*vm*/, 15 /*ta*/, 16 /*vt*/ };
            return find(leptons.begin(), leptons.end(), pdg)!=leptons.end();
        };
    };
    struct DecaysHadronically {
        bool operator()(const std::vector < int > &pdgs, const std::vector < int > &children) {
            const vector<int> leptons = { 11 /* e*/, 12 /*ve*/, 13 /*mu*/, 14 /*vm*/, 15 /*ta*/, 16 /*vt*/ };
            vector<int> chPgds(children); // convert index -> pdg
            transform(chPgds.begin(), chPgds.end(), chPgds.begin(),
                      [&](const int &iCh) -> int { return pdgs[iCh]; });
            size_t n_of_leptons = count_if(chPgds.begin(), chPgds.end(), IsLepton());
            return pdgs.size()>0 and n_of_leptons==0;
        };
    } decays_hadronically;

    for(int iP=0;iP<static_cast<int>(mc_pdgId.size()); ++iP){
        bool is_W =  abs(ipdgs[iP])==24;
        if(not is_W)
            continue;
        else if(decays_hadronically(ipdgs, mc_children_index[iP]))
            hadronicWindices.push_back(iP);
    }
    index = hadronicWindices.size()>0 ? hadronicWindices[0] : index;
    return index;
}
//----------------------------------------------------------
int TruthPrinter::their_truthflav_to_mytruthflav (const int flav_in)
{
    int flav_out = -1;
    if(flav_in==0) flav_out = 0; // light
    else if(flav_in==4) flav_out = 1; // c
    else if(flav_in==5) flav_out = 2; // b
    else {
        /* do nothing, too many printouts */ ;
        // cout<<"their_truthflav_to_mytruthflav: expect [0,4,5], got "<<std::to_string(flav_in)<<endl;
    // DG-2016-10-12 no idea what we should do for the 15; ask Yulia
    // throw std::invalid_argument("their_truthflav_to_mytruthflav: expect [0,1,2], got "+std::to_string(flav_in));
    }
    return flav_out;
}
//----------------------------------------------------------
int TruthPrinter::pdg_truthflav_to_mytruthflav(const int &pdg)
{
    const int flav_in = abs(pdg);
    int flav_out = 0;
    if(flav_in==1 || // d
       flav_in==2 || // u
       flav_in==3 )  // s
        flav_out = 0;
    else if(flav_in==4) flav_out = 1; // c
    else if(flav_in==5) flav_out = 2; // b
    else
        cout<<"pdg_truthflav_to_mytruthflav: expect [1,2,3,4,5], got "<<std::to_string(flav_in)<<endl;
    return flav_out;
}
//----------------------------------------------------------
bool TruthPrinter::is_thc_event(const std::vector < float > &mc_pdgId,
                                const std::vector < std::vector < int > > &mc_children_index)
{
    bool is_thc = false;
    for(int iP=0;iP<static_cast<int>(mc_pdgId.size()); ++iP){
        bool is_top =  abs(mc_pdgId[iP])==6;
        if(not is_top) continue;
        else {
            bool has_higgs = false;
            bool has_c = false;
            for(auto iC : mc_children_index[iP]) {
                int pdg_child = abs(mc_pdgId[iC]);
                if(pdg_child==25) has_higgs=true;
                if(pdg_child==4) has_c=true;
            }
            is_thc = is_thc or (is_top and has_higgs and has_c);
        }
    }
    return is_thc;
}
//----------------------------------------------------------
} // tth
