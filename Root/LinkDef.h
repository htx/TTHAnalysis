#ifdef __CINT__
#include "TTHAnalysis/TTH_OutputData.h"
#include "TTHAnalysis/TTH_NtupleData.h"
#include "TTHAnalysis/TTH_NtupleReader.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class TTH_OutputData+;
#pragma link C++ class TTH_NtupleData+;
#pragma link C++ class TTH_NtupleReader+;

#endif
