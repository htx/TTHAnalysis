#include "TTHAnalysis/Training.h"

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"

#include <iostream>
#include <stdexcept>
#include <vector>

using std::cout;
using std::endl;
using std::vector;
using std::string;

namespace tth
{
//----------------------------------------------------------
TrainingSig::TrainingSig():
    hmbb(nullptr),
    hmbbjminusmbb(nullptr),
    hmlvb0(nullptr),
    hmlvb1(nullptr),
    hmlvb2(nullptr),
    fWll(0.0),
    fWlc(0.0),
    fAllMatch(0.0),
    fMiss1Match(0.0)
{}
//----------------------------------------------------------
TrainingBkg::TrainingBkg():
    hmlvb0(nullptr),
    hmlvb1(nullptr),
    hmlvb2(nullptr),
    hmqq(nullptr),
    hmqqbminusmqq(nullptr),
    fWll(0.0),
    fWlc(0.0),
    fAllMatch(0.0),
    fMiss1Match(0.0)
{}
//----------------------------------------------------------
/// retrieve histogram or nullptr
struct RetrieveOrNull
{
    TFile *inf;
    bool quiet;
    bool verbose;
    RetrieveOrNull(TFile *f):inf(f), quiet(false), verbose(false) {};
    const string fname() const { return string(inf->GetName()); }
    TH1* getTH1(const string &hname) {
        TH1 *h = nullptr;
        if(TH1 *hh = static_cast<TH1*>(inf->Get(hname.c_str()))) {
            h = hh;
            if(verbose) cout<<"retrieved TH1 '"<<hname<<"' from "<<fname()<<endl;
        } else if(not quiet) cout<<"Cannot retrieve TH1 '"<<hname<<"' from "<<fname()<<endl;
        return h;
    }
    TH2* getTH2(const string &hname) {
        TH2 *h = nullptr;
        if(TH2 *hh = static_cast<TH2*>(inf->Get(hname.c_str()))) {
            h = hh;
            if(verbose) cout<<"retrieved TH2 '"<<hname<<"' from "<<fname()<<endl;
        } else if(not quiet) cout<<"Cannot retrieve TH2 '"<<hname<<"' from "<<fname()<<endl;
        return h;
    }
};
//----------------------------------------------------------
/// just a list of the regions we consider
std::vector<std::string> all_training_regions()
{
    vector<string> regions = {
        "4jin2bin",
        "4jex2bex",
        "4jex3bex",
        "4jex4bin",
        "5jex2bex",
        "5jex3bex",
        "5jex4bin",
        "6jin2bex",
        "6jin3bex",
        "6jin4bin",
        "4jin2bin_hyp3", // CAJR
        "4jin2bin_hyp1", // CAJR
        "4jex2bex_hyp1", // CAJR
        "4jex3bex_hyp1", // CAJR
        "4jex4bin_hyp1", // CAJR
        "5jex2bex_hyp1", // CAJR
        "5jex3bex_hyp1", // CAJR
        "5jex4bin_hyp1", // CAJR
        "6jin2bex_hyp1", // CAJR
        "6jin3bex_hyp1", // CAJR
        "6jin4bin_hyp1"  // CAJR
    };
    return regions;
}
//----------------------------------------------------------
TrainingSig& TrainingSig::retrieveMassTemplates(TFile *f, const std::string &region)
{
    // todo: this check is only good for typos.
    // A check on the actually available regions should actually be
    // based on the values listed in plot_kinematic_templates.py (or
    // peek the files contents)
    const vector<string> available_regions = tth::all_training_regions();
    if(find(available_regions.begin(), available_regions.end(), region)==available_regions.end())
        throw std::invalid_argument("This is not a valid region '"+region+"'");

    RetrieveOrNull in_sig(f);
    in_sig.verbose = true; // tmp
    hmbb          = in_sig.getTH1("reg_"+region+"_Mbb");
    hmbbjminusmbb = in_sig.getTH1("reg_"+region+"_MbbjMinusMbb");
    hmlvb1        = in_sig.getTH1("reg_"+region+"_Mlvb1");
    hmlvb2        = in_sig.getTH1("reg_"+region+"_Mlvb2");
    hmlvb0        = in_sig.getTH1("reg_"+region+"_Mlvb0");
    for(auto h : {hmbb, hmbbjminusmbb, hmlvb1, hmlvb2, hmlvb0}){
        if(h) {
            h->SetDirectory(0);
            if(h->Integral())
                h->Scale(1.0/h->Integral());
        }
    }
    return *this;
}
//----------------------------------------------------------
TrainingSig& TrainingSig::retrieveMatchingFractions(TFile *f, const std::string &region)
{
    const vector<string> available_regions = tth::all_training_regions();
    if(find(available_regions.begin(), available_regions.end(), region)==available_regions.end())
        throw std::invalid_argument("This is not a valid region '"+region+"'");

    RetrieveOrNull in_sig(f);
    if(TH1* hcounts = in_sig.getTH1("reg_"+region+"_matchingcounts")){
        const float tot = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("tot"));
        const float allm = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("allmatch"));
        const float miss = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("miss1match"));
        const float other = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("other"));
        if(allm+miss) { // for now only consider allm vs. miss1
            fAllMatch = allm / (allm+miss);
            fMiss1Match = miss / (allm+miss);
            cout<<"matching fractions for "<<region<<" :"
                <<" "<<fAllMatch<<", "<<fMiss1Match
                <<" (counts: "<<tot<<", "<<allm<<", "<<miss<<", "<<other<<")"
                <<" from "<<f->GetName()
                <<endl;
        } else {
            cout<<"Cannot determine matching fractions for "<<region
                <<" (counts: "<<tot<<", "<<allm<<", "<<miss<<", "<<other<<")"
                <<endl;
        }
    } else {
        cout<<"Missing histogram to determine matching fractions for "<<region<<endl;
    }
    return *this;
}
//----------------------------------------------------------
TrainingBkg& TrainingBkg::retrieveMassTemplates(TFile *f, const std::string &region)
{
    // todo: same comment as for TrainingSig::retrieveMassTemplates
    const vector<string> available_regions = tth::all_training_regions();
    if(find(available_regions.begin(), available_regions.end(), region)==available_regions.end())
        throw std::invalid_argument("This is not a valid region '"+region+"'");

    RetrieveOrNull in_bkg(f);
    in_bkg.verbose = true; // tmp
    hmqq          = in_bkg.getTH1("reg_"+region+"_Mqq");
    hmqqbminusmqq = in_bkg.getTH1("reg_"+region+"_MqqbMinusMqq");
    hmlvb1        = in_bkg.getTH1("reg_"+region+"_Mlvb1");
    hmlvb2        = in_bkg.getTH1("reg_"+region+"_Mlvb2");
    hmlvb0        = in_bkg.getTH1("reg_"+region+"_Mlvb0");
    for(auto h : {hmqq, hmqqbminusmqq, hmlvb1, hmlvb2, hmlvb0}){
        if(h) {
            h->SetDirectory(0);
            if(h->Integral())
                h->Scale(1.0/h->Integral());
        } else {
        }
    }
    return *this;
}
//----------------------------------------------------------
TrainingBkg& TrainingBkg::retrieveWflavorFractions(TFile *f, const std::string &region)
{
    // flavor fractions of quarks from W>qq (might need to change reg --> reg+"_4matched")
    RetrieveOrNull in_bkg(f);
    if(TH2 *h2fW = in_bkg.getTH2("reg_"+region+"_subqfromWtruthflav_vs_leadqfromWtruthflav")){
        const float bin_light = 0.5, bin_c = 1.5; // see booking bins in TTH_Analysis
        // For now assume W > ll, lc, cl, and neglect W > cc, xb
        const double counts_ll = h2fW->GetBinContent(h2fW->FindFixBin(bin_light, bin_light));
        const double counts_lc = h2fW->GetBinContent(h2fW->FindFixBin(bin_light, bin_c));
        const double counts_cl = h2fW->GetBinContent(h2fW->FindFixBin(bin_c,     bin_light));
        fWll =               counts_ll / (counts_ll + counts_lc + counts_cl);
        fWlc = (counts_lc + counts_cl) / (counts_ll + counts_lc + counts_cl);
        cout<<"fractions truth W flavor '"<<region<<"'"
            <<": fWll "<<fWll<<" fWlc "<<fWlc
            <<" from "<<f->GetName()
            <<endl;
    } else {
        cout<<"Cannot determine background fractions of truth W flavor for "<<region<<endl;
        fWll = 0.0;
        fWlc = 0.0;
    }
    return *this;
}
//----------------------------------------------------------
TrainingBkg& TrainingBkg::retrieveMatchingFractions(TFile *f, const std::string &region)
{
    // duplication with TrainingSig: refactor \todo
    const vector<string> available_regions = tth::all_training_regions();
    if(find(available_regions.begin(), available_regions.end(), region)==available_regions.end())
        throw std::invalid_argument("This is not a valid region '"+region+"'");
    RetrieveOrNull in_bkg(f);
    if(TH1* hcounts = in_bkg.getTH1("reg_"+region+"_matchingcounts")){
        const float tot = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("tot"));
        const float allm = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("allmatch"));
        const float miss = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("miss1match"));
        const float other = hcounts->GetBinContent(hcounts->GetXaxis()->FindBin("other"));
        if(allm+miss) { // for now only consider allm vs. miss
            fAllMatch = allm / (allm+miss);
            fMiss1Match = miss / (allm+miss);
            cout<<"matching fractions for "<<region<<" :"
                <<" "<<fAllMatch<<", "<<fMiss1Match
                <<" (counts: "<<tot<<", "<<allm<<", "<<miss<<", "<<other<<")"
                <<endl;
        } else {
            cout<<"Cannot determine matching fractions for "<<region
                <<" (counts: "<<tot<<", "<<allm<<", "<<miss<<", "<<other<<")"
                <<endl;
        }
    } else {
        cout<<"Missing histogram to determine matching fractions for "<<region<<endl;
    }
    return *this;
}
//----------------------------------------------------------
} // tth
