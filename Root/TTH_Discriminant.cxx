#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <stdexcept>
#include "IFAETopFramework/AnalysisObject.h"
#include "IFAETopFramework/HistManager.h"
#include "IFAETopFramework/Selection.h"


#include "TTHAnalysis/BtagWeights.h"
#include "TTHAnalysis/TTH_Options.h"
#include "TTHAnalysis/TTH_Discriminant.h"
#include "TTHAnalysis/TTH_Options.h"
#include "TTHAnalysis/TTH_TruthMatchHelper.h"
#include "TTHAnalysis/TruthPrinter.h"
#include "TTHAnalysis/TruthJetMatch.h"
#include "TTHAnalysis/TlvTruth.h"
#include "TTHAnalysis/Kinematic.h"
#include "TTHAnalysis/Hypotheses.h"

#include "TSystem.h"

#include <iostream>

using tth::TruthPrinter;
using tth::RecoJet;
using tth::TrainingSig;
using tth::TrainingBkg;

// compute the difference in quadrature between m(1+2+3) - m(1+2)
double compute_delta_quadrature_mass(const TLorentzVector &p1,
                                     const TLorentzVector &p2,
                                     const TLorentzVector &p3)
{
    const double m123 = (p1 + p2 + p3).M();
    const double m12 = (p1 + p2).M();
    return sqrt(m123*m123 - m12*m12);
}
//_______________________________________________________________
//
double compute_delta_mass(const TLorentzVector &p1,
                                     const TLorentzVector &p2,
                                     const TLorentzVector &p3)
{
    const double m123 = (p1 + p2 + p3).M();
    const double m12 = (p1 + p2).M();
    return m123 - m12;
}
//_______________________________________________________________
//
TTH_Discriminant::TTH_Discriminant(TTH_Options *opt)
    : m_histMngr(0), m_has_mc_branches(true) {
  m_histMngr = new HistManager();

  m_sample_id = opt->StrSampleID();
  const bool is_qcd = (opt->StrSampleName().find("QCD") != std::string::npos);
  m_has_mc_branches = not(opt->IsData() or is_qcd);
  if(m_sample_id.find("41069") != std::string::npos) m_sig=true;
  if(m_sample_id=="410000." || m_sample_id=="410120.") m_bkg=true;
  //std::cout << "m_sig = " <<  m_sig << std::endl;
  //std::cout << "m_bkg = " <<  m_bkg << std::endl;

}
//_______________________________________________________________
//
TTH_Discriminant::TTH_Discriminant(const std::string &sampleid,
                                   const bool &has_mc_branches)
    : m_histMngr(0), m_has_mc_branches(true) {
  m_histMngr = new HistManager();

  m_sample_id = sampleid;
  m_has_mc_branches = has_mc_branches;
  if(m_sample_id.find("41069") != std::string::npos) m_sig=true;
  if(m_sample_id=="410000." || m_sample_id=="410120.") m_bkg=true;
  //std::cout << "m_sig = " <<  m_sig << std::endl;
  //std::cout << "m_bkg = " <<  m_bkg << std::endl;

}

//________________________________________________________________
//
TTH_Discriminant::~TTH_Discriminant() {
  delete m_histMngr;
  m_recoJets.clear();
  m_recoExtraJets.clear();
  m_recoExtraJetsB.clear();
  m_recoExtraJetsNotb.clear();
}
//----------------------------------------------------------
TTH_Discriminant::Result& TTH_Discriminant::Result::setDefaults()
{
    Discriminant = ProbSig = ProbBkg = -9.0;
    ProbSigNum = ProbSigDen = 0.0;
    ProbBkgNum = ProbBkgDen = 0.0;
    ProbBkgNum_bb = ProbBkgNum_bj = 0.0;
    ProbBkgDen_bb = ProbBkgDen_bj = 0.0;
    return *this;
}
//________________________________________________________________
//

bool TTH_Discriminant::FillHistosDataMC() {
  return true;
} // FillHistosDataMC
//________________________________________________________________
//
tth::TlvTruth TTH_Discriminant::fillTruthTlvs(const vector<float> *pts,
                                              const vector<float> *etas,
                                              const vector<float> *phis,
                                              const vector<float> *masses,
                                              const vector<float> *pdgs,
                                              const vector < vector < int > > *childrenIndices) const
{
    tth::TlvTruth tlvt;
    /// @brief a local function to assign TLV from the input ntuple
    struct AssignTLorentzVector{
        const vector<float> *m_pts, *m_etas, *m_phis, *m_ms;
        AssignTLorentzVector(const vector<float> *pts, const vector<float> *etas,
                             const vector<float> *phis, const vector<float> *ms):
            m_pts(pts), m_etas(etas), m_phis(phis), m_ms(ms) {};
        void operator()(TLorentzVector &tlv, const int index) const {
            tlv.SetPtEtaPhiM(m_pts->at(index),
                             m_etas->at(index),
                             m_phis->at(index),
                             m_ms->at(index));
        }
    } assign4mom(pts, etas, phis, masses);

    const bool missing_any_input = ((not pts) or
                                    (not etas) or
                                    (not phis) or
                                    (not masses) or
                                    (not pdgs) or
                                    (not childrenIndices));
    if(missing_any_input){
        return tlvt;
    }
    for(size_t iTruth0 = 0; iTruth0 < pts->size(); ++iTruth0){ // 0th loop: find Top
        if(IsTop(pdgs->at(iTruth0))){
            const vector<int> &topChildren=childrenIndices->at(iTruth0);
            TLorentzVector qfromW1, qfromW2;// 2 q from W before pt sorting
            bool foundqfromW1=false; bool foundqfromW2=false;
            for(const int iTopChild1 : topChildren){ // 1st: find W, H
                const int pdgTopChild1 = pdgs->at(iTopChild1);
                if(IsW(pdgTopChild1)){
                    const vector<int> &WChildren=childrenIndices->at(iTopChild1);
                    const size_t nWchildren=childrenIndices->at(iTopChild1).size();
                    for(size_t iWchild = 0; iWchild < nWchildren; ++iWchild){
                        int pdgWchild=pdgs->at(childrenIndices->at(iTopChild1).at(iWchild));
                        if(IsLepton(pdgWchild) || IsNu(pdgWchild)){ // leptonic W -> lep top
                            for(size_t iTopChild2 : topChildren){ // 2nd: find b from lep top
                                if(IsBot(pdgs->at(iTopChild2))){
                                    if (!tlvt.has_bleptop1 && !tlvt.has_bleptop2) {
                                        assign4mom(tlvt.bleptop1, iTopChild2);
                                        tlvt.has_bleptop1 = true;
                                    } else if (!tlvt.has_bleptop2) {
                                        assign4mom(tlvt.bleptop2, iTopChild2);
                                        tlvt.has_bleptop2 = true;
                                    }
                                }
                            }
                            if(tlvt.has_bleptop1) break;
                        } else if (IsUdcs(pdgWchild)){ // hadronic W -> had top
                            if (!foundqfromW1 && !foundqfromW2) {
                                assign4mom(qfromW1, WChildren.at(iWchild));
                                foundqfromW1=true;
                            } else if (!foundqfromW2){
                                assign4mom(qfromW2, WChildren.at(iWchild));
                                foundqfromW2=true;
                            }
                            if(foundqfromW1 && foundqfromW2){
                                const bool are_sorted = qfromW1.Pt() > qfromW2.Pt();
                                tlvt.leadqW = are_sorted ? qfromW1 : qfromW2;
                                tlvt.subqW  = are_sorted ? qfromW2 : qfromW1;
                                tlvt.has_leadqW=true;
                                tlvt.has_subqW=true;
                            }
                            for(size_t iTopChild3 : topChildren){ // 3rd: find b from had top
                                if(IsBot(pdgs->at(iTopChild3))){
                                    assign4mom(tlvt.bhadtop, iTopChild3);
                                    tlvt.has_bhadtop=true;
                                }
                            }
                        } // IsUdcs
                        else{
                            cout<<"Something strange is coming from W:"
                                <<" pdgid = "<< pdgs->at(WChildren.at(iWchild))
                                <<endl;
                        }
                    } // for(iWchild)
                } // IsW
                else if(IsHiggs(pdgTopChild1)){
                    const vector<int> &HChildren=childrenIndices->at(iTopChild1);
                    const size_t nHchildren=childrenIndices->at(iTopChild1).size();
                    for(size_t iHchild = 0; iHchild <nHchildren; ++iHchild){//Higgs children loop
                        int pdgHchild=pdgs->at(childrenIndices->at(iTopChild1).at(iHchild));
                        if (IsBot(pdgHchild)){ // 2 b from Higgs
                            if (!tlvt.has_bfromH1 && !tlvt.has_bfromH2) {
                                assign4mom(tlvt.bfromH1, HChildren.at(iHchild));
                                tlvt.has_bfromH1=true;
                            } else if (!tlvt.has_bfromH2){
                                assign4mom(tlvt.bfromH2, HChildren.at(iHchild));
                                tlvt.has_bfromH2=true;
                            }
                            if(tlvt.has_bfromH1 && tlvt.has_bfromH2){
                                const bool are_sorted = tlvt.bfromH1.Pt() > tlvt.bfromH2.Pt();
                                tlvt.leadbH = are_sorted ? tlvt.bfromH1 : tlvt.bfromH2;
                                tlvt.subbH  = are_sorted ? tlvt.bfromH2 : tlvt.bfromH1;
                                tlvt.has_leadbH=true;
                                tlvt.has_subbH=true;
                            }
                            for(size_t iTopChild4 : topChildren){// 4th: find u/c from t->Hu/c
                                if(IsUc(pdgs->at(iTopChild4)) and
                                   pts->at(iTopChild4)>20.0){
                                    // o_qFromTop_pt probably unused TODO cleanup DG-10-27
                                    // out.o_qFromTop_pt=pts->at(iTopChild4);
                                    assign4mom(tlvt.qfromHtop, iTopChild4);
                                    tlvt.has_qfromHtop=true;
                                } // IsUc
                            } // for(iTopChild4)
                        } // IsBot (from Higgs)
                    } // for(iHchild)
                } // IsHiggs
            } // for(iTopChild1)
        } // IsTop
    } // for(iTruth0)
    return tlvt;
} // fillTruthTlvs
//----------------------------------------------------------
tth::TruthMatchingOutput
TTH_Discriminant::matchToTruth(const TLorentzVector &tlv_lep,
                               const std::vector<TLorentzVector> &neutrino_solutions,
                               const tth::TlvTruth &tlvt)
{
    tth::TruthMatchingOutput out;
    const bool print_debug = false;
    const auto& ttf2mtf = TruthPrinter::their_truthflav_to_mytruthflav; // alias to shorten function
    if(m_sig){
        tth::SignalHypothesis sig;
        for(RecoJet &reco_jet : m_recoJets) {
            reco_jet.truthmatch = determineJetTruthMatch(reco_jet, tlvt);
            sig.assignRecoJet(reco_jet);
        }
        out.o_matchedjets_n = sig.numberMatched();
        out.o_nextrajets = sig.num_extra_jets;
        out.o_nottruthmatch = sig.whatIsMissing();
        if(print_debug) cout<<sig.str()<<endl;
        tth::KinematicConfigurationSig kin = sig.kinematic(tlv_lep, neutrino_solutions);
        out.o_Mbb          = kin.Mbb;
        out.o_Mlvb0        = kin.Mlvb0;
        out.o_Mlvb1        = kin.Mlvb1;
        out.o_Mlvb2        = kin.Mlvb2;
        out.o_Mbbj         = kin.Mbbq;
        out.o_MbbjMinusMbb = kin.MbbqMinusMbb;
    } else if(m_bkg){
        tth::BackgroundHypothesis bkg;
        for(RecoJet &reco_jet : m_recoJets) {
            reco_jet.truthmatch = determineJetTruthMatch(reco_jet, tlvt);
            bkg.assignRecoJet(reco_jet);
        }
        out.o_matchedjets_n = bkg.numberMatched();
        out.o_nextrajets = bkg.num_extra_jets;
        out.o_nottruthmatch = bkg.whatIsMissing();
        if(print_debug) cout<<bkg.str()<<endl;
        tth::KinematicConfigurationBkg kin = bkg.kinematic(tlv_lep, neutrino_solutions);
        out.o_Mqq          = kin.Mqq;
        out.o_Mlvb0        = kin.Mlvb0;
        out.o_Mlvb1        = kin.Mlvb1;
        out.o_Mlvb2        = kin.Mlvb2;
        out.o_Mqqb         = kin.Mqqb;
        out.o_MqqbMinusMqq = kin.MqqbMinusMqq;
        // for the bkg we also need the W flavor fractions
        using tth::UnknownTruthJetMatch;
        out.o_leadJetFromW_truthflav = ttf2mtf(bkg.q1 ? bkg.q1->truthflav : UnknownTruthJetMatch);
        out.o_subJetFromW_truthflav  = ttf2mtf(bkg.q2 ? bkg.q2->truthflav : UnknownTruthJetMatch);
    }
    return out;
}
//----------------------------------------------------------
std::vector<tth::RecoJet>
TTH_Discriminant::fillRecoJets(const std::vector< AnalysisObject* > &jets) const
{
    std::vector<tth::RecoJet> recoJets;
    int iJet = 0;
    for(const AnalysisObject* jet : jets){
        const float pt = jet->Pt();
        const float eta = jet->Eta();
        const float abs_eta = fabs(eta);
        const float phi = jet->Phi();
        const float energy = jet->Energy();
        const float tag_out = jet->GetMoment("btagw");
        const int truth_flav = static_cast<int>(jet->GetMoment("truthLabel"));
        if ((pt < 25.) || (abs_eta > 2.5)) continue; // outside acceptance
        recoJets.push_back(RecoJet()
                           .setPtEtaPhiEnergy(pt, eta, phi, energy)
                           .setBtagAttributes(tth::GetBTagWeightsVarOP(tag_out))
                           .setTruthFlavor(truth_flav));
        iJet++;
    } // for(jet)
    return recoJets;
}
//----------------------------------------------------------
bool TTH_Discriminant::ClearData() {
  m_recoJets.clear();
  m_recoExtraJets.clear();
  m_recoExtraJetsB.clear();
  m_recoExtraJetsNotb.clear();
  return true;
}
//----------------------------------------------------------
std::vector<TLorentzVector>
TTH_Discriminant::NuSolution(const TLorentzVector &l, const TLorentzVector &nu) {
  std::vector<TLorentzVector> nuSolutions;
  TLorentzVector nuSolution;
  //jr
  //double WMass = 80385;
  double WMass = 80.385;

  double MissingPt = sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py());
  double alpha = 0.5 * (WMass * WMass - l.M() * l.M());
  double beta = alpha + nu.Px() * l.Px() + nu.Py() * l.Py();
  double gamma = -(beta * beta - (l.E() * l.E() * MissingPt * MissingPt)) / (l.E() * l.E() - l.Pz() * l.Pz());
  double lambda = 2. * beta * l.Pz() / (l.E() * l.E() - l.Pz() * l.Pz());
  double delta = lambda * lambda - 4. * gamma;
  if (delta < 0) {
    double L = sqrt(l.E() * l.E() - l.Pz() * l.Pz());
    double M = sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py());
    double N = nu.Px() * l.Px() + nu.Py() * l.Py();
    double Alfa1 = -alpha * (N + M * L) / (N * N - M * M * L * L);
    double Alfa2 = -alpha * (N - M * L) / (N * N - M * M * L * L);
    double Alfa;
    if (Alfa1 > 0)
      Alfa = Alfa1;
    if (Alfa2 > 0)
      Alfa = Alfa2;
    TLorentzVector nu2;
    nu2.SetPxPyPzE(nu.Px() * Alfa, nu.Py() * Alfa, nu.Pz() * Alfa, nu.E() * Alfa);
    double betaPrime2 = alpha + nu2.Px() * l.Px() + nu2.Py() * l.Py();
    double lambdaPrime2 = 2. * betaPrime2 * l.Pz() / (l.E() * l.E() - l.Pz() * l.Pz());
    double Pz2 = lambdaPrime2 / 2;
    double E2 = sqrt(nu2.Px() * nu2.Px() + nu2.Py() * nu2.Py() + Pz2 * Pz2);
    nuSolution.SetPxPyPzE(nu2.Px(), nu2.Py(), Pz2, E2);
    nuSolutions.push_back(nuSolution);
  } else {
    double Pz1 = (lambda - sqrt(delta)) / 2.;
    double Pz2 = (lambda + sqrt(delta)) / 2.;
    double E1 = sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py() + Pz1 * Pz1);
    double E2 = sqrt(nu.Px() * nu.Px() + nu.Py() * nu.Py() + Pz2 * Pz2);
    if (TMath::Abs(Pz1) < TMath::Abs(Pz2)) {
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz1, E1);
      nuSolutions.push_back(nuSolution);
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz2, E2);
      nuSolutions.push_back(nuSolution);
    } else {
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz2, E2);
      nuSolutions.push_back(nuSolution);
      nuSolution.SetPxPyPzE(nu.Px(), nu.Py(), Pz1, E1);
      nuSolutions.push_back(nuSolution);
    }
  }
  return nuSolutions;
}

void TTH_Discriminant::GetPartPermNoOrder(std::string sofar, std::string rest, int n) {
  std::string substring;
  if (n == 0) {
    ExtraJetCombinations.push_back(sofar);
  } else {
    for (size_t i = 0; i < rest.length(); i++) {
      substring = rest.substr(i + 1, rest.length());
      GetPartPermNoOrder(sofar + rest[i], substring, n - 1);
    }
  }
}

//----------------------------------------------------------
void TTH_Discriminant::print_unmatched_jets() const
{
    cout<<"m_recoExtraJets["<<m_recoExtraJets.size()<<"]"<<endl;
    size_t iJet = 0;
    for(auto const &jet : m_recoExtraJets) {
        cout<<"["<<iJet<<"]"
            <<" (pt, eta, phi) :"
            <<" ("<<jet.Pt()<<", "<<jet.Eta()<<", "<<jet.Phi()<<")"
            <<" truthflav "<<jet.truthflav
            <<" truthmatch "<<jet.truthmatch
            <<endl;
        iJet++;
    }
}

//----------------------------------------------------------
void TTH_Discriminant::InitTrainings()
{
    std::string base_path = gSystem->ExpandPathName("${ROOTCOREBIN}/data/TTHAnalysis/");
    TFile* f_sig_mm = TFile::Open((base_path
                                +"/templates_uHcW_all_match.root").c_str());
    m_sig_4jin2bin_am.retrieveMassTemplates(f_sig_mm, "4jin2bin");
    f_sig_mm->Close();
    f_sig_mm = TFile::Open((base_path+"/templates_uHcW_miss1match.root").c_str());
    // CAJR m_sig_4jin2bin_mm.retrieveMassTemplates(f_sig_mm, "4jin2bin");
    // Select hypothesis where missing parton is light quark (u or c)
    m_sig_4jin2bin_mm.retrieveMassTemplates(f_sig_mm, "4jin2bin_hyp3");
    // CAJR
    m_sig_4jex2bex_mm.retrieveMatchingFractions(f_sig_mm, "4jin2bin"); // 4j (matching from mm)
    m_sig_4jex2bex_mm.retrieveMatchingFractions(f_sig_mm, "4jex2bex");
    m_sig_4jex3bex_mm.retrieveMatchingFractions(f_sig_mm, "4jex3bex");
    m_sig_4jex4bin_mm.retrieveMatchingFractions(f_sig_mm, "4jex4bin");
    m_sig_5jex2bex_mm.retrieveMatchingFractions(f_sig_mm, "5jex2bex"); // 5j
    m_sig_5jex3bex_mm.retrieveMatchingFractions(f_sig_mm, "5jex3bex");
    m_sig_5jex4bin_mm.retrieveMatchingFractions(f_sig_mm, "5jex4bin");
    m_sig_6jin2bex_mm.retrieveMatchingFractions(f_sig_mm, "6jin2bex"); // 6j
    m_sig_6jin3bex_mm.retrieveMatchingFractions(f_sig_mm, "6jin3bex");
    m_sig_6jin4bin_mm.retrieveMatchingFractions(f_sig_mm, "6jin4bin");
    f_sig_mm->Close();

    // TODO merge the 41000/410120 and light/bb/cc
    TFile* f_bkg_am = TFile::Open((base_path
                                   +"/templates_ttbarlight_410000_all_match.root").c_str());
    m_bkg_4jin2bin_am.retrieveMassTemplates(f_bkg_am, "4jin2bin");
    m_bkg_4jin2bin_am.retrieveWflavorFractions(f_bkg_am, "4jin2bin"); // 4j (W flavor from am)
    m_bkg_4jex2bex_am.retrieveWflavorFractions(f_bkg_am, "4jex2bex");
    m_bkg_4jex3bex_am.retrieveWflavorFractions(f_bkg_am, "4jex3bex");
    m_bkg_4jex4bin_am.retrieveWflavorFractions(f_bkg_am, "4jex4bin");
    m_bkg_5jex2bex_am.retrieveWflavorFractions(f_bkg_am, "5jex2bex"); // 5j
    m_bkg_5jex3bex_am.retrieveWflavorFractions(f_bkg_am, "5jex3bex");
    m_bkg_5jex4bin_am.retrieveWflavorFractions(f_bkg_am, "5jex4bin");
    m_bkg_6jin2bex_am.retrieveWflavorFractions(f_bkg_am, "6jin2bex"); // 6j
    m_bkg_6jin3bex_am.retrieveWflavorFractions(f_bkg_am, "6jin3bex");
    m_bkg_6jin4bin_am.retrieveWflavorFractions(f_bkg_am, "6jin4bin");
    f_bkg_am->Close();
    TFile* f_bkg_mm = TFile::Open((base_path
                                   +"/templates_ttbarlight_410000_miss1match.root").c_str());
    // CAJR Select hyp1 (q2 missing)
    m_bkg_4jin2bin_mm.retrieveMassTemplates(f_bkg_mm, "4jin2bin_hyp1");
    m_bkg_4jin2bin_mm.retrieveWflavorFractions(f_bkg_mm, "4jin2bin_hyp1"); // 4j (W flavor from am)
    m_bkg_4jex2bex_mm.retrieveWflavorFractions(f_bkg_mm, "4jex2bex_hyp1");
    m_bkg_4jex3bex_mm.retrieveWflavorFractions(f_bkg_mm, "4jex3bex_hyp1");
    m_bkg_4jex4bin_mm.retrieveWflavorFractions(f_bkg_mm, "4jex4bin_hyp1");
    m_bkg_5jex2bex_mm.retrieveWflavorFractions(f_bkg_mm, "5jex2bex_hyp1"); // 5j
    m_bkg_5jex3bex_mm.retrieveWflavorFractions(f_bkg_mm, "5jex3bex_hyp1");
    m_bkg_5jex4bin_mm.retrieveWflavorFractions(f_bkg_mm, "5jex4bin_hyp1");
    m_bkg_6jin2bex_mm.retrieveWflavorFractions(f_bkg_mm, "6jin2bex_hyp1"); // 6j
    m_bkg_6jin3bex_mm.retrieveWflavorFractions(f_bkg_mm, "6jin3bex_hyp1");
    m_bkg_6jin4bin_mm.retrieveWflavorFractions(f_bkg_mm, "6jin4bin_hyp1");
    // CAJR
    m_bkg_4jin2bin_mm.retrieveMatchingFractions(f_bkg_mm, "4jin2bin"); // 4j (matching from mm)
    m_bkg_4jex2bex_mm.retrieveMatchingFractions(f_bkg_mm, "4jex2bex");
    m_bkg_4jex3bex_mm.retrieveMatchingFractions(f_bkg_mm, "4jex3bex");
    m_bkg_4jex4bin_mm.retrieveMatchingFractions(f_bkg_mm, "4jex4bin");
    m_bkg_5jex2bex_mm.retrieveMatchingFractions(f_bkg_mm, "5jex2bex"); // 5j
    m_bkg_5jex3bex_mm.retrieveMatchingFractions(f_bkg_mm, "5jex3bex");
    m_bkg_5jex4bin_mm.retrieveMatchingFractions(f_bkg_mm, "5jex4bin");
    m_bkg_6jin2bex_mm.retrieveMatchingFractions(f_bkg_mm, "6jin2bex"); // 6j
    m_bkg_6jin3bex_mm.retrieveMatchingFractions(f_bkg_mm, "6jin3bex");
    m_bkg_6jin4bin_mm.retrieveMatchingFractions(f_bkg_mm, "6jin4bin");
    f_bkg_mm->Close();
}
//----------------------------------------------------------
const TTH_Discriminant::Result&
TTH_Discriminant::GetDiscriminant(const size_t &nj, const size_t &nb,
                                  const std::vector<tth::RecoJet> &recoJets,
                                  const std::vector<TLorentzVector> &NuSolutions,
                                  const TLorentzVector& tlv_lep)
{
    Result &result = m_result;
    result.setDefaults();
    if(NuSolutions.size()!=1 and NuSolutions.size()!=2)
        throw std::invalid_argument("Discriminant works with 1 or 2 nu solutions,"
                                    " got "+std::to_string(NuSolutions.size()));
    // kinematic templates
    const TrainingSig &tr_sig_am = m_sig_4jin2bin_am;
    const TrainingSig &tr_sig_mm = m_sig_4jin2bin_mm;
    const TrainingBkg &tr_bkg_am = m_bkg_4jin2bin_am;
    const TrainingBkg &tr_bkg_mm = m_bkg_4jin2bin_mm;
    // W flavor fractions for bkg
    const TrainingBkg &tr_bkg_am_fW = pickAllMatchBkgTraining(nj, nb);
    const TrainingBkg &tr_bkg_mm_fW = pickMiss1MatchBkgTraining(nj, nb);
    // matching fractions from 'miss1match'
    const TrainingSig &tr_sig_fm = pickMiss1MatchSigTraining(nj, nb);
    const TrainingBkg &tr_bkg_fm = pickMiss1MatchBkgTraining(nj, nb);

    const NumDenPair pSig_am = accumulateSigAllMatchedProbabilities(recoJets, NuSolutions, tlv_lep,
                                                                    tr_sig_am);
    const NumDenPair pSig_mm = accumulateSigAllMatchedProbabilities(recoJets, NuSolutions, tlv_lep,
                                                                    tr_sig_mm);
    const NumDenPair pBkg_am = accumulateBkgAllMatchedProbabilities(recoJets, NuSolutions, tlv_lep,
                                                                    tr_bkg_am,
                                                                    tr_bkg_am_fW.fWll,
                                                                    tr_bkg_am_fW.fWlc);
    const NumDenPair pBkg_mm = accumulateBkgAllMatchedProbabilities(recoJets, NuSolutions, tlv_lep,
                                                                    tr_bkg_mm,
                                                                    tr_bkg_mm_fW.fWll,
                                                                    tr_bkg_mm_fW.fWlc);
    result.ProbSigNum = (tr_sig_fm.fAllMatch*pSig_am.num + tr_sig_fm.fMiss1Match*pSig_mm.num);
    result.ProbSigDen = (tr_sig_fm.fAllMatch*pSig_am.den + tr_sig_fm.fMiss1Match*pSig_mm.den);
    result.ProbBkgNum = (tr_bkg_fm.fAllMatch*pBkg_am.num + tr_bkg_fm.fMiss1Match*pBkg_mm.num);
    result.ProbBkgDen = (tr_bkg_fm.fAllMatch*pBkg_am.den + tr_bkg_fm.fMiss1Match*pBkg_mm.den);
    using std::cout;
    using std::endl;
    // checks sig
    if(result.ProbSigNum<=0) { cout<<"!!! ProbSigNum = "<< result.ProbSigNum<<endl; }
    if(result.ProbSigDen<=0) { cout<<"!!! ProbSigDen = "<< result.ProbSigDen<<endl; }
    else result.ProbSig = result.ProbSigNum / result.ProbSigDen;
    // checks bkg
    if(result.ProbBkgNum<=0) { cout<<"!!! ProbBkgNum = "<< result.ProbBkgNum<<endl; }
    if(result.ProbBkgDen<=0) { cout<<"!!! ProbBkgDen = "<< result.ProbBkgDen<<endl; }
    else result.ProbBkg = result.ProbBkgNum /result.ProbBkgDen;
    // all good, divide
    if   (result.ProbSig==0 && result.ProbBkg==0) result.Discriminant=-3;
    else result.Discriminant = result.ProbSig / (result.ProbSig + result.ProbBkg);
    return result;
} // TTH_Discriminant::GetDiscriminant
//----------------------------------------------------------
TTH_Discriminant::NumDenPair
TTH_Discriminant::accumulateSigAllMatchedProbabilities(const std::vector<tth::RecoJet> &recoJets,
                                                       const std::vector<TLorentzVector> &NuSolutions,
                                                       const TLorentzVector& tlv_lep,
                                                       const tth::TrainingSig &tr_sig)
{
    NumDenPair prob(0.0, 0.0);
    tth::KinematicConfigurationSig kinSig;
    //Prob sig
    float PMlvb0_sig = -9;
    float PMlvb1_sig = -9;
    float PMlvb2_sig = -9;
    float PMbb = -9;
    float PMbbqMinusMbb = -9;
    float PermProbMPSig = 1;   // kin prob
    float PermProbBtagSig = 1; // btag prob
    struct {double operator()(const TH1* h, const double &x) { return h->GetBinContent(h->FindFixBin(x)); }} content_of_at;
    struct {float operator()(const float &v) { return (v < 0.0 ? 0.0 : v); }} force_non_negative;
/*
  Indices for the tthq all-matched hypothesis

                W   /---  l
               _/---
        t     /     \---  v
    +---------
    |         \
    |          \--------  b     j1
    |
    |         H    /----  b     j2
    |         _/---
    |   t    /     \----  b     j3
    +----------
               \
                \-------  q     j4
*/

    for (unsigned int j1 = 0; j1 < recoJets.size(); j1++){
        const RecoJet &rj1 = recoJets.at(j1);
        if (NuSolutions.size() == 1){
            kinSig.Mlvb0 = (rj1+tlv_lep+NuSolutions.at(0)).M();
            PMlvb0_sig = force_non_negative(content_of_at(tr_sig.hmlvb0, kinSig.Mlvb0));
        } else if (NuSolutions.size() == 2){
            kinSig.Mlvb1 = (rj1+tlv_lep+NuSolutions.at(0)).M();
            kinSig.Mlvb2 = (rj1+tlv_lep+NuSolutions.at(1)).M();
            PMlvb1_sig = force_non_negative(content_of_at(tr_sig.hmlvb1, kinSig.Mlvb1));
            PMlvb2_sig = force_non_negative(content_of_at(tr_sig.hmlvb2, kinSig.Mlvb2));
        }
        const float PMlvb_sig = (NuSolutions.size()==1 ?
                                 PMlvb0_sig :
                                 0.65 * PMlvb1_sig + 0.35 * PMlvb2_sig);
        for (unsigned int j2 = 0; j2 < recoJets.size(); j2++){
            if(j2==j1) continue;
            const RecoJet &rj2 = recoJets.at(j2);
            for (unsigned int j3 = 0; j3 < recoJets.size(); j3++){
                if(j3==j1 || j3==j2) continue;
                const RecoJet &rj3 = recoJets.at(j3);
                kinSig.Mbb = (rj2+rj3).M();
                PMbb = force_non_negative(content_of_at(tr_sig.hmbb, kinSig.Mbb));
                for (unsigned int j4 = 0; j4 < recoJets.size(); j4++){
                    if(j4==j1 || j4==j2 || j4==j3) continue;
                    const RecoJet &rj4 = recoJets.at(j4);
                    kinSig.MbbqMinusMbb = compute_delta_mass(rj2, rj3, rj4);
                    PMbbqMinusMbb = force_non_negative(content_of_at(tr_sig.hmbbjminusmbb,
                                                                     kinSig.MbbqMinusMbb));
                    PermProbMPSig = PMlvb_sig * PMbb * PMbbqMinusMbb;
                    PermProbBtagSig = (rj1.probB * rj2.probB * rj3.probB * rj4.probC);
                    // assume that any extra jet is light
                    for (unsigned int extra_j = 0; extra_j < recoJets.size(); extra_j ++){
                        if(extra_j==j1 || extra_j==j2 || extra_j==j3 || extra_j==j4 ) continue;
                        const RecoJet &extra_rj = recoJets.at(extra_j);
                        PermProbBtagSig *= extra_rj.probL;
                    }//extra_j loop
                    prob.num += PermProbBtagSig*PermProbMPSig;
                    prob.den += PermProbBtagSig;
                } // for(j4)
            } // for(j3)
        } // for(j2)
    } // for(j1)
    return prob;
}
//----------------------------------------------------------
TTH_Discriminant::NumDenPair
TTH_Discriminant::accumulateBkgAllMatchedProbabilities(const std::vector<tth::RecoJet> &recoJets,
                                                       const std::vector<TLorentzVector> &NuSolutions,
                                                       const TLorentzVector& tlv_lep,
                                                       const tth::TrainingBkg &tr_bkg,
                                                       const float fWll,
                                                       const float fWlc)
{
    NumDenPair prob(0.0, 0.0);
    tth::KinematicConfigurationBkg kinBkg;
    //Prob bkg
    float PMqq = -9;
    float PMqqbMinusMqq = -9;
    float PMlvb0_bkg = -9;
    float PMlvb1_bkg = -9;
    float PMlvb2_bkg = -9;
    float PermProbMPBkg = 1;   // kin prob
    float PermProbBtagBkg = 1; // btag prob
    struct {double operator()(const TH1* h, const double &x) { return h->GetBinContent(h->FindFixBin(x)); }} content_of_at;
    struct {float operator()(const float &v) { return (v < 0.0 ? 0.0 : v); }} force_non_negative;
/*
  Indices for the tt+light all-matched hypothesis

                W   /---  l
               _/---
        t     /     \---  v
    +---------
    |         \
    |          \--------  b     j1
    |
    |         W    /----  q     j2
    |         _/---
    |   t    /     \----  q'    j3
    +----------
               \
                \-------  b     j4
*/
    for (unsigned int j1 = 0; j1 < recoJets.size(); j1++){
        const RecoJet &rj1 = recoJets.at(j1);
        if (NuSolutions.size() == 1){
            kinBkg.Mlvb0 = (rj1+tlv_lep+NuSolutions.at(0)).M();
            PMlvb0_bkg = force_non_negative(content_of_at(tr_bkg.hmlvb0, kinBkg.Mlvb0));
        } else if (NuSolutions.size() == 2){
            kinBkg.Mlvb1 = (rj1+tlv_lep+NuSolutions.at(0)).M();
            kinBkg.Mlvb2 = (rj1+tlv_lep+NuSolutions.at(1)).M();
            PMlvb1_bkg = force_non_negative(content_of_at(tr_bkg.hmlvb1, kinBkg.Mlvb1));
            PMlvb2_bkg = force_non_negative(content_of_at(tr_bkg.hmlvb2, kinBkg.Mlvb2));
        }
        const float PMlvb_bkg = (NuSolutions.size()==1 ? PMlvb0_bkg : 0.65 * PMlvb1_bkg + 0.35 * PMlvb2_bkg);
        for (unsigned int j2 = 0; j2 < recoJets.size(); j2++){
            if(j2==j1) continue;
            const RecoJet &rj2 = recoJets.at(j2);
            for (unsigned int j3 = 0; j3 < recoJets.size(); j3++){
                if(j3==j1 || j3==j2) continue;
                const RecoJet &rj3 = recoJets.at(j3);
                kinBkg.Mqq = (rj2+rj3).M();
                PMqq = force_non_negative(content_of_at(tr_bkg.hmqq, kinBkg.Mqq));
                for (unsigned int j4 = 0; j4 < recoJets.size(); j4++){
                    if(j4==j1 || j4==j2 || j4==j3) continue;
                    const RecoJet &rj4 = recoJets.at(j4);
                    kinBkg.MqqbMinusMqq = compute_delta_mass(rj2, rj3, rj4);
                    PMqqbMinusMqq = force_non_negative(content_of_at(tr_bkg.hmqqbminusmqq, kinBkg.MqqbMinusMqq));
                    PermProbMPBkg = PMlvb_bkg * PMqq * PMqqbMinusMqq;
                    PermProbBtagBkg = rj1.probB * rj2.probL * (fWll * rj3.probL + fWlc * rj3.probC) * rj4.probB;
                    // assume that any extra jet is light
                    for (unsigned int extra_j = 0; extra_j < recoJets.size(); extra_j ++){
                        if(extra_j==j1 || extra_j==j2 || extra_j==j3 || extra_j==j4 ) continue;
                        const RecoJet &extra_rj = recoJets.at(extra_j);
                        PermProbBtagBkg *= extra_rj.probL;
                    }//extra_j loop
                    prob.num += PermProbBtagBkg*PermProbMPBkg;
                    prob.den += PermProbBtagBkg;
                } // for(j4)
            } // for(j3)
        } // for(j2)
    } // for(j1)
    return prob;
}
//----------------------------------------------------------
const TrainingBkg&
TTH_Discriminant::pickAllMatchBkgTraining(const size_t nj, const size_t nb) const
{
    return (nj==4 and nb==2 ?  m_bkg_4jex2bex_am :
            nj==4 and nb==3 ?  m_bkg_4jex3bex_am :
            nj==4 and nb>=4 ?  m_bkg_4jex4bin_am :
            nj==5 and nb==2 ?  m_bkg_5jex2bex_am :
            nj==5 and nb==3 ?  m_bkg_5jex3bex_am :
            nj==5 and nb>=4 ?  m_bkg_5jex4bin_am :
            nj>=6 and nb==2 ?  m_bkg_6jin2bex_am :
            nj>=6 and nb==3 ?  m_bkg_6jin3bex_am :
            nj>=6 and nb>=4 ?  m_bkg_6jin4bin_am :
            m_bkg_4jin2bin_am); // default: inclusive
}
//----------------------------------------------------------
const TrainingBkg&
TTH_Discriminant::pickMiss1MatchBkgTraining(const size_t nj, const size_t nb) const
{
    return (nj==4 and nb==2 ?  m_bkg_4jex2bex_mm :
            nj==4 and nb==3 ?  m_bkg_4jex3bex_mm :
            nj==4 and nb>=4 ?  m_bkg_4jex4bin_mm :
            nj==5 and nb==2 ?  m_bkg_5jex2bex_mm :
            nj==5 and nb==3 ?  m_bkg_5jex3bex_mm :
            nj==5 and nb>=4 ?  m_bkg_5jex4bin_mm :
            nj>=6 and nb==2 ?  m_bkg_6jin2bex_mm :
            nj>=6 and nb==3 ?  m_bkg_6jin3bex_mm :
            nj>=6 and nb>=4 ?  m_bkg_6jin4bin_mm :
            m_bkg_4jin2bin_mm); // default: inclusive
}
//----------------------------------------------------------
const TrainingSig&
TTH_Discriminant::pickMiss1MatchSigTraining(const size_t nj, const size_t nb) const
{
 return (nj==4 and nb==2 ?  m_sig_4jex2bex_mm :
         nj==4 and nb==3 ?  m_sig_4jex3bex_mm :
         nj==4 and nb>=4 ?  m_sig_4jex4bin_mm :
         nj==5 and nb==2 ?  m_sig_5jex2bex_mm :
         nj==5 and nb==3 ?  m_sig_5jex3bex_mm :
         nj==5 and nb>=4 ?  m_sig_5jex4bin_mm :
         nj>=6 and nb==2 ?  m_sig_6jin2bex_mm :
         nj>=6 and nb==3 ?  m_sig_6jin3bex_mm :
         nj>=6 and nb>=4 ?  m_sig_6jin4bin_mm :
         m_sig_4jin2bin_mm); // default: inclusive
}
//----------------------------------------------------------
bool TTH_Discriminant::IsLepton(int pdgid){
  if((fabs(pdgid)==11) || (fabs(pdgid)==13) || (fabs(pdgid)==15)) return true;
  return false;
}
//----------------------------------------------------------
bool TTH_Discriminant::IsNu(int pdgid){
  if((fabs(pdgid)==12) || (fabs(pdgid)==14) || (fabs(pdgid)==16)) return true;
  return false;
}
//----------------------------------------------------------
bool TTH_Discriminant::IsUdcs(int pdgid)
{
    return (pdgid==+1 or pdgid==-1 or
            pdgid==+2 or pdgid==-2 or
            pdgid==+3 or pdgid==-3 or
            pdgid==+4 or pdgid==-4);
}
//----------------------------------------------------------
bool TTH_Discriminant::IsUc(int pdgid)
{
    return (pdgid==+2 or pdgid==-2 or
            pdgid==+4 or pdgid==-4);
}
//----------------------------------------------------------
bool TTH_Discriminant::IsBot(int pdgid)
{
    return (pdgid==+5 or pdgid==-5);
}
//----------------------------------------------------------
bool TTH_Discriminant::IsTop(int pdgid)
{
    return (pdgid==+6 or pdgid==-6);
}
//----------------------------------------------------------
bool TTH_Discriminant::IsW(int pdgid)
{
    return (pdgid==+24 or pdgid==-24);
}
//----------------------------------------------------------
bool TTH_Discriminant::IsHiggs(int pdgid)
{
    return (pdgid==+25);
}
//----------------------------------------------------------
