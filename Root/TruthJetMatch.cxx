#include "TTHAnalysis/TruthJetMatch.h"

#include "TTHAnalysis/RecoJet.h"
#include "TTHAnalysis/TlvTruth.h"

#include "TLorentzVector.h"

namespace tth
{
using std::string;
std::string TruthJetMatch2str(const TruthJetMatch &t)
{
    string s = "unknown";
    switch(t) {
    case TruthJetMatch::UnknownTruthJetMatch : s = "UnknownTruthJetMatch"; break;
    case TruthJetMatch::bLepTop              : s = "bLepTop"             ; break;
    case TruthJetMatch::bHadTop              : s = "bHadTop"             ; break;
    case TruthJetMatch::LeadJetW             : s = "LeadJetW"            ; break;
    case TruthJetMatch::SubLeadJetW          : s = "SubLeadJetW"         ; break;
    case TruthJetMatch::LeadbHiggs           : s = "LeadbHiggs"          ; break;
    case TruthJetMatch::SubLeadbHiggs        : s = "SubLeadbHiggs"       ; break;
    case TruthJetMatch::JetHiggsTop          : s = "JetHiggsTop"         ; break;
    case TruthJetMatch::DoubleTruthJetMatch  : s = "DoubleTruthJetMatch" ; break;
    }
    return s;
}
//----------------------------------------------------------
bool matchDeltaR03(const TLorentzVector &truthParticle, const TLorentzVector &jet){
  float dRmatch= 0.3;
  float dR = truthParticle.DeltaR(jet);
  if(dR>=dRmatch) return false;
  return true;
}
//----------------------------------------------------------
TruthJetMatch determineJetTruthMatch(const RecoJet &reco_jet,
                                     const TlvTruth &tlvt)
{
    TruthJetMatch res = reco_jet.truthmatch;
    const TruthJetMatch unkn = TruthJetMatch::UnknownTruthJetMatch;
    if(tlvt.has_bleptop1 && matchDeltaR03(tlvt.bleptop1, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::bLepTop);
        res = (double_match ? tth::DoubleTruthJetMatch : tth::bLepTop);
    }//truthmatch lep top 1
    if(tlvt.has_bleptop2 and matchDeltaR03(tlvt.bleptop2, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::bLepTop);
        res = (double_match ? tth::DoubleTruthJetMatch: tth::bLepTop);
    }//truthmatch lep top 2
    if(tlvt.has_leadqW and matchDeltaR03(tlvt.leadqW, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::LeadJetW);
        res = (double_match ? tth::DoubleTruthJetMatch : tth::LeadJetW);
    }//truthmatch lead q W
    if(tlvt.has_subqW and matchDeltaR03(tlvt.subqW, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::SubLeadJetW);
        res = (double_match ? tth::DoubleTruthJetMatch : tth::SubLeadJetW);
    }//truthmatch sublead q W
    if(tlvt.has_bhadtop and matchDeltaR03(tlvt.bhadtop, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::bHadTop);
        res = (double_match ? tth::DoubleTruthJetMatch : tth::bHadTop);
    }//truthmatch had top
    if(tlvt.has_leadbH and matchDeltaR03(tlvt.leadbH, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::LeadbHiggs);
        res = (double_match ? tth::DoubleTruthJetMatch : tth::LeadbHiggs);
    }//truthmatch lead b from H
    if(tlvt.has_subbH and matchDeltaR03(tlvt.subbH, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::SubLeadbHiggs);
        res = (double_match ? tth::DoubleTruthJetMatch : tth::SubLeadbHiggs);
    }//truthmatch sub b from H
    if(tlvt.has_qfromHtop and matchDeltaR03(tlvt.qfromHtop, reco_jet)){
        const bool double_match = (res>unkn and res!=tth::JetHiggsTop);
        res = (double_match ? tth::DoubleTruthJetMatch : tth::JetHiggsTop);
    }//truthmatch u/c Htop
    return res;
}
//----------------------------------------------------------
} // tth
