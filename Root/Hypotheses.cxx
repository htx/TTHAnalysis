#include "TTHAnalysis/Hypotheses.h"

#include "TTHAnalysis/RecoJet.h"

#include <limits>
#include <iostream>
#include <sstream>

using std::cout;
using std::endl;

namespace tth
{
//----------------------------------------------------------
SignalHypothesis::SignalHypothesis():
    b1(nullptr),
    b2(nullptr),
    blep(nullptr),
    j(nullptr),
    ej1(nullptr),
    ej2(nullptr),
    num_extra_jets(0)
{}
//----------------------------------------------------------
bool SignalHypothesis::allMatched() const
{
    return numberUnmatched()==0;
}
//----------------------------------------------------------
size_t SignalHypothesis::numberMatched() const
{
    return ((b1==nullptr   ? 0 : 1) +
            (b2==nullptr   ? 0 : 1) +
            (blep==nullptr ? 0 : 1) +
            (j==nullptr    ? 0 : 1));
}
//----------------------------------------------------------
size_t SignalHypothesis::numberUnmatched() const
{
    return ((b1==nullptr   ? 1 : 0) +
            (b2==nullptr   ? 1 : 0) +
            (blep==nullptr ? 1 : 0) +
            (j==nullptr    ? 1 : 0));
}
//----------------------------------------------------------
size_t SignalHypothesis::whatIsMissing() const
{
    size_t miss = std::numeric_limits<std::size_t>::max();
    if     (b1==nullptr) miss = 0;
    else if(b2==nullptr) miss = 1;
    else if(blep==nullptr) miss = 2;
    else if(j==nullptr) miss= 3;
    return miss;
}
//----------------------------------------------------------
bool SignalHypothesis::assignRecoJet(RecoJet &reco_jet)
{
    bool is_matched = true;
    const int truth_match = reco_jet.truthmatch;
    switch(truth_match) {
    case tth::LeadbHiggs : {
        if(not b1){ b1 = &reco_jet; }
        else {
            cout<<"Warning: multiple b1, taking highest-pt"<<endl;
            b1 = (b1->Pt() > reco_jet.Pt() ? b1 : &reco_jet);
            num_extra_jets++;
        }
        break;
    }
    case tth::SubLeadbHiggs : {
        if(not b2){ b2 = &reco_jet; }
        else {
            cout<<"Warning: multiple b2, taking highest-pt"<<endl;
            b2 = (b2->Pt() > reco_jet.Pt() ? b2 : &reco_jet);
            num_extra_jets++;
        }
        break;
    }
    case tth::bLepTop : {
        if(not blep){ blep = &reco_jet; }
        else {
            cout<<"Warning: multiple blep, taking highest-pt"<<endl;
            blep = (blep->Pt() > reco_jet.Pt() ? blep : &reco_jet);
            num_extra_jets++;
        }
        break;
    }
    case tth::JetHiggsTop : { // TODO  && truth_flav==4) {
        if(not j){ j = &reco_jet; }
        else {
            cout<<"Warning: multiple j, taking highest-pt"<<endl;
            j = (j->Pt() > reco_jet.Pt() ? j : &reco_jet);
            num_extra_jets++;
        }
        break;
    }
    default: {
        if     (not ej1){ ej1 = &reco_jet; }
        else if(not ej2){ ej2 = &reco_jet; }
        else {
            if     (ej1->Pt() < reco_jet.Pt()){ ej1 = &reco_jet; }
            else if(ej2->Pt() < reco_jet.Pt()){ ej2 = &reco_jet; }
        }
        num_extra_jets++;
        is_matched = false;
        break;
    }
    } // end switch(truth_match)
    return is_matched;
}
//----------------------------------------------------------
double compute_delta_mass(const TLorentzVector &p1,
                                     const TLorentzVector &p2,
                                     const TLorentzVector &p3)
{
    const double m123 = (p1 + p2 + p3).M();
    const double m12 = (p1 + p2).M();
    return m123 - m12;
}
/**
   @brief local function to replace missing match with extra jet

   Used for the missing parton hypothesis. The idea is to use the
   extra jet in place of the missing-match jet.
*/
struct ValidOrEj{
    RecoJet *ej;
    ValidOrEj(RecoJet *j): ej(j) {}
    RecoJet* operator()(RecoJet* j) { return (j==nullptr ? ej : j); }
};
//----------------------------------------------------------
tth::KinematicConfigurationSig
SignalHypothesis::kinematic(const TLorentzVector &tlv_lep,
                            const std::vector<TLorentzVector> &neutrino_solutions) const
{
    KinematicConfigurationSig kin;
    if(allMatched()){
        if (neutrino_solutions.size() == 1) {
            kin.Mlvb0 = (*blep + tlv_lep + neutrino_solutions.at(0)).M() ;
        } else if (neutrino_solutions.size() == 2) {
            kin.Mlvb1 = (*blep + tlv_lep + neutrino_solutions.at(0)).M() ;
            kin.Mlvb2 = (*blep + tlv_lep + neutrino_solutions.at(1)).M() ;
        }
        kin.Mbb = (*b1 + *b2).M();
        kin.Mbbq = (*b1 + *b2 + *j).M();
        kin.MbbqMinusMbb = compute_delta_mass(*b1, *b2, *j);
    } else if(numberUnmatched()==1 and ej1!=nullptr) { // 1 missing parton hypothesis
        ValidOrEj jORej(ej1);
        if (neutrino_solutions.size() == 1) {
            kin.Mlvb0 = (*jORej(blep) + tlv_lep + neutrino_solutions.at(0)).M() ;
        } else if (neutrino_solutions.size() == 2) {
            kin.Mlvb1 = (*jORej(blep) + tlv_lep + neutrino_solutions.at(0)).M() ;
            kin.Mlvb2 = (*jORej(blep) + tlv_lep + neutrino_solutions.at(1)).M() ;
        }
        kin.Mbb = (*jORej(b1) + *jORej(b2)).M() ;
        kin.Mbbq = (*jORej(b1) + *jORej(b2) + *jORej(j)).M() ;
        kin.MbbqMinusMbb = compute_delta_mass(*jORej(b1), *jORej(b2), *jORej(j));
    } else {
        // cout<<"Hypotheses with >1 un-matched jets are not implemented yet"<<endl;
    }
    return kin;
}
//----------------------------------------------------------
std::string SignalHypothesis::str() const
{
    std::ostringstream oss;
    oss<<"SignalHypothesis:"
       <<" b1 "<<(b1!=nullptr) // don't care about ptr value, only whether it's there
       <<" b2 "<<(b2!=nullptr)
       <<" blep "<<(blep!=nullptr)
       <<" j "<<(j!=nullptr)
       <<" ej1 "<<(ej1!=nullptr)
       <<" ej2 "<<(ej2!=nullptr)
       <<" num_extra_jets "<<num_extra_jets;
    return oss.str();
}
//----------------------------------------------------------
std::string SignalHypothesis::str_detailed() const
{
    std::ostringstream oss;
    oss<<"SignalHypothesis: "
       <<"\n b1 "<<(b1!=nullptr)<<" : "<<(b1 ? b1->str_truthmatch() : "")
       <<"\n b2 "<<(b2!=nullptr)<<" : "<<(b2 ? b2->str_truthmatch() : "")
       <<"\n blep "<<(blep!=nullptr)<<" : "<<(blep ? blep->str_truthmatch() : "")
       <<"\n j "<<(j!=nullptr)<<" : "<<(j ? j->str_truthmatch() : "")
       <<"\n ej1 "<<(ej1!=nullptr)<<" : "<<(ej1 ? ej1->str_truthmatch() : "")
       <<"\n ej2 "<<(ej2!=nullptr)<<" : "<<(ej2 ? ej2->str_truthmatch() : "")
       <<"\n num_extra_jets "<<num_extra_jets;
    return oss.str();
}
//----------------------------------------------------------
BackgroundHypothesis::BackgroundHypothesis():
    q1(nullptr),
    q2(nullptr),
    blep(nullptr),
    bhad(nullptr),
    ej1(nullptr),
    ej2(nullptr),
    num_extra_jets(0)
{}
//----------------------------------------------------------
bool BackgroundHypothesis::assignRecoJet(RecoJet &reco_jet)
{
    bool is_matched = true;
    const int truth_match = reco_jet.truthmatch;
    switch(truth_match) {
    case tth::LeadJetW : {
        if(not q1){ q1 = &reco_jet; }
        else {
            cout<<"Warning: multiple q1, taking highest-pt"<<endl;
            q1 = (q1->Pt() > reco_jet.Pt() ? q1 : &reco_jet);
            num_extra_jets++;
        }
        // if(q1 and q2 and q1->Pt()<q2->Pt()) { swap(q1, q2); } // DG-2017-11 not sure we need it
        break;
    }
    case tth::SubLeadJetW : {
        if(not q2){ q2 = &reco_jet; }
        else {
            cout<<"Warning: multiple q2, taking highest-pt"<<endl;
            q2 = (q2->Pt() > reco_jet.Pt() ? q2 : &reco_jet);
            num_extra_jets++;
        }
        // if(q1 and q2 and q1->Pt()<q2->Pt()) { swap(q1, q2); } // DG-2017-11 not sure we need it
        break;
    }
    case tth::bLepTop : {
        if(not blep){ blep = &reco_jet; }
        else {
            cout<<"Warning: multiple blep, taking highest-pt"<<endl;
            blep = (blep->Pt() > reco_jet.Pt() ? blep : &reco_jet);
            num_extra_jets++;
        }
        break;
    }
    case tth::bHadTop : {
        if(not bhad){ bhad = &reco_jet; }
        else {
            cout<<"Warning: multiple bhad, taking highest-pt"<<endl;
            bhad = (bhad->Pt() > reco_jet.Pt() ? bhad : &reco_jet);
            num_extra_jets++;
        }
        break;
    }
    default: {
        if     (not ej1){ ej1 = &reco_jet; }
        else if(not ej2){ ej2 = &reco_jet; }
        else {
            if     (ej1->Pt() < reco_jet.Pt()){ ej1 = &reco_jet; }
            else if(ej2->Pt() < reco_jet.Pt()){ ej2 = &reco_jet; }
        }
        num_extra_jets++;
        is_matched = false;
        break;
    }
    } // end switch(truth_match)
    return is_matched;
}
//----------------------------------------------------------
bool BackgroundHypothesis::allMatched() const
{
    return numberUnmatched()==0;
}
//----------------------------------------------------------
size_t BackgroundHypothesis::numberMatched() const
{
    return ((q1==nullptr   ? 0 : 1) +
            (q2==nullptr   ? 0 : 1) +
            (blep==nullptr ? 0 : 1) +
            (bhad==nullptr ? 0 : 1));
}
//----------------------------------------------------------
size_t BackgroundHypothesis::numberUnmatched() const
{
    return ((q1==nullptr   ? 1 : 0) +
            (q2==nullptr   ? 1 : 0) +
            (blep==nullptr ? 1 : 0) +
            (bhad==nullptr ? 1 : 0));
}
//----------------------------------------------------------
size_t BackgroundHypothesis::whatIsMissing() const
{
    size_t miss = std::numeric_limits<std::size_t>::max();
    if     (q1==nullptr  ) miss = 0;
    else if(q2==nullptr  ) miss = 1;
    else if(blep==nullptr) miss = 2;
    else if(bhad==nullptr) miss = 3;
    return miss;
}
//----------------------------------------------------------
KinematicConfigurationBkg
BackgroundHypothesis::kinematic(const TLorentzVector &tlv_lep,
                                const std::vector<TLorentzVector> &neutrino_solutions) const
{
    KinematicConfigurationBkg kin;
    if(allMatched()){
        if (neutrino_solutions.size() == 1) {
            kin.Mlvb0 = (*blep + tlv_lep + neutrino_solutions.at(0)).M() ;
        } else if (neutrino_solutions.size() == 2) {
            kin.Mlvb1 = (*blep + tlv_lep + neutrino_solutions.at(0)).M() ;
            kin.Mlvb2 = (*blep + tlv_lep + neutrino_solutions.at(1)).M() ;
        }
        kin.Mqq = (*q1 + *q2).M();
        kin.Mqqb = (*q1 + *q2 + *bhad).M();
        kin.MqqbMinusMqq = compute_delta_mass(*q1, *q2, *bhad);
    } else if(numberUnmatched()==1 and ej1!=nullptr) { // 1 missing parton hypothesis
        ValidOrEj jORej(ej1);
        if (neutrino_solutions.size() == 1) {
            kin.Mlvb0 = (*jORej(blep) + tlv_lep + neutrino_solutions.at(0)).M() ;
        } else if (neutrino_solutions.size() == 2) {
            kin.Mlvb1 = (*jORej(blep) + tlv_lep + neutrino_solutions.at(0)).M() ;
            kin.Mlvb2 = (*jORej(blep) + tlv_lep + neutrino_solutions.at(1)).M() ;
        }
        kin.Mqq = (*jORej(q1) + *jORej(q2)).M();
        kin.Mqqb = (*jORej(q1) + *jORej(q2) + *jORej(bhad)).M();
        kin.MqqbMinusMqq = compute_delta_mass(*jORej(q1), *jORej(q2), *jORej(bhad));
    } else {
        // cout<<"Hypotheses with >1 un-matched jets are not implemented yet"<<endl;
    }
    return kin;
}
//----------------------------------------------------------
//----------------------------------------------------------
std::string BackgroundHypothesis::str() const
{
    std::ostringstream oss;
    oss<<"BackgroundHypothesis:"
       <<" q1 "<<(q1!=nullptr)
       <<" q2 "<<(q2!=nullptr)
       <<" blep "<<(blep!=nullptr)
       <<" bhad "<<(bhad!=nullptr)
       <<" ej1 "<<(ej1!=nullptr)
       <<" ej2 "<<(ej2!=nullptr)
       <<" num_extra_jets "<<num_extra_jets;
    return oss.str();
}
//----------------------------------------------------------
std::string BackgroundHypothesis::str_detailed() const
{
    std::ostringstream oss;
    oss<<"BackgroundHypothesis: "
       <<"\n q1 "<<(q1!=nullptr)<<" : "<<(q1 ? q1->str_truthmatch() : "")
       <<"\n q2 "<<(q2!=nullptr)<<" : "<<(q2 ? q2->str_truthmatch() : "")
       <<"\n blep "<<(blep!=nullptr)<<" : "<<(blep ? blep->str_truthmatch() : "")
       <<"\n bhad "<<(bhad!=nullptr)<<" : "<<(bhad ? bhad->str_truthmatch() : "")
       <<"\n ej1 "<<(ej1!=nullptr)<<" : "<<(ej1 ? ej1->str_truthmatch() : "")
       <<"\n ej2 "<<(ej2!=nullptr)<<" : "<<(ej2 ? ej2->str_truthmatch() : "")
       <<"\n num_extra_jets "<<num_extra_jets;
     return oss.str();
}
//----------------------------------------------------------
} // tth
