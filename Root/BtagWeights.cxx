#include "TTHAnalysis/BtagWeights.h"

#include <stdexcept>
#include <string>
#include <vector>

namespace tth
{

using std::vector;

//----------------------------------------------------------
BtagWeights GetBTagWeights77OP(const float &taggerOutput) {
    if(taggerOutput>1.0)
        throw std::invalid_argument("GetBTagWeights77OP: invalid tagger output "
                                    +std::to_string(taggerOutput));
    BtagWeights weights;
    // mv2c10 77% operating point; values from
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarks#MV2c10_tagger_added_on_11th_May
    const float tagger_threshold = 0.645925;
    const float efficiency_l = 1.0/134.34;
    const float efficiency_c = 1.0/6.21;
    const float efficiency_b = 0.77;

    weights.tagger_output = taggerOutput;
    const bool is_tagged = taggerOutput > tagger_threshold;
    weights.probability_l_tag = is_tagged ? efficiency_l : (1.0 - efficiency_l);
    weights.probability_c_tag = is_tagged ? efficiency_c : (1.0 - efficiency_c);
    weights.probability_b_tag = is_tagged ? efficiency_b : (1.0 - efficiency_b);
    weights.probability_b_tag_binary = is_tagged ? 1.0 : 0.0;
    return weights;
}
//----------------------------------------------------------
BtagWeights GetBTagWeightsVarOP(const float &taggerOutput) {
    if(taggerOutput<-1.0 or taggerOutput>+1.0)
        throw std::invalid_argument("GetBTagWeightsVarOP: invalid tagger output "
                                    +std::to_string(taggerOutput));
    BtagWeights weights;
    // mv2c10 operating points; values from
    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarks#MV2c10_tagger_added_on_11th_May
    const vector<float> thresholds     = {-1.0, 0.1758475, 0.645925, 0.8244273, 0.934906, +1.0};
    const vector<float> efficiencies_b = { 1.0, 0.85,      0.77,     0.70,      0.60,      0.0};
    const vector<float> efficiencies_c = { 1.0, 1/3.10,    1/6.21,   1/12.17,   1/34.54,   0.0};
    const vector<float> efficiencies_l = { 1.0, 1/33.53,   1/134.34, 1/381.32,  1/1538.78, 0.0};
    // CAJR
    const vector<float> SF_c = { 1.0, 1.2, 1.3, 1.4, 1.6, 1.6 };
    const vector<float> SF_l = { 1.0, 2.0, 2.0, 2.0, 2.0, 2.0 };
    // CAJR
    const vector<float> &binary_effs_b = efficiencies_b; // DG 2017-03-20 not sure this is correct, but not using binary for now
    unsigned int index_tightest_op = 0;
    while(index_tightest_op<thresholds.size()) {
        if(taggerOutput >  thresholds[index_tightest_op  ] and
           taggerOutput <= thresholds[index_tightest_op+1])
            break;
        else
            index_tightest_op += 1;
    }
    const int &ito = index_tightest_op;
    weights.tagger_output = taggerOutput;
    weights.probability_l_tag = SF_l[ito]*efficiencies_l[ito] - SF_l[ito+1]*efficiencies_l[ito+1];
    weights.probability_c_tag = SF_c[ito]*efficiencies_c[ito] - SF_c[ito+1]*efficiencies_c[ito+1];
    weights.probability_b_tag = efficiencies_b[ito] - efficiencies_b[ito+1];
    weights.probability_b_tag_binary = binary_effs_b[ito] - binary_effs_b[ito+1];
    return weights;
}
//----------------------------------------------------------

} // tth
