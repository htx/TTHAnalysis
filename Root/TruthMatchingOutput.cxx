#include "TTHAnalysis/TruthMatchingOutput.h"

#include <iostream>
#include <sstream>

namespace tth
{

TruthMatchingOutput::TruthMatchingOutput()
{
    ClearOutputData();
}

void TruthMatchingOutput::ClearOutputData() {
    o_Mbb = 0;
    o_Mqqb = 0;
    o_Mqq = 0;
    o_MqqbMinusMqq = 0;
    o_Mlvb0 = 0;
    o_Mlvb1 = 0;
    o_Mlvb2 = 0;
    o_Mbbj = 0;
    o_MbbjMinusMbb = 0;
    o_jetFromTop_truthflav = -99;
    o_qFromTop_pt = 0;
    o_ExtraJetsbb = 0;
    o_ExtraJetsbj = 0;
    o_leadJetPt = 0;
    o_leadJetEta = 0;
    o_matchedjets_n = 0;
    o_matchedjetsttbar_n = 0;
    o_truthbextrajets_n = 0;
    o_truthnotbextrajets_n = 0;

    o_ExtraJetsPerm.clear();
    o_ExtraJetsPerm_ll.clear();
    o_ExtraJetsPerm_lc.clear();
    o_ExtraJetsPerm_lb.clear();
    o_ExtraJetsPerm_cc.clear();
    o_ExtraJetsPerm_bc.clear();
    o_ExtraJetsPerm_bb.clear();
    o_ExtraJetsPerm_notbb.clear();
    o_Mqq_NoLeadJetW_b.clear();
    o_Mqq_NoLeadJetW_c.clear();
    o_Mqq_NoLeadJetW_l.clear();
    o_Mqqb_NoLeadJetW_b.clear();
    o_Mqqb_NoLeadJetW_c.clear();
    o_Mqqb_NoLeadJetW_l.clear();
    o_MqqbMinusMqq_NoLeadJetW_b.clear();
    o_MqqbMinusMqq_NoLeadJetW_c.clear();
    o_MqqbMinusMqq_NoLeadJetW_l.clear();
    o_Mqq_NoSubLeadJetW_b.clear();
    o_Mqq_NoSubLeadJetW_c.clear();
    o_Mqq_NoSubLeadJetW_l.clear();
    o_Mqqb_NoSubLeadJetW_b.clear();
    o_Mqqb_NoSubLeadJetW_c.clear();
    o_Mqqb_NoSubLeadJetW_l.clear();
    o_MqqbMinusMqq_NoSubLeadJetW_b.clear();
    o_MqqbMinusMqq_NoSubLeadJetW_c.clear();
    o_MqqbMinusMqq_NoSubLeadJetW_l.clear();
    o_ExtraJetNoLeadJetW_truthflav.clear();
    o_ExtraJetNoSubLeadJetW_truthflav.clear();

    o_Mqq_NoLeadJetW_Jet_1_b = 0;
    o_Mqq_NoLeadJetW_Jet_1_c = 0;
    o_Mqq_NoLeadJetW_Jet_1_l = 0;
    o_Mqqb_NoLeadJetW_Jet_1_b = 0;
    o_Mqqb_NoLeadJetW_Jet_1_c = 0;
    o_Mqqb_NoLeadJetW_Jet_1_l = 0;
    o_MqqbMinusMqq_NoLeadJetW_Jet_1_b = 0;
    o_MqqbMinusMqq_NoLeadJetW_Jet_1_c = 0;
    o_MqqbMinusMqq_NoLeadJetW_Jet_1_l = 0;
    o_Mqq_NoSubLeadJetW_Jet_1_b = 0;
    o_Mqq_NoSubLeadJetW_Jet_1_c = 0;
    o_Mqq_NoSubLeadJetW_Jet_1_l = 0;
    o_Mqqb_NoSubLeadJetW_Jet_1_b = 0;
    o_Mqqb_NoSubLeadJetW_Jet_1_c = 0;
    o_Mqqb_NoSubLeadJetW_Jet_1_l = 0;
    o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b = 0;
    o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c = 0;
    o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l = 0;
    o_ExtraJetNoLeadJetW_Jet_1_truthflav = 0;
    o_ExtraJetNoSubLeadJetW_Jet_1_truthflav = 0;

    o_jets_pt.clear();
    o_jets_eta.clear();

    o_leadJetFromW_truthflav = -100;
    o_subJetFromW_truthflav = -100;
    o_extraJet1_truthflav = -100;
    o_extraJet2_truthflav = -100;
    o_extrajetleadnotb_truthflav = -100;
    o_Mqq_Jet_1 = 0;
    o_Mqqb_Jet_1 = 0;
    o_MqqbMinusMqq_Jet_1 = 0;
    o_jetFromW5matched_truthflav = -100;
    o_extrajet5matched_truthflav = -100;

    o_extraJet1Perm_truthflav.clear();
    o_extraJet2Perm_truthflav.clear();
    o_jets_truthflav.clear();
    o_jets_truthmatch.clear();

    o_nottruthmatch = -9;
    o_nextrajets = 0;

}
const TruthMatchingOutput& TruthMatchingOutput::copyDataTo(VLQ_OutputData */*tthod*/) const
{
    std::cout<<"TruthMatchingOutput::copyDataTo: obsolete"<<std::endl;
/*
    tthod->o_ht = o_ht;
    tthod->o_Mbb = o_Mbb;
    tthod->o_Mqqb = o_Mqqb;
    tthod->o_Mqq = o_Mqq;
    tthod->o_MqqbMinusMqq = o_MqqbMinusMqq;
    tthod->o_Mlvb0 = o_Mlvb0;
    tthod->o_Mlvb1 = o_Mlvb1;
    tthod->o_Mlvb2 = o_Mlvb2;
    tthod->o_Mbbj = o_Mbbj;
    tthod->o_MbbjMinusMbb = o_MbbjMinusMbb;
    tthod->o_jetFromTop_truthflav = o_jetFromTop_truthflav;
    tthod->o_ExtraJetsbb = o_ExtraJetsbb;
    tthod->o_ExtraJetsbj = o_ExtraJetsbj;
    tthod->o_leadJetPt = o_leadJetPt;
    tthod->o_leadJetEta = o_leadJetEta;
    tthod->o_jets_n = o_jets_n;
    tthod->o_bjets_n = o_bjets_n;
    tthod->o_matchedjets_n = o_matchedjets_n;
    tthod->o_matchedjetsttbar_n = o_matchedjetsttbar_n;
    tthod->o_truthbextrajets_n = o_truthbextrajets_n;
    tthod->o_truthnotbextrajets_n = o_truthnotbextrajets_n;

    (*tthod->o_ExtraJetsPerm) = o_ExtraJetsPerm;
    (*tthod->o_ExtraJetsPerm_ll) = o_ExtraJetsPerm_ll;
    (*tthod->o_ExtraJetsPerm_lc) = o_ExtraJetsPerm_lc;
    (*tthod->o_ExtraJetsPerm_lb) = o_ExtraJetsPerm_lb;
    (*tthod->o_ExtraJetsPerm_cc) = o_ExtraJetsPerm_cc;
    (*tthod->o_ExtraJetsPerm_bc) = o_ExtraJetsPerm_bc;
    (*tthod->o_ExtraJetsPerm_bb) = o_ExtraJetsPerm_bb;
    (*tthod->o_ExtraJetsPerm_notbb) = o_ExtraJetsPerm_notbb;
    (*tthod->o_Mqq_NoLeadJetW_b) = o_Mqq_NoLeadJetW_b;
    (*tthod->o_Mqq_NoLeadJetW_c) = o_Mqq_NoLeadJetW_c;
    (*tthod->o_Mqq_NoLeadJetW_l) = o_Mqq_NoLeadJetW_l;
    (*tthod->o_Mqqb_NoLeadJetW_b) = o_Mqqb_NoLeadJetW_b;
    (*tthod->o_Mqqb_NoLeadJetW_c) = o_Mqqb_NoLeadJetW_c;
    (*tthod->o_Mqqb_NoLeadJetW_l) = o_Mqqb_NoLeadJetW_l;
    (*tthod->o_MqqbMinusMqq_NoLeadJetW_b) = o_MqqbMinusMqq_NoLeadJetW_b;
    (*tthod->o_MqqbMinusMqq_NoLeadJetW_c) = o_MqqbMinusMqq_NoLeadJetW_c;
    (*tthod->o_MqqbMinusMqq_NoLeadJetW_l) = o_MqqbMinusMqq_NoLeadJetW_l;
    (*tthod->o_Mqq_NoSubLeadJetW_b) = o_Mqq_NoSubLeadJetW_b;
    (*tthod->o_Mqq_NoSubLeadJetW_c) = o_Mqq_NoSubLeadJetW_c;
    (*tthod->o_Mqq_NoSubLeadJetW_l) = o_Mqq_NoSubLeadJetW_l;
    (*tthod->o_Mqqb_NoSubLeadJetW_b) = o_Mqqb_NoSubLeadJetW_b;
    (*tthod->o_Mqqb_NoSubLeadJetW_c) = o_Mqqb_NoSubLeadJetW_c;
    (*tthod->o_Mqqb_NoSubLeadJetW_l) = o_Mqqb_NoSubLeadJetW_l;
    (*tthod->o_MqqbMinusMqq_NoSubLeadJetW_b) = o_MqqbMinusMqq_NoSubLeadJetW_b;
    (*tthod->o_MqqbMinusMqq_NoSubLeadJetW_c) = o_MqqbMinusMqq_NoSubLeadJetW_c;
    (*tthod->o_MqqbMinusMqq_NoSubLeadJetW_l) = o_MqqbMinusMqq_NoSubLeadJetW_l;
    (*tthod->o_ExtraJetNoLeadJetW_truthflav) = o_ExtraJetNoLeadJetW_truthflav;
    (*tthod->o_ExtraJetNoSubLeadJetW_truthflav) = o_ExtraJetNoSubLeadJetW_truthflav;

    tthod->o_Mqq_NoLeadJetW_Jet_1_b = o_Mqq_NoLeadJetW_Jet_1_b;
    tthod->o_Mqq_NoLeadJetW_Jet_1_c = o_Mqq_NoLeadJetW_Jet_1_c;
    tthod->o_Mqq_NoLeadJetW_Jet_1_l = o_Mqq_NoLeadJetW_Jet_1_l;
    tthod->o_Mqqb_NoLeadJetW_Jet_1_b = o_Mqqb_NoLeadJetW_Jet_1_b;
    tthod->o_Mqqb_NoLeadJetW_Jet_1_c = o_Mqqb_NoLeadJetW_Jet_1_c;
    tthod->o_Mqqb_NoLeadJetW_Jet_1_l = o_Mqqb_NoLeadJetW_Jet_1_l;
    tthod->o_MqqbMinusMqq_NoLeadJetW_Jet_1_b = o_MqqbMinusMqq_NoLeadJetW_Jet_1_b;
    tthod->o_MqqbMinusMqq_NoLeadJetW_Jet_1_c = o_MqqbMinusMqq_NoLeadJetW_Jet_1_c;
    tthod->o_MqqbMinusMqq_NoLeadJetW_Jet_1_l = o_MqqbMinusMqq_NoLeadJetW_Jet_1_l;
    tthod->o_Mqq_NoSubLeadJetW_Jet_1_b = o_Mqq_NoSubLeadJetW_Jet_1_b;
    tthod->o_Mqq_NoSubLeadJetW_Jet_1_c = o_Mqq_NoSubLeadJetW_Jet_1_c;
    tthod->o_Mqq_NoSubLeadJetW_Jet_1_l = o_Mqq_NoSubLeadJetW_Jet_1_l;
    tthod->o_Mqqb_NoSubLeadJetW_Jet_1_b = o_Mqqb_NoSubLeadJetW_Jet_1_b;
    tthod->o_Mqqb_NoSubLeadJetW_Jet_1_c = o_Mqqb_NoSubLeadJetW_Jet_1_c;
    tthod->o_Mqqb_NoSubLeadJetW_Jet_1_l = o_Mqqb_NoSubLeadJetW_Jet_1_l;
    tthod->o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b = o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b;
    tthod->o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c = o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c;
    tthod->o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l = o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l;
    tthod->o_ExtraJetNoLeadJetW_Jet_1_truthflav = o_ExtraJetNoLeadJetW_Jet_1_truthflav;
    tthod->o_ExtraJetNoSubLeadJetW_Jet_1_truthflav = o_ExtraJetNoSubLeadJetW_Jet_1_truthflav;

    (*tthod->o_jets_pt) = o_jets_pt;
    (*tthod->o_jets_eta) = o_jets_eta;

    tthod->o_leadJetFromW_truthflav = o_leadJetFromW_truthflav;
    tthod->o_subJetFromW_truthflav = o_subJetFromW_truthflav;
    tthod->o_extraJet1_truthflav = o_extraJet1_truthflav;
    tthod->o_extraJet2_truthflav = o_extraJet2_truthflav;
    tthod->o_extrajetleadnotb_truthflav = o_extrajetleadnotb_truthflav;
    tthod->o_Mqq_Jet_1 = o_Mqq_Jet_1;
    tthod->o_Mqqb_Jet_1 = o_Mqqb_Jet_1;
    tthod->o_MqqbMinusMqq_Jet_1 = o_MqqbMinusMqq_Jet_1;
    tthod->o_jetFromW5matched_truthflav = o_jetFromW5matched_truthflav;
    tthod->o_extrajet5matched_truthflav = o_extrajet5matched_truthflav;

    (*tthod->o_extraJet1Perm_truthflav) = o_extraJet1Perm_truthflav;
    (*tthod->o_extraJet2Perm_truthflav) = o_extraJet2Perm_truthflav;
    (*tthod->o_jets_truthflav) = o_jets_truthflav;
    (*tthod->o_jets_truthmatch) = o_jets_truthmatch;

    tthod->o_nottruthmatch = o_nottruthmatch;
*/
    return *this;
}

std::string TruthMatchingOutput::str() const
{
    std::ostringstream oss;
    oss<<"TruthMatchingOutput:"
        <<"\n Mbb: "<<o_Mbb
        <<"\n Mqqb: "<<o_Mqqb
        <<"\n Mqq: "<<o_Mqq
        <<"\n MqqbMinusMqq: "<<o_MqqbMinusMqq
        <<"\n Mlvb0: "<<o_Mlvb0
        <<"\n Mlvb1: "<<o_Mlvb1
        <<"\n Mlvb2: "<<o_Mlvb2
        <<"\n Mbbj: "<<o_Mbbj
        <<"\n MbbjMinusMbb: "<<o_MbbjMinusMbb
        <<"\n jetFromTop_truthflav: "<<o_jetFromTop_truthflav
        <<"\n qFromTop_pt: "<<o_qFromTop_pt
        <<"\n ExtraJetsbb: "<<o_ExtraJetsbb
        <<"\n ExtraJetsbj: "<<o_ExtraJetsbj
        <<"\n leadJetPt: "<<o_leadJetPt
        <<"\n leadJetEta: "<<o_leadJetEta
        <<"\n matchedjets_n: "<<o_matchedjets_n
        <<"\n matchedjetsttbar_n: "<<o_matchedjetsttbar_n
        <<"\n truthbextrajets_n: "<<o_truthbextrajets_n
        <<"\n truthnotbextrajets_n: "<<o_truthnotbextrajets_n
        <<"\n Mqq_NoLeadJetW_Jet_1_b: "<<o_Mqq_NoLeadJetW_Jet_1_b
        <<"\n Mqq_NoLeadJetW_Jet_1_c: "<<o_Mqq_NoLeadJetW_Jet_1_c
        <<"\n Mqq_NoLeadJetW_Jet_1_l: "<<o_Mqq_NoLeadJetW_Jet_1_l
        <<"\n Mqqb_NoLeadJetW_Jet_1_b: "<<o_Mqqb_NoLeadJetW_Jet_1_b
        <<"\n Mqqb_NoLeadJetW_Jet_1_c: "<<o_Mqqb_NoLeadJetW_Jet_1_c
        <<"\n Mqqb_NoLeadJetW_Jet_1_l: "<<o_Mqqb_NoLeadJetW_Jet_1_l
        <<"\n MqqbMinusMqq_NoLeadJetW_Jet_1_b: "<<o_MqqbMinusMqq_NoLeadJetW_Jet_1_b
        <<"\n MqqbMinusMqq_NoLeadJetW_Jet_1_c: "<<o_MqqbMinusMqq_NoLeadJetW_Jet_1_c
        <<"\n MqqbMinusMqq_NoLeadJetW_Jet_1_l: "<<o_MqqbMinusMqq_NoLeadJetW_Jet_1_l
        <<"\n Mqq_NoSubLeadJetW_Jet_1_b: "<<o_Mqq_NoSubLeadJetW_Jet_1_b
        <<"\n Mqq_NoSubLeadJetW_Jet_1_c: "<<o_Mqq_NoSubLeadJetW_Jet_1_c
        <<"\n Mqq_NoSubLeadJetW_Jet_1_l: "<<o_Mqq_NoSubLeadJetW_Jet_1_l
        <<"\n Mqqb_NoSubLeadJetW_Jet_1_b: "<<o_Mqqb_NoSubLeadJetW_Jet_1_b
        <<"\n Mqqb_NoSubLeadJetW_Jet_1_c: "<<o_Mqqb_NoSubLeadJetW_Jet_1_c
        <<"\n Mqqb_NoSubLeadJetW_Jet_1_l: "<<o_Mqqb_NoSubLeadJetW_Jet_1_l
        <<"\n MqqbMinusMqq_NoSubLeadJetW_Jet_1_b: "<<o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_b
        <<"\n MqqbMinusMqq_NoSubLeadJetW_Jet_1_c: "<<o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_c
        <<"\n MqqbMinusMqq_NoSubLeadJetW_Jet_1_l: "<<o_MqqbMinusMqq_NoSubLeadJetW_Jet_1_l
        <<"\n ExtraJetNoLeadJetW_Jet_1_truthflav: "<<o_ExtraJetNoLeadJetW_Jet_1_truthflav
        <<"\n ExtraJetNoSubLeadJetW_Jet_1_truthflav: "<<o_ExtraJetNoSubLeadJetW_Jet_1_truthflav
        <<"\n leadJetFromW_truthflav: "<<o_leadJetFromW_truthflav
        <<"\n subJetFromW_truthflav: "<<o_subJetFromW_truthflav
        <<"\n extraJet1_truthflav: "<<o_extraJet1_truthflav
        <<"\n extraJet2_truthflav: "<<o_extraJet2_truthflav
        <<"\n extrajetleadnotb_truthflav: "<<o_extrajetleadnotb_truthflav
        <<"\n Mqq_Jet_1: "<<o_Mqq_Jet_1
        <<"\n Mqqb_Jet_1: "<<o_Mqqb_Jet_1
        <<"\n MqqbMinusMqq_Jet_1: "<<o_MqqbMinusMqq_Jet_1
        <<"\n jetFromW5matched_truthflav: "<<o_jetFromW5matched_truthflav
        <<"\n extrajet5matched_truthflav: "<<o_extrajet5matched_truthflav
        <<"\n nottruthmatch: "<<o_nottruthmatch
        <<"\n nextrajets: "<<o_nextrajets
        ;
    return oss.str();
}

} // tth

