#include "TTHAnalysis/TTH_Options.h"

#include <iostream>

using std::cout;
using std::endl;
using std::string;
using std::transform;

//_____________________________________________________________
//
TTH_Options::TTH_Options():
    OptionsBase(),
    m_useTopPtWeight(false),
    m_useLeptonsSF(true),
    m_useBtagSF(true),
    m_usePileUpWeight(true),
    m_dumpTree(false),
    m_dumpHistos(true),
    m_runDiscriminant(false),
    m_runTemplate(false),
    m_runDataMC(true),
    m_runBoosted(false),
    m_jetsPtCut(25.),
    m_btagOP("77"),
    m_HFfilterType(NOFILTER),
    m_doTRF(false),
    m_normWeight(true),
    m_run3b(true),
    m_runS1(false),
    m_rejectOdd(false),
    m_rejectEven(false),
    m_data2015(true),
    m_data2016(true),
    m_doTruthPlots(false),
    m_nStart(-1),
    m_nFinish(-1)
{
}
//_____________________________________________________________
//
TTH_Options::TTH_Options(const TTH_Options &q) : OptionsBase(q) {
    m_useTopPtWeight = q.m_useTopPtWeight;
    m_useLeptonsSF = q.m_useLeptonsSF;
    m_useBtagSF = q.m_useBtagSF;
    m_usePileUpWeight = q.m_usePileUpWeight;
    m_dumpTree = q.m_dumpTree;
    m_dumpHistos = q.m_dumpHistos;
    m_runDiscriminant = q.m_runDiscriminant;
    m_runTemplate = q.m_runTemplate;
    m_runDataMC = q.m_runDataMC;
    m_runBoosted = q.m_runBoosted;
    m_jetsPtCut = q.m_jetsPtCut;
    m_btagOP = q.m_btagOP;
    m_HFfilterType = q.m_HFfilterType;
    m_doTRF = q.m_doTRF;
    m_normWeight = q.m_normWeight;
    m_run3b = q.m_run3b;
    m_runS1 = q.m_runS1;
    m_rejectOdd = q.m_rejectOdd;
    m_rejectEven = q.m_rejectEven;
    m_nStart = q.m_nStart;
    m_nFinish = q.m_nFinish;
    m_data2015 = q.m_data2015;
    m_data2016 = q.m_data2016;
    m_doTruthPlots = q.m_doTruthPlots;
}
//_____________________________________________________________
//
TTH_Options::~TTH_Options()
{
}
//_____________________________________________________________
//
bool contains(const std::string &str, const std::string &substr)
{
  return (str.find(substr)!=std::string::npos);
}
//_____________________________________________________________
//
bool TTH_Options::IdentifyOption(const std::string &argument,
                                 const std::string &value)
{
  string temp_arg = argument;
  string temp_val = value;
  if (!OptionsBase::IdentifyOption(argument, value)) {
      if (contains(temp_arg, "--USETOPPTWEIGHT")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_useTopPtWeight = true;
          else if (contains(temp_val, "FALSE")) m_useTopPtWeight = false;
          else { cout << "Unknown --useTopPtWeight option" << endl; }
      } else if (contains(temp_arg, "--JETPTCUT")) {
          m_jetsPtCut = atof(temp_val.c_str());
      } else if (contains(temp_arg, "--BTAGOP")) {
          m_btagOP = temp_val;
      } else if (contains(temp_arg, "--HFFILTERTYPE")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "NOFILTER")) m_HFfilterType = NOFILTER;
          else if (contains(temp_val, "TTBARLIGHT"))m_HFfilterType = TTBARLIGHT;
          else if (contains(temp_val, "TTBARBB")) m_HFfilterType = TTBARBB;
          else if (contains(temp_val, "TTBARCC")) m_HFfilterType = TTBARCC;
          else { cout << "Unknown --HFfilterType option" << endl; }
      } else if (contains(temp_arg, "--DOTRF")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_doTRF = true;
          else if (contains(temp_val, "FALSE")) m_doTRF = false;
          else { cout << "Unknown --doTRF option" << endl; }
      } else if (contains(temp_arg, "--NORMWEIGHT")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_normWeight = true;
          else if (contains(temp_val, "FALSE")) m_normWeight = false;
          else { cout << "Unknown --normWeight option" << endl; }
      } else if (contains(temp_arg, "--RUN3B")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_run3b = true;
          else if (contains(temp_val, "FALSE")) m_run3b = false;
          else { cout << "Unknown --run3b option" << endl; }
      } else if (contains(temp_arg, "--RUNS1")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_runS1 = true;
          else if (contains(temp_val, "FALSE")) m_runS1 = false;
          else { cout << "Unknown --runS1 option" << endl; }
      } else if (contains(temp_arg, "--REJECTODD")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_rejectOdd = true;
          else if (contains(temp_val, "FALSE")) m_rejectOdd = false;
          else { cout << "Unknown --rejectOdd option" << endl; }
      } else if (contains(temp_arg, "--REJECTEVEN")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_rejectEven = true;
          else if (contains(temp_val, "FALSE")) m_rejectEven = false;
          else { cout << "Unknown --rejectEven option" << endl; }
      } else if (contains(temp_arg, "--NSTART")) {
          m_nStart = atof(temp_val.c_str());
      } else if (contains(temp_arg, "--NFINISH")) {
          m_nFinish = atof(temp_val.c_str());
      } else if (contains(temp_arg, "--data2016")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_data2016 = true;
          else if (contains(temp_val, "FALSE")) m_data2016 = false;
          else { cout << "Unknown --data2016 option" << endl; }
      } else if (contains(temp_arg, "--RUNDISCRIMINANT")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_runDiscriminant = true;
          else if (contains(temp_val, "FALSE")) m_runDiscriminant = false;
          else { cout << "Unknown --runDiscriminant option" << endl; }
      } else if (contains(temp_arg, "--RUNTEMPLATE")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_runTemplate = true;
          else if (contains(temp_val, "FALSE")) m_runTemplate = false;
          else { cout << "Unknown --runTemplate option" << endl; }
      } else if (contains(temp_arg, "--RUNDATAMC")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_runDataMC = true;
          else if (contains(temp_val, "FALSE")) m_runDataMC = false;
          else { cout << "Unknown --runDataMC option" << endl; }
      } else if (contains(temp_arg, "--RUNBOOSTED")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_runBoosted = true;
          else if (contains(temp_val, "FALSE")) m_runBoosted = false;
          else { cout << "Unknown --runBoosted option" << endl; }
      } else if (contains(temp_arg, "--USELEPTONSSF")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_useLeptonsSF = true;
          else if (contains(temp_val, "FALSE")) m_useLeptonsSF = false;
          else { cout << "Unknown --useLeptonSF option" << endl; }
      } else if (contains(temp_arg, "--USEBTAGGINGSF")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_useBtagSF = true;
          else if (contains(temp_val, "FALSE")) m_useBtagSF = false;
          else { cout << "Unknown --useBtaggingSF option" << endl; }
      } else if (contains(temp_arg, "--USEPUWEIGHT")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_usePileUpWeight = true;
          else if (contains(temp_val, "FALSE")) m_usePileUpWeight = false;
          else { cout << "Unknown --usePUWeight option" << endl; }
      } else if (contains(temp_arg, "--DUMPTREE")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_dumpTree = true;
          else if (contains(temp_val, "FALSE")) m_dumpTree = false;
          else { cout << "Unknown --dumpTree option" << endl; }
      } else if (contains(temp_arg, "--DUMPHISTOS")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_dumpHistos = true;
          else if (contains(temp_val, "FALSE")) m_dumpHistos = false;
          else { cout << "Unknown --dumpHistos option" << endl; }
      } else if (contains(temp_arg, "--DOMCPLOTS")) {
          transform(temp_val.begin(), temp_val.end(), temp_val.begin(), toupper);
          if      (contains(temp_val, "TRUE")) m_doTruthPlots = true;
          else if (contains(temp_val, "FALSE")) m_doTruthPlots = false;
          else { cout << "Unknown --doTruthPlots option" << endl; }
      } else {
          return false;
      }
  }
  return true;
}

//_____________________________________________________________
//
void TTH_Options::PrintOptions() {
  OptionsBase::PrintOptions();
  cout<<"============== TTH_Options ================="<<endl;
  cout<<" m_useTopPtWeight          = "<<m_useTopPtWeight<<endl;
  cout<<" m_jetsPtCut               = "<<m_jetsPtCut<<endl;
  cout<<" m_btagOP                  = "<<m_btagOP<<endl;
  cout<<" m_useLeptonsSF            = "<<m_useLeptonsSF<<endl;
  cout<<" m_useBtagSF               = "<<m_useBtagSF<<endl;
  cout<<" m_usePileUpWeight         = "<<m_usePileUpWeight<< endl;
  cout<<" m_dumpTree                = "<<m_dumpTree<<endl;
  cout<<" m_dumpHistos              = "<<m_dumpHistos<<endl;
  cout<<" m_HFfilterType            = "<<m_HFfilterType<<endl;
  cout<<" m_runDiscriminant         = "<<m_runDiscriminant<< endl;
  cout<<" m_runTemplate             = "<<m_runTemplate<<endl;
  cout<<" m_runDataMC               = "<<m_runDataMC<<endl;
  cout<<" m_runBoosted              = "<<m_runBoosted<<endl;
  cout<<" m_doTRF                   = "<<m_doTRF<<endl;
  cout<<" m_normWeight              = "<<m_normWeight<<endl;
  cout<<" m_run3b                   = "<<m_run3b<<endl;
  cout<<" m_runS1                   = "<<m_runS1<<endl;
  cout<<" m_rejectOdd               = "<<m_rejectOdd<<endl;
  cout<<" m_rejectEven              = "<<m_rejectEven<<endl;
  cout<<" m_nStart                  = "<<m_nStart<<endl;
  cout<<" m_nFinish                 = "<<m_nFinish<<endl;
  cout<<" m_data2015                = "<<m_data2015<<endl;
  cout<<" m_data2016                = "<<m_data2016<<endl;
  cout<<" m_doTruthPlots            = "<<m_doTruthPlots<<endl;
  cout<<"============================================="<<endl;
  cout<<""<<endl;
}
