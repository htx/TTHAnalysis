#include <iostream>

#include "TTHAnalysis/TTH_NtupleData.h"

//____________________________________________________________
//
TTH_NtupleData::TTH_NtupleData()
    : NtupleData(), d_runNumber(0), d_eventNumber(0), ////////events
      d_trf_weight_77_4in(0), d_trf_weight_77_3ex(0), d_trf_weight_70_4in(0),
      d_trf_weight_70_3ex(0), d_mu(0), d_HF_SimpleClassification(0),
      d_TopHeavyFlavorFilterFlag(0), d_jets_n(0), ////////jets
      d_jets_pt(0), d_jets_eta(0), d_jets_phi(0), d_jets_e(0), d_jets_mv2c10(0),
      d_jets_mv2c20(0), d_jets_truthflav(0), d_jets_truthmatch(0),
      d_truthHiggsDaughtersID(0), d_truth_ttbar_pt(0), d_truth_top_pt(0),
      d_HhadT_jets(0), d_e_jets(0), d_mu_jets(0), d_e_jets_2015(0),
      d_mu_jets_2015(0), d_e_jets_2016(0), d_mu_jets_2016(0),
      d_boosted_e_jets(0), d_boosted_mu_jets(0),
      // d_jets_truth_index(0),
      d_ljets_pt(0), //////////ljets
      d_ljets_eta(0), d_ljets_m(0), d_ljets_sd12(0), d_ljets_sd23(0),
      d_ljets_tau21(0), d_ljets_tau32(0), d_ljets_topTag_loose(0),
      d_ljets_topTag_tight(0), d_ljets_topTagN_loose(0),
      d_ljets_topTagN_tight(0), d_el_pt(0),              //////////electrons
      d_el_eta(0), d_el_phi(0), d_el_e(0), d_mu_pt(0),   //////////muons
      d_mu_eta(0), d_mu_phi(0), d_mu_e(0), d_met_met(0), /////////met
      d_met_phi(0), d_eventWeight_pileup(0), d_eventWeight_mc(0),
      d_eventWeight_leptonSF(0), d_eventWeight_bTagSF(0), d_weight_jvt(0),
      d_weight_ttbar_FracRw(0), d_weight_ttbb_Nominal(0) {}

//____________________________________________________________
//
TTH_NtupleData::~TTH_NtupleData() {}
